//
//  QLPreviewItemCustom.m
//  ComcastBusiness
//
//  Created by User14 on 09/08/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "QLPreviewItemCustom.h"

@implementation QLPreviewItemCustom
@synthesize previewItemTitle = _previewItemTitle;
@synthesize previewItemURL   = _previewItemURL;

- (id) initWithTitle:(NSString*)title url:(NSURL*)url
{
    self = [super init];
    if (self != nil) {
        _previewItemTitle = title;
        _previewItemURL   = url;
    }
    return self;
}
@end
