//
//  QLPreviewItemCustom.h
//  ComcastBusiness
//
//  Created by User14 on 09/08/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuickLook/QuickLook.h>

@interface QLPreviewItemCustom : NSObject <QLPreviewItem>

- (id) initWithTitle:(NSString*)title url:(NSURL*)url;
@end
