//
//  VCLeftMenu.m
//  ComcastBusiness
//
//  Created by Admin on 5/11/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "VCLeftMenu.h"
#import "cellSideMenu.h"
#import "WebserviceAPI.h"
#import "NSString+URLEncoding.h"
#import "XmlParser.h"
#import "Reachability.h"
@interface VCLeftMenu ()

@end

@implementation VCLeftMenu
@synthesize tableView,arrayFeeds,viewSettings,btnSelectDiffShow,viewHamburger,btnHideHamburger,btnSettings,leaddataofflineforleftmenu,leadcount,ShowName,globalbutoon,newleadbutoon,arrayShows,shwlst,btnsel,autosyncbut,viewback;


- (void)viewDidLoad {
    
    self.btnSelectDiffShow.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.btnSelectDiffShow.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    self.viewSettings.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    

    
    [super viewDidLoad];
    self.arrayFeeds=[[NSMutableArray alloc] init];
    
    self.btnsel.hidden = YES;
    shwlst = [[Showlist alloc] init];
    //[self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
    
    self.newleadbutoon.layer.cornerRadius = 2; // this value vary as per your desire
    self.newleadbutoon.clipsToBounds = YES;
    
    
    [self.btnSelectDiffShow setBackgroundColor:myAppdelegate.colorforall];
    [myAppdelegate.objRootView ChangeColor:btnSettings];
    
    
    [clsDatabase checkDataBase];
    ShowName.text = myAppdelegate.ShowName;//@"HITEC";
    [self FetchAllLeads];
    
    [clsDatabase alterDB:@"leads_offline" fldname:@"leadtype"];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    // Do any additional setup after loading the view.
}



- (void) viewWillAppear:(BOOL)animated
{
    NSLog(@"asdsadsadsad");
}
- (void) FetchAllLeads
{
    
    NSLog(@"Fetchleadssss");
    if([self.arrayFeeds count] > 0)
    {
        [self.arrayFeeds removeAllObjects];
    }
    
    if([self.arrayShows count] > 0)
    {
        [self.arrayShows removeAllObjects];
    }
    
    arrayShows = [[NSMutableArray alloc] init];
    
    NSString *temp2=[NSString stringWithFormat:@"SELECT * FROM leads_offline where Show_id='%@' order by Id desc",myAppdelegate.ShowId];
    sqlite3_stmt *dataRows1=[clsDatabase getDataset:[temp2 UTF8String]];
    
    while(sqlite3_step(dataRows1) == SQLITE_ROW)
    {
        if (leaddataofflineforleftmenu==nil) {
            
        }else {
            leaddataofflineforleftmenu=nil;
        }
        leaddataofflineforleftmenu =[[LeadCustomClass alloc]init];
        
        leaddataofflineforleftmenu.fromOffline = @"1";
        leaddataofflineforleftmenu.ids =  [NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows1,0)];
        
        leaddataofflineforleftmenu.Surid   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,1)]];
        
        leaddataofflineforleftmenu.shwid =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,2)]];
        
        leaddataofflineforleftmenu.Fname   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,3)]];
        leaddataofflineforleftmenu.Fname   =  [self convertEntities:[NSString stringWithFormat:@"%@",leaddataofflineforleftmenu.Fname] ];
        
        leaddataofflineforleftmenu.Lname =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,4)]];
        
        leaddataofflineforleftmenu.Lname   =  [self convertEntities:[NSString stringWithFormat:@"%@",leaddataofflineforleftmenu.Lname] ];
        
        leaddataofflineforleftmenu.Email   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,5)]];
        
        leaddataofflineforleftmenu.Email   =  [self convertEntities:[NSString stringWithFormat:@"%@",leaddataofflineforleftmenu.Email] ];
        
        leaddataofflineforleftmenu.Phone =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,6)]];
        
        leaddataofflineforleftmenu.Jtitle   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,7)]];
        
        leaddataofflineforleftmenu.Jtitle   =  [self convertEntities:[NSString stringWithFormat:@"%@",leaddataofflineforleftmenu.Jtitle] ];
        
        leaddataofflineforleftmenu.Notes =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,8)]];
        
        leaddataofflineforleftmenu.Notes   =  [self convertEntities:[NSString stringWithFormat:@"%@",leaddataofflineforleftmenu.Notes] ];
        
        leaddataofflineforleftmenu.Rsrcid   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,9)]];
        
        leaddataofflineforleftmenu.emailid =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,10)]];
        
        leaddataofflineforleftmenu.Bname   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,11)]];
        
        leaddataofflineforleftmenu.Bname   =  [self convertEntities:[NSString stringWithFormat:@"%@",leaddataofflineforleftmenu.Bname] ];
        
        leaddataofflineforleftmenu.Address =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,12)]];
        
        leaddataofflineforleftmenu.Address   =  [self convertEntities:[NSString stringWithFormat:@"%@",leaddataofflineforleftmenu.Address] ];
        
        leaddataofflineforleftmenu.Zcode   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,13)]];
        
        leaddataofflineforleftmenu.surdata =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,14)]];
        
        NSString *leadiddd =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,15)]];
        
        leaddataofflineforleftmenu.badgeresult =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,16)]];
//        leaddataofflineforleftmenu.leadtype =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,17)]];
      leaddataofflineforleftmenu.leadtype  =  ((char *)sqlite3_column_text(dataRows1, 17)) ? [NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1, 17)] : @"1";

        if(leadiddd !=NULL)
        {
            if(leadiddd.length >0)
                leaddataofflineforleftmenu.leadId = leadiddd;
            else
                leaddataofflineforleftmenu.leadId = @"0";
        }
        else
            leaddataofflineforleftmenu.leadId = @"0";
        
        [self.arrayFeeds addObject:leaddataofflineforleftmenu];
    }
    
    
    
    //    temp2=[NSString stringWithFormat:@"select L.*,GROUP_CONCAT(SA.Email_Id) as Email,GROUP_CONCAT(SA.Asset_Id) as Asset_Id from leads L left join send_assets SA on SA.Lead_Id=L.Id  where L.Show_Id=%@ group by SA.Lead_Id",myAppdelegate.ShowId];
    
//    temp2=[NSString stringWithFormat:@"select L.*,GROUP_CONCAT(DISTINCT SA.Email_Id) as Email,GROUP_CONCAT(DISTINCT SA.Asset_Id) as Asset_Id from leads L left join send_assets SA on SA.Lead_Id=L.Id  AND SA.deleted = '0' where L.Show_Id=%@  AND L.Id NOT IN (select Lead_Id from leads_offline) group by L.Id order by L.Id desc",myAppdelegate.ShowId];

    temp2=[NSString stringWithFormat:@"select L.*,GROUP_CONCAT(DISTINCT SA.Email_Id) as Email,GROUP_CONCAT(DISTINCT SA.Asset_Id) as Asset_Id from leads L left join send_assets SA on SA.Lead_Id=L.Id  AND SA.deleted = '0' AND  SA.Asset_Id in (SELECT Asset_Id from all_assets) where L.Show_Id=%@  AND L.Id NOT IN (select Lead_Id from leads_offline) group by L.Id order by L.Id desc",myAppdelegate.ShowId];
    
    
    
    
    dataRows1=[clsDatabase getDataset:[temp2 UTF8String]];
    
    while(sqlite3_step(dataRows1) == SQLITE_ROW)
    {
        //        if (leaddataoffline==nil) {
        //
        //        }else {
        //            leaddataoffline=nil;
        //        }
        leaddataofflineforleftmenu =[[LeadCustomClass alloc]init];
        
        leaddataofflineforleftmenu.fromOffline = @"0";
        leaddataofflineforleftmenu.leadId =  [NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows1,0)];
        
        leaddataofflineforleftmenu.Surid   =  [NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows1,1)];
        
        leaddataofflineforleftmenu.shwid =  [NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows1,2)];
        
        leaddataofflineforleftmenu.Fname   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,3)]];
        leaddataofflineforleftmenu.Fname   =  [self convertEntities:[NSString stringWithFormat:@"%@",leaddataofflineforleftmenu.Fname] ];
        leaddataofflineforleftmenu.Lname =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,4)]];
        
        leaddataofflineforleftmenu.Lname   =  [self convertEntities:[NSString stringWithFormat:@"%@",leaddataofflineforleftmenu.Lname] ];
        
        leaddataofflineforleftmenu.Email   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,5)]];
        
        leaddataofflineforleftmenu.Email   =  [self convertEntities:[NSString stringWithFormat:@"%@",leaddataofflineforleftmenu.Email] ];
        
        leaddataofflineforleftmenu.Phone =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,6)]];
        
        leaddataofflineforleftmenu.Bname   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,7)]];
        
        leaddataofflineforleftmenu.Bname   =  [self convertEntities:[NSString stringWithFormat:@"%@",leaddataofflineforleftmenu.Bname] ];
        
        leaddataofflineforleftmenu.Jtitle   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,8)]];
        
        leaddataofflineforleftmenu.Jtitle   =  [self convertEntities:[NSString stringWithFormat:@"%@",leaddataofflineforleftmenu.Jtitle] ];
        
        leaddataofflineforleftmenu.Address =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,9)]];
        
        leaddataofflineforleftmenu.Address   =  [self convertEntities:[NSString stringWithFormat:@"%@",leaddataofflineforleftmenu.Address] ];
        
        leaddataofflineforleftmenu.Zcode   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,12)]];
        
        leaddataofflineforleftmenu.Notes =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,13)]];
        
        leaddataofflineforleftmenu.Notes   =  [self convertEntities:[NSString stringWithFormat:@"%@",leaddataofflineforleftmenu.Notes] ];
        
        leaddataofflineforleftmenu.badgeresult =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,14)]];
        
        leaddataofflineforleftmenu.leadtype  =  ((char *)sqlite3_column_text(dataRows1, 15)) ? [NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1, 15)] : @"1";
        
        leaddataofflineforleftmenu.emailid =  ((char *)sqlite3_column_text(dataRows1, 18)) ? [NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1, 18)] : nil;
        
        //        leaddataoffline.Rsrcid   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,15)]];
        leaddataofflineforleftmenu.Rsrcid   =  ((char *)sqlite3_column_text(dataRows1, 19)) ? [NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1, 19)] : nil;
        
        
        
        
        
        
        //        leaddataoffline.surdata =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,14)]];
        
        [self.arrayFeeds addObject:leaddataofflineforleftmenu];
    }
    
    
    
    leadcount.text = [NSString stringWithFormat:@"Lead Count: %d",[self.arrayFeeds count]];
    [self.tableView reloadData];
    
    
     temp2=[NSString stringWithFormat:@"SELECT Show_id,Name,Badge_Scan,Attract_Loop FROM tradeshow"];
    
    dataRows1=[clsDatabase getDataset:[temp2 UTF8String]];
    
    while(sqlite3_step(dataRows1) == SQLITE_ROW)
    {
        Showlist *slist = [[Showlist alloc] init];
        
        slist.ShwId    = [NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows1,0)];
        
        slist.ShwName  = [self convertEntities:[NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,1)]]];
        
        slist.ShwAtrct = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,2)]];
        
        slist.ShwBscan = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,3)]];
        
        [arrayShows addObject:slist];
    }
    
    ShowName.text = myAppdelegate.ShowName;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if(tableView.tag == 100)
        return arrayShows.count;
    else
    return self.arrayFeeds.count;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    NSLog(@"Width:%f Table Width:%f", self.view.frame.size.width, self.tableView.frame.size.width);
    NSLog(@"Selected Row: %ld", (long)indexPath.row);
    
    if(tableView.tag == 100)
    {
     [self dismissViewControllerAnimated:NO completion:nil];
        [self dismissModalViewControllerAnimated:YES];
        
        shwlst = [arrayShows objectAtIndex:indexPath.row];
        
        if(![shwlst.ShwId isEqualToString:myAppdelegate.ShowId])
        {
            [myAppdelegate HideLeftMenu];
            if([[Reachability sharedReachability] internetConnectionStatus] != NotReachable)
            {
            myAppdelegate.tmpshwid = shwlst.ShwId;
            myAppdelegate.tmpshwnme = shwlst.ShwName;

            [myAppdelegate.objRootView ShowAlertView:@"Do you really want to change the event? This will remove all data for currently selected event and new event data will be downloaded." alertType:13];
            }
            else
            {
                [myAppdelegate.objRootView ShowAlertView:@"Network is not available \n Please try again later" alertType:14];
                
            }
            
        }
        
    }
    else
    {
    UIButton *butval= [UIButton buttonWithType:UIButtonTypeRoundedRect];
    myAppdelegate.activeView=0;
    butval.tag = indexPath.row;
    
    [self Click_iCloudEdit:butval];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"cellSideMenu"; // [self.arrayFeeds objectAtIndex:indexPath.row];
    cellSideMenu *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
//    cell.lblTitle.text=[self.arrayFeeds objectAtIndex:indexPath.row];

    if(tableView.tag == 100)
    {
        [cell.lblactTitle setHidden:YES];
        cell.lblactTitle.text = @"[ACTIVE EVENT]";
        cell.lblactTitle.font = [UIFont fontWithName:@"Montserrat-Italic" size:12.0];
        shwlst = [arrayShows objectAtIndex:indexPath.row];
        cell.lblTitle.text=shwlst.ShwName;
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        
        if([myAppdelegate.ShowId isEqualToString:shwlst.ShwId])
        {
            [cell.lblactTitle setHidden:NO];
            cell.lblTitle.frame = CGRectMake(cell.lblTitle.frame.origin.x, cell.lblTitle.frame.origin.y, 150, cell.lblTitle.frame.size.height);
             
           
        }
        else
        {
            [cell.lblactTitle setHidden:YES];
            cell.lblTitle.frame = CGRectMake(cell.lblTitle.frame.origin.x, cell.lblTitle.frame.origin.y, 250, cell.lblTitle.frame.size.height);
        }
    }
    else
    {

        cell.lblactTitle.font = [UIFont fontWithName:@"Montserrat-Italic" size:13];
        cell.lblactTitle.text = @"[ACTIVE LEAD]";
    leaddataofflineforleftmenu =  [self.arrayFeeds objectAtIndex:indexPath.row];
    NSString *title= [NSString stringWithFormat:@"%@ %@",leaddataofflineforleftmenu.Fname, leaddataofflineforleftmenu.Lname];
    cell.lblTitle.text=title;
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
    cell.btnCloud.tag=indexPath.row;
    cell.btnEdit.tag=indexPath.row;
    
        
        
    [cell.btnCloud addTarget:self action:@selector(Click_iCloud:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnEdit addTarget:self action:@selector(Click_iCloudEdit:) forControlEvents:UIControlEventTouchUpInside];
    
//    if (indexPath.row>5) {
//        [cell.btnCloud setSelected:YES];
//    }
    
    if([leaddataofflineforleftmenu.fromOffline isEqualToString:@"1"])
    {
        [cell.btnCloud setSelected:YES];
        [cell.btnCloud setImage:[UIImage imageNamed:@"cloudIconRed.png"] forState:UIControlStateNormal];
        
    }
    else
    {
        [cell.btnCloud setSelected:NO];
        [cell.btnCloud setImage:[UIImage imageNamed:@"cloudIcon.png"] forState:UIControlStateNormal];
    }
    
    [cell.lblactTitle setHidden:YES];
    NSLog(@"myAppdelegate.editledid=%@",myAppdelegate.editledid);
    NSLog(@"myAppdelegate.editledid=%f",cell.lblactTitle.frame.size.width);
    if(![myAppdelegate.editledid isEqualToString:@"-1"])
    {
        if([leaddataofflineforleftmenu.fromOffline isEqualToString:@"1"])
        {
            if([leaddataofflineforleftmenu.ids isEqualToString:myAppdelegate.editledid])
            {
//                [cell.lblTitle setTextColor:[UIColor colorWithRed:0.0/255.0f green:140.0/255.0f blue:205.0/255.0f alpha:1.0]];
                 [cell.lblactTitle setHidden:NO];
//                [cell.lblactTitle setFont:[UIFont fontWithName:@"Montserrat-Italic" size:13]];
                cell.lblTitle.frame = CGRectMake(cell.lblTitle.frame.origin.x, cell.lblTitle.frame.origin.y, 110, cell.lblTitle.frame.size.height);
            }
            else
            {
//               [cell.lblTitle setTextColor:[UIColor colorWithRed:145.0/255.0f green:150.0/255.0f blue:157.0/255.0f alpha:1.0]];
                [cell.lblactTitle setHidden:YES];
               
                cell.lblTitle.frame = CGRectMake(cell.lblTitle.frame.origin.x, cell.lblTitle.frame.origin.y, 218, cell.lblTitle.frame.size.height);
            }
        }
        else
        {
            if([leaddataofflineforleftmenu.leadId isEqualToString:myAppdelegate.editledid])
            {
//                [cell.lblTitle setTextColor:[UIColor colorWithRed:0.0/255.0f green:140.0/255.0f blue:205.0/255.0f alpha:1.0]];
                  [cell.lblactTitle setHidden:NO];
//                [cell.lblactTitle setFont:[UIFont fontWithName:@"Montserrat-Italic" size:13]];
                cell.lblTitle.frame = CGRectMake(cell.lblTitle.frame.origin.x, cell.lblTitle.frame.origin.y, 110, cell.lblTitle.frame.size.height);
            }
            else
            {
//                [cell.lblTitle setTextColor:[UIColor colorWithRed:145.0/255.0f green:150.0/255.0f blue:157.0/255.0f alpha:1.0]];
                 [cell.lblactTitle setHidden:YES];
                cell.lblTitle.frame = CGRectMake(cell.lblTitle.frame.origin.x, cell.lblTitle.frame.origin.y, 218, cell.lblTitle.frame.size.height);
            }
        }
    }
    else
    {
//        [cell.lblTitle setTextColor:[UIColor colorWithRed:145.0/255.0f green:150.0/255.0f blue:157.0/255.0f alpha:1.0]];
        cell.lblTitle.frame = CGRectMake(cell.lblTitle.frame.origin.x, cell.lblTitle.frame.origin.y, 218, cell.lblTitle.frame.size.height);
    }
//        [myAppdelegate.objRootView ChangeColor:cell.btnCloud];
//        [myAppdelegate.objRootView ChangeColor:cell.btnEdit];
    }
    return cell;
}

-(IBAction)Click_iCloud:(id)sender{
    
    UIButton *btn=(UIButton *)sender;
    
    if([btn isSelected])
    {
        NSLog(@"Need To Upload");
        leaddataofflineforleftmenu = [arrayFeeds objectAtIndex:btn.tag];
        [myAppdelegate HideLeftMenu];
        myAppdelegate.objRootView.progView.hidden = false;
        [myAppdelegate.objRootView ShowAlertView:@"PLEASE WAIT..." alertType:4];
        [self performSelector:@selector(UploadToServer) withObject:nil afterDelay:0.5];
    }
    else
    {
        NSLog(@"No Need To Upload");
    }
    
    
    
}
-(void)UploadToServer
{
    
//    [self FetchJson:leaddataofflineforleftmenu.ids];
    
    myAppdelegate.showchange = true;
    [myAppdelegate FetchJson:leaddataofflineforleftmenu.ids];
    
   NSData * mydata =  [WebserviceAPI saveLeadData:leaddataofflineforleftmenu.Surid Version:@"1" Showid:myAppdelegate.ShowId First_Name:leaddataofflineforleftmenu.Fname Last_Name:leaddataofflineforleftmenu.Lname Email:leaddataofflineforleftmenu.Email Phone:leaddataofflineforleftmenu.Phone Job_Function:leaddataofflineforleftmenu.Jtitle Fields:myAppdelegate.surveyjson Notes:leaddataofflineforleftmenu.Notes Email_Id:leaddataofflineforleftmenu.emailid Asset_Id:leaddataofflineforleftmenu.Rsrcid Business_Name:leaddataofflineforleftmenu.Bname Address:leaddataofflineforleftmenu.Address Zip_Code:leaddataofflineforleftmenu.Zcode IDs:leaddataofflineforleftmenu.ids LeadID:leaddataofflineforleftmenu.leadId  Fresult:leaddataofflineforleftmenu.badgeresult leadtype:leaddataofflineforleftmenu.leadtype];
    
    if(mydata == nil)
    {
        [myAppdelegate.objRootView ShowAlertView:@"The cloud cannot be reached at this time. The lead information has been stored locally and will sync when the connection is restored." alertType:7];
        
    }
    else
    {
        XmlParser *parser=[[XmlParser alloc] init];
        [parser parseData:mydata];
        if([parser.ResponseCode isEqualToString:@"404"])
        {
            NSString *myDelquery = [NSString stringWithFormat:@"delete  from  survey_submitted_data_offline where Id = %@",leaddataofflineforleftmenu.ids];
            [self createDB:myDelquery];
            
            myDelquery = [NSString stringWithFormat:@"delete  from  leads_offline where Id = %@",leaddataofflineforleftmenu.ids];
            [self createDB:myDelquery];
            
            
            [myAppdelegate.objRootView ShowAlertView:@"It seems this lead is not present in the system." alertType:8];
            [self performSelector:@selector(CallHideAlert) withObject:nil afterDelay:0.3];
        }
        else
        {
            myAppdelegate.objRootView.progView.hidden = true;
            myAppdelegate.objRootView.sucsView.hidden = false;
            [myAppdelegate.objRootView HideAlertView];
            [self performSelector:@selector(HideAlert) withObject:nil afterDelay:0.1];
        }
    }
    
    myAppdelegate.showchange = false;
    
}

-(void) FetchJson:(NSString *)idval
{
    
    NSMutableDictionary *nameElements;
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    NSMutableArray *listOfVals;
    NSMutableArray *listOfValsother;
    NSMutableArray *listofother;
    listOfVals = [[NSMutableArray alloc] init];
    listOfValsother = [[NSMutableArray alloc] init];
    listofother = [[NSMutableArray alloc] init];
    myAppdelegate.surveyjson = @"";
    int k = 0;
    int kl = 0;
    
    NSString *temp22=[NSString stringWithFormat:@"select Label from field_options where Other_Flag = 1"];
    sqlite3_stmt *dataRows11=[clsDatabase getDataset:[temp22 UTF8String]];
    
    while(sqlite3_step(dataRows11) == SQLITE_ROW)
    {
        NSString *value = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows11,0)]];
        
        [listofother addObject:value];
    }

    
    NSString *temp2=[NSString stringWithFormat:@"select (Field_Name || Field_Value) as Value from survey_submitted_data_offline where Lead_Id='%@' ",idval];
    sqlite3_stmt *dataRows1=[clsDatabase getDataset:[temp2 UTF8String]];
    
    while(sqlite3_step(dataRows1) == SQLITE_ROW)
    {
        
        NSString *value = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,0)]];
        
        NSString *Field_Name = [value substringToIndex:9];
        NSString *Field_Value = [value substringFromIndex:9];
        
        //Field_Value = [Field_Value URLEncodedString];
        
//        if(!([Field_Value isEqualToString:@"Additional Details"] || [Field_Value isEqualToString:@"Other (Please Specify)"]))
        if(!([listofother containsObject:Field_Value]))
        {
            if(Field_Value.length < 2)
            {
                nameElements = [[NSMutableDictionary alloc] init ];
                [nameElements setObject:[Field_Value URLEncodedString] forKey:Field_Name];
                [listOfVals insertObject:nameElements atIndex:k];
                k++;
            }
            else
            {
                if(([[Field_Value substringToIndex:2] isEqualToString:@"ot"]))
                {
                    nameElements = [[NSMutableDictionary alloc] init ];
                    [nameElements setObject:[[Field_Value substringFromIndex:2]URLEncodedString] forKey:Field_Name];
                    [listOfValsother insertObject:nameElements atIndex:kl];
                    kl++;
                }
                else
                {
                    nameElements = [[NSMutableDictionary alloc] init ];
                    [nameElements setObject:[Field_Value URLEncodedString] forKey:Field_Name];
                    [listOfVals insertObject:nameElements atIndex:k];
                    k++;
                }
            }
        }
        
    }
    
    [dict setObject:listOfVals forKey:@"Survey_Detail"];
    [dict setObject:listOfValsother forKey:@"Other_Detail"];
    
    NSError *error;
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:nil error:&error];
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        myAppdelegate.surveyjson = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    
    
    //    surveyjson = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:dict options:1 error:nil] encoding:4];
    //    surveyjson = [surveyjson stringByReplacingOccurrencesOfString:@" "
    //                                          withString:@""];
    
    NSLog(@"%@",myAppdelegate.surveyjson);
    if(myAppdelegate.surveyjson == NULL)
        myAppdelegate.surveyjson = @"";
    
}

-(void) createDB:(NSString *)dbnamepath
{
    NSString *docsDir;
    NSArray *dirPaths;
    static sqlite3 *database = nil;
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    NSString *databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"ComCast.sqlite"]];
    
    
    
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
    {
        
        
        //        const char *dbpath = [databasePath UTF8String];
        
        
        //if (sqlite3_open(dbpath, &contactDB) == SQLITE_OK) {
        char *errMsg;
        const char *sql_stmt = [dbnamepath UTF8String];
        
        if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
            //NSLog(@"Doneeeeee");
        }
        sqlite3_close(database);
        //}
        //else {
        //
        //		}
    }
}


-(void)HideAlert
{
    myAppdelegate.objRootView.sucsView.hidden = true;
    [myAppdelegate.objRootView ShowAlertView:@"Your information was successfully uploaded to the Cloud." alertType:3];
    [self FetchAllLeads];
    
}
-(void)CallHideAlert
{
myAppdelegate.objRootView.progView.hidden = true;
[myAppdelegate.objRootView HideAlertView];
}
- (NSString*)convertEntities:(NSString*)string
{
    
    NSString    *returnStr = nil;
    
    if( string )
    {
        returnStr = [ string stringByReplacingOccurrencesOfString:@"&amp;" withString: @"&"  ];
        
        returnStr = [ returnStr stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""  ];
        
        returnStr = [ returnStr stringByReplacingOccurrencesOfString:@"&#x27;" withString:@"'"  ];
        
        returnStr = [ returnStr stringByReplacingOccurrencesOfString:@"&#x39;" withString:@"'"  ];
        
        returnStr = [ returnStr stringByReplacingOccurrencesOfString:@"&#x92;" withString:@"'"  ];
        
        returnStr = [ returnStr stringByReplacingOccurrencesOfString:@"&#x96;" withString:@"'"  ];
        
        returnStr = [ returnStr stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"  ];
        
        returnStr = [ returnStr stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"  ];
        
        returnStr = [ [ NSString alloc ] initWithString:returnStr ];
    }
    
    return returnStr;
}

-(IBAction)Click_iCloudEdit:(id)sender{
    
    
    UIButton *btn=(UIButton *)sender;
    globalbutoon = btn;
    
    if(myAppdelegate.edited)
    {
        [myAppdelegate HideLeftMenu];
        if(([myAppdelegate.editledid isEqualToString:@"-1"]))
        {
            [myAppdelegate.objRootView ShowAlertView:@"Your progress for this lead has not been synced and by continuing you will lose progress" alertType:10];
        }
        else
        {
          [myAppdelegate.objRootView ShowAlertView:@"The edits you have made have not been synced and by continuing you will not save the edits to the synced lead" alertType:9];
        }
    }
    else
    {
         [self performoneditlead:sender];
    }
    //NSLog(@"HERE%d",[myAppdelegate.selectassetdataarray count]);
}

-(IBAction) SetForEdit:(id)sender
{
    if(myAppdelegate.edited)
    {
        [myAppdelegate HideLeftMenu];
        if(([myAppdelegate.editledid isEqualToString:@"-1"]))
        {
            [myAppdelegate.objRootView ShowAlertView:@"Your progress for this lead has not been synced and by continuing you will lose progress" alertType:12];
        }
        else
        {
            [myAppdelegate.objRootView ShowAlertView:@"The edits you have made have not been synced and by continuing you will not save the edits to the synced lead" alertType:11];
        }
    }
    else
    {
        [myAppdelegate HideLeftMenu];
        myAppdelegate.edited = false;
        myAppdelegate.previousemail = @"";
        myAppdelegate.activeView=-1;
        myAppdelegate.editledid = @"-1";
        myAppdelegate.scanfname = @"";
        myAppdelegate.scanlname = @"";
        myAppdelegate.scanemail = @"";
        myAppdelegate.scanphone = @"";
        myAppdelegate.scanbname = @"";
        myAppdelegate.scanjtitle = @"";
        myAppdelegate.scanaddr = @"";
        myAppdelegate.scanzcode = @"";
        myAppdelegate.notesval = @"";
        myAppdelegate.leaddataoffline.Fname   = @"";
        myAppdelegate.leaddataoffline.Lname   = @"";
        myAppdelegate.leaddataoffline.Email   = @"";
        myAppdelegate.leaddataoffline.Phone   = @"";
        myAppdelegate.leaddataoffline.Bname   = @"";
        myAppdelegate.leaddataoffline.Jtitle  = @"";
        myAppdelegate.leaddataoffline.Address = @"";
        myAppdelegate.leaddataoffline.Zcode   = @"";
        myAppdelegate.leaddataoffline.leadtype = @"1";
        myAppdelegate.UpdateId =  @"0";
        myAppdelegate.completeresult = @"";
        myAppdelegate.curentleadid = @"0";
        
        myAppdelegate.leaddataoffline.Notes = @"";
        myAppdelegate.firsttime = false;
        if([myAppdelegate.emailsArray count] > 0)
            [myAppdelegate.emailsArray removeAllObjects];
        
        [myAppdelegate.emailsArray addObject:@""];
        [myAppdelegate.emailsArray addObject:@""];
        [myAppdelegate.emailsArray addObject:@""];
        [myAppdelegate.emailsArray addObject:@""];
        
        if([myAppdelegate.selectassetdataarray count] > 0)
        {
            [myAppdelegate.selectassetdataarray removeAllObjects];
        }
        if([myAppdelegate.formvalarray count] > 0)
        {
            [myAppdelegate.formvalarray removeAllObjects];
        }
        if([myAppdelegate.formidarray count] > 0)
        {
            [myAppdelegate.formidarray removeAllObjects];
        }
        [myAppdelegate.objVCLeftMenu FetchAllLeads];
        // [myAppdelegate.navcontroller popToRootViewControllerAnimated:YES];
        
        
        
        
        [myAppdelegate.objVCLeftMenu FetchAllLeads];
        //[self ScanBadge];
        
        NSUserDefaults *prefsp = [NSUserDefaults standardUserDefaults];
        NSString *Badge_Scan = [prefsp valueForKey:@"Badge_Scan"];
        
        if([Badge_Scan isEqualToString:@"0"])
        {
            UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            Step1ViewController* step1 = (Step1ViewController*)[mainStoryBoard instantiateViewControllerWithIdentifier: @"step1ViewController"];
            [myAppdelegate.navcontroller pushViewController:step1 animated:NO];
        }
        else
        {
         [myAppdelegate.navcontroller popToRootViewControllerAnimated:YES];
        }
        
    }
}

-(void)performoneditlead:(id)sender
{
    myAppdelegate.edited = false;
    myAppdelegate.previousemail = @"";
    UIButton *btn=(UIButton *)sender;
    [self FetchAllLeads];
    [myAppdelegate HideLeftMenu];
    
    if([myAppdelegate.formvalarray count] > 0)
        [myAppdelegate.formvalarray removeAllObjects];
    
    if([myAppdelegate.formidarray count] > 0)
    {
        [myAppdelegate.formidarray removeAllObjects];
    }
    leaddataofflineforleftmenu =  [self.arrayFeeds objectAtIndex:btn.tag];
    
    if([leaddataofflineforleftmenu.fromOffline isEqualToString:@"1"])
    {
        myAppdelegate.editledid = leaddataofflineforleftmenu.ids;
        NSString *temp2=[NSString stringWithFormat:@"select Field_Name, Field_Value from survey_submitted_data_offline where Lead_Id='%@' ",leaddataofflineforleftmenu.ids];
        sqlite3_stmt *dataRows1=[clsDatabase getDataset:[temp2 UTF8String]];
        
        while(sqlite3_step(dataRows1) == SQLITE_ROW)
        {
            
//            NSString *value = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,0)]];
//            value = [self convertEntities:value];
//            [myAppdelegate.formvalarray addObject:value];
            
            NSString *fldname = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,0)]];
            
            NSString *fldvalue = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,1)]];
            
            
            fldvalue = [self convertEntities:fldvalue];
            [myAppdelegate.formvalarray addObject:[NSString stringWithFormat:@"%@%@%@",fldname,myAppdelegate.seprator,fldvalue]];
            
        }
        
    }
    else
    {
        myAppdelegate.editledid = leaddataofflineforleftmenu.leadId;
        
        NSString *surId = @"";
        NSString *temp0=[NSString stringWithFormat:@"SELECT Survey_Id FROM survey_linking where Show_Id = %@",myAppdelegate.ShowId];

        sqlite3_stmt *dataRows0=[clsDatabase getDataset:[temp0 UTF8String]];
        
        while(sqlite3_step(dataRows0) == SQLITE_ROW)
        {
            surId =  [NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows0,0)];
        }
//        SELECT Survey_Id FROM survey_linking where Show_Id = 106
        
//        match it with selected lead id
        
        if([leaddataofflineforleftmenu.Surid isEqualToString:surId])
        {
        NSString *temp2=[NSString stringWithFormat:@"select Field_Name,  Field_Value from survey_submitted_data where Lead_Id= %@ and Other_Flag = 0",leaddataofflineforleftmenu.leadId];
        sqlite3_stmt *dataRows1=[clsDatabase getDataset:[temp2 UTF8String]];
        
        while(sqlite3_step(dataRows1) == SQLITE_ROW)
        {
            
            NSString *fldname = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,0)]];
            
            NSString *fldvalue = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,1)]];
            
            
            fldvalue = [self convertEntities:fldvalue];
            [myAppdelegate.formvalarray addObject:[NSString stringWithFormat:@"%@%@%@",fldname,myAppdelegate.seprator,fldvalue]];
            
//            [myAppdelegate.formvalarray addObject:value];
            
        }
        
        temp2=[NSString stringWithFormat:@"SELECT survey_fields.Field_Name_Id, FO.Label,SBD.Field_Value FROM `survey_fields` left join field_options as FO on FO.Field_Id=survey_fields.Field_Id left JOIN survey_linking as SL on SL.Survey_Id= survey_fields.Survey_Id LEFT JOIN survey_submitted_data SBD on lower(SBD.Field_Name)=lower(survey_fields.Field_Name_Id) WHERE SBD.Other_Flag=1  AND FO.Other_Flag=1 AND SL.Show_Id=%@ AND SBD.Lead_Id=%@",myAppdelegate.ShowId,leaddataofflineforleftmenu.leadId];
        
        dataRows1=[clsDatabase getDataset:[temp2 UTF8String]];
        
        while(sqlite3_step(dataRows1) == SQLITE_ROW)
        {
            
            NSString *fldname = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,0)]];
            
            NSString *fldlbl = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,1)]];
            
            NSString *fldval = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,2)]];
            
            fldlbl = [self convertEntities:fldlbl];
            fldval = [self convertEntities:fldval];
            
            [myAppdelegate.formvalarray addObject:[NSString stringWithFormat:@"%@%@%@",fldname,myAppdelegate.seprator,fldlbl]];
            [myAppdelegate.formvalarray addObject:[NSString stringWithFormat:@"%@%@ot%@",fldname,myAppdelegate.seprator,fldval]];
            
        }
        
    }
    }
    
    NSLog(@"Formval=%@",myAppdelegate.formvalarray);
    if(leaddataofflineforleftmenu.ids != nil)
        myAppdelegate.UpdateId = leaddataofflineforleftmenu.ids;
    else
        myAppdelegate.UpdateId = @"0";
    
    myAppdelegate.curentleadid = leaddataofflineforleftmenu.leadId;
    myAppdelegate.leaddataoffline = leaddataofflineforleftmenu;
    
    myAppdelegate.notesval = leaddataofflineforleftmenu.Notes;
    
    //    if(myAppdelegate.activeView == 15)
    //    {
    if(myAppdelegate.preview != nil)
    {
        [myAppdelegate.preview dismissViewControllerAnimated:YES completion:nil];
    }
    //    }
    UIButton *butval= [UIButton buttonWithType:UIButtonTypeRoundedRect];
    myAppdelegate.activeView=0;
    butval.tag = 1;
    [myAppdelegate ShowViewController:butval];
    
    if([myAppdelegate.selectassetdataarray count] > 0)
    {
        [myAppdelegate.selectassetdataarray removeAllObjects];
    }
    
    if([myAppdelegate.leaddataoffline.Rsrcid stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0)
    {
        
    }
    else
    {
        NSArray *aArray = [myAppdelegate.leaddataoffline.Rsrcid componentsSeparatedByString:@","];
        myAppdelegate.selectassetdataarray =  [NSMutableArray arrayWithArray:aArray];
    }
    if([myAppdelegate.emailsArray count] > 0)
        [myAppdelegate.emailsArray removeAllObjects];
    
    if([myAppdelegate.leaddataoffline.emailid stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0)
    {
        [myAppdelegate.emailsArray addObject:@""];
        [myAppdelegate.emailsArray addObject:@""];
        [myAppdelegate.emailsArray addObject:@""];
        [myAppdelegate.emailsArray addObject:@""];
    }
    else
    {
        NSArray *aArray = [myAppdelegate.leaddataoffline.emailid componentsSeparatedByString:@","];
        
        myAppdelegate.emailsArray =  [NSMutableArray arrayWithArray:aArray];
        for(int i = [myAppdelegate.emailsArray count]; i<= 3; i++)
        {
            [myAppdelegate.emailsArray addObject:@""];
        }
    }
    if ([myAppdelegate.selectassetdataarray count] > 0) {
        [myAppdelegate.objRootView.btnBadge setHidden:NO];
    }else{
        [myAppdelegate.objRootView.btnBadge setHidden:YES];
    }
    
    [myAppdelegate.objRootView.btnBadge setTitle:[NSString stringWithFormat:@"%d", [myAppdelegate.selectassetdataarray count]] forState:UIControlStateNormal];
    
    [self.tableView reloadData];
    myAppdelegate.firsttime = true;


}
-(IBAction)Click_Continue:(id)sender{
    
    [self performoneditlead:sender];
}
-(IBAction)Click_Settings:(id)sender{
    
    UIButton *btnSender=sender;
    
    //if ([btnSender isSelected]) {
        if ([[btnSettings titleForState:UIControlStateNormal] isEqualToString:@"Close Settings"]) {
            
        
        
        CGRect imageFrame = self.viewSettings.frame;
        imageFrame.origin.y=self.view.frame.size.height-self.viewSettings.frame.size.height;
        self.viewSettings.frame=imageFrame;
        imageFrame = self.viewSettings.frame;
        imageFrame.origin.y = self.view.frame.size.height+self.viewSettings.frame.size.height;;
        
        [UIView animateWithDuration:0.7
                              delay:0.6
                            options: UIViewAnimationCurveEaseOut
                         animations:^{
                             self.viewSettings.frame = imageFrame;
                             
                             
                             CATransition *transition = [CATransition animation];
                             transition.type = kCATransitionFade;
                             transition.duration = 1.0;
                             [btnSender.layer addAnimation:transition forKey:kCATransition];
                             [btnSender setTitle:@"Settings" forState:UIControlStateNormal];
                             //[btnSender setTitleColor:newColor forState:UIControlStateNormal];
                             [btnSender setSelected:NO];
                             
                             
                             
                         }
                         completion:^(BOOL finished){
                             
//                             CATransition *transition = [CATransition animation];
//                             transition.type = kCATransitionFade;
//                             transition.duration = 0.3;
//                             [btnSender.layer addAnimation:transition forKey:kCATransition];
//                             [btnSender setTitle:@"Settings" forState:UIControlStateNormal];
//                             //[btnSender setTitleColor:newColor forState:UIControlStateNormal];
//                              [btnSender setSelected:NO];
                             
                             
                            
                             
                             [myAppdelegate.objRootView ChangeColor:btnSettings];                             NSLog(@"Done!");
                         }];
        
        
        
        //[self.viewSettings setHidden:YES];
        
    }else{
        //[btnSender setSelected:YES];
        [self.viewSettings setHidden:NO];
        
        CGRect imageFrame = self.viewSettings.frame;
        imageFrame.origin.y=self.view.frame.size.height+self.viewSettings.frame.size.height;
        self.viewSettings.frame=imageFrame;
        imageFrame = self.viewSettings.frame;
        imageFrame.origin.y = self.view.frame.size.height-self.viewSettings.frame.size.height;;
        
        [UIView animateWithDuration:0.7
                              delay:0.0
                            options: UIViewAnimationCurveEaseOut
                         animations:^{
                             self.viewSettings.frame = imageFrame;
                             
                             CATransition *transition = [CATransition animation];
                             transition.type = kCATransitionFade;
                             transition.duration = 1.0;
                             [btnSender.layer addAnimation:transition forKey:kCATransition];
                             [btnSender setTitle:@"Close Settings" forState:UIControlStateNormal];
                             //[btnSender setTitleColor:newColor forState:UIControlStateNormal];
                             [btnSender setSelected:YES];
                             
                             
                             
                             
                         }
                         completion:^(BOOL finished){
                             
//                             [UIView transitionWithView:btnSender duration:1 options:UIViewAnimationOptionTransitionCurlUp animations:^{
//                                 
//                                 [btnSender setSelected:YES];
//                                 [btnSender setTitle:@"Close Settings" forState:UIControlStateNormal];
//                                 [btnSender setTitle:@"Close Settings" forState:UIControlStateSelected];
//                                 
//                             } completion:nil];
//                             
                            
//                             [btnSender setTitle:@"Close Settings" forState:UIControlStateNormal];
//                             [btnSender setSelected:YES];
                             NSLog(@"Done!");
                             [myAppdelegate.objRootView ChangeColor:btnSettings];
                         }];
        
        
        
    }
    
   
    
    
   
}
-(IBAction)Click_DfrShow:(id)sender
{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    // grab the view controller we want to show
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"VCLeftMenupopover"];
    
    // present the controller
    // on iPad, this will be a Popover
    // on iPhone, this will be an action sheet
    controller.modalPresentationStyle = UIModalPresentationPopover;
    [self presentViewController:controller animated:YES completion:nil];
    
    // configure the Popover presentation controller
    UIPopoverPresentationController *popController = [controller popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionDown;
    popController.backgroundColor = [UIColor colorWithRed:167.0/255.0 green:170.0/255.0 blue:174.0/255.0 alpha:1.0];
    popController.delegate = self;
    
    // in case we don't have a bar button as reference
    
    
    
    popController.sourceView = self.view;
    popController.sourceRect = CGRectMake(100, 825, 185, 10);
}
-(IBAction)Click_AutoSync:(id)sender{
//    UIButton *btnSender=sender;
//    
//    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//    if ([autosyncbut isSelected]) {
//        [autosyncbut setSelected:NO];
//        self.btnsel.hidden = YES;
//        [prefs setObject:@"0" forKey:@"startupsync"];
//        
//    }else{
//        [autosyncbut setSelected:YES];
//        self.btnsel.hidden = NO;
//        [prefs setObject:@"1" forKey:@"startupsync"];
//        [myAppdelegate.objRootView ChangeColor:self.btnsel];
//        
//    }
//    
//    [prefs synchronize];
}

//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    
//    UITouch *touch= [touches anyObject];
//    NSLog(@"%@ --%@", touch.view, NSStringFromClass(touch.view));
//    
//    
//     CGPoint touchPoint = [touch locationInView:self.view];
//    
//    if (touchPoint.y<self.view.frame.size.height-384) {
//        if ([btnSettings isSelected]) {
//            [self Click_Settings:btnSettings];
//        }
//    }
//    //if( [touch.view isKindOfClass:[ViewController class]] ){
//  
//}
@end
