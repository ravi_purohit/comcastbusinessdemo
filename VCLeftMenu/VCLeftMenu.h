//
//  VCLeftMenu.h
//  ComcastBusiness
//
//  Created by Admin on 5/11/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeadCustomClass.h"
#import "clsDatabase.h"
#import "Showlist.h"
@interface VCLeftMenu : UIViewController{
NSMutableArray *arrayFeeds;
    
    LeadCustomClass *leaddataofflineforleftmenu;
    Showlist *shwlst;
//    NSString *editleadidsss;
}

@property(nonnull,strong)NSMutableArray *arrayFeeds;
@property(nonnull,strong)NSMutableArray *arrayShows;
@property(nonnull,retain)IBOutlet UITableView *tableView;
@property(nonnull,retain)IBOutlet UILabel *leadcount;
@property(nonnull,retain)IBOutlet UILabel *ShowName;
@property(nonnull,strong)IBOutlet UIView *viewSettings, *viewHamburger, *viewback;
@property(nonnull,strong)IBOutlet UIButton *btnSelectDiffShow, *btnHideHamburger, *btnSettings, *globalbutoon,*newleadbutoon,*btnsel,*autosyncbut;


@property(nonatomic,strong) LeadCustomClass *leaddataofflineforleftmenu;
@property(nonatomic,strong) Showlist *shwlst;

-(IBAction)Click_Settings:(id)sender;
-(IBAction)Click_DfrShow:(id)sender;
-(IBAction)Click_AutoSync:(id)sender;
-(IBAction)Click_Continue:(id)sender;
-(IBAction) SetForEdit:(id)sender;
- (void) FetchAllLeads;

@end
