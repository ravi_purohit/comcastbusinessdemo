//
//  SidebarTableViewController.m
//  SidebarDemo
//
//  Created by Simon Ng on 10/11/14.
//  Copyright (c) 2014 AppCoda. All rights reserved.
//

#import "SidebarTableViewController.h"
#import "SWRevealViewController.h"
//#import "PhotoViewController.h"
#import "cellSideMenu.h"

@interface SidebarTableViewController ()

@end

@implementation SidebarTableViewController {
    
}

@synthesize tableView,arrayFeeds,viewSettings,btnSelectDiffShow,viewHamburger,btnHideHamburger,leaddataoffline;

- (void)viewDidLoad {
    
    //[self.tableView setBackgroundColor:[UIColor clearColor]];
    CGRect imageFrame = self.viewSettings.frame;
    imageFrame.origin.y=1024.0;
    self.viewSettings.frame=imageFrame;
    
    self.viewHamburger.backgroundColor= myAppdelegate.hamburgerBGColor;
    
    self.btnSelectDiffShow.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.btnSelectDiffShow.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    
    [super viewDidLoad];
    self.arrayFeeds=[[NSMutableArray alloc] init];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
//    [self.arrayFeeds addObject:@"John Doe"];
    
    //menuItems = @[@"title", @"news", @"comments", @"map", @"calendar", @"wishlist", @"bookmark", @"tag"];
}
- (void) FetchAllLeads
{
    
    NSString *temp2=[NSString stringWithFormat:@"SELECT * FROM leads_offline"];
    sqlite3_stmt *dataRows1=[clsDatabase getDataset:[temp2 UTF8String]];
    
    while(sqlite3_step(dataRows1) == SQLITE_ROW)
    {
        if (leaddataoffline==nil) {
            
        }else {
            leaddataoffline=nil;
        }
        leaddataoffline =[[LeadCustomClass alloc]init];
        
        leaddataoffline.ids =  [NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows1,0)];
        
        leaddataoffline.Surid   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,1)]];
        
        leaddataoffline.shwid =  [NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows1,2)];
        
        leaddataoffline.Fname   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,3)]];
        
        leaddataoffline.Lname =  [NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows1,4)];
        
        leaddataoffline.Email   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,5)]];
        
        leaddataoffline.Phone =  [NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows1,6)];
        
        leaddataoffline.Jtitle   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,7)]];
        
        leaddataoffline.Notes =  [NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows1,8)];
        
        leaddataoffline.Rsrcid   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,9)]];
        
        leaddataoffline.emailid =  [NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows1,10)];
        
        leaddataoffline.Bname   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,11)]];
        
        leaddataoffline.Address =  [NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows1,12)];
        
        leaddataoffline.Zcode   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,13)]];
        
        leaddataoffline.surdata =  [NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows1,14)];
        
        leaddataoffline.leadtype  =  ((char *)sqlite3_column_text(dataRows1, 17)) ? [NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1, 17)] : @"1";

        
        [self.arrayFeeds addObject:leaddataoffline];
    }
    
    [self.tableView reloadData];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.arrayFeeds.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    NSLog(@"Width:%f Table Width:%f", self.view.frame.size.width, self.tableView.frame.size.width);
    NSLog(@"Selected Row: %ld", (long)indexPath.row);
    
//    UIViewController *viewControllerForPopover =
//    [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"VCMap"];
//    
//    
//        UINavigationController *nav =[[UINavigationController alloc] initWithRootViewController:viewControllerForPopover];
//        //UINavigationController *destViewController=nav;
//    
//    //UIViewController *toViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OtherViewControllerId"];
//    SWRevealViewControllerSeguePushController *segue = [[SWRevealViewControllerSeguePushController alloc] initWithIdentifier:@"" source:self destination:nav];
//    [self prepareForSegue:segue sender:nil];
//    [segue perform];
    
    
    
//    UINavigationController *nav =[[UINavigationController alloc] initWithRootViewController:viewControllerForPopover];
//    UINavigationController *destViewController=nav;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *CellIdentifier = @"cellSideMenu"; // [self.arrayFeeds objectAtIndex:indexPath.row];
    cellSideMenu *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    leaddataoffline =  [self.arrayFeeds objectAtIndex:indexPath.row];
    NSString *title= leaddataoffline.Fname;
    cell.lblTitle.text=title;

    
    cell.lblTitle.text=[self.arrayFeeds objectAtIndex:indexPath.row];
    
    cell.btnCloud.tag=indexPath.row;
    cell.btnEdit.tag=indexPath.row;
    
    [cell.btnCloud addTarget:self action:@selector(Click_iCloud:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnEdit addTarget:self action:@selector(Click_iCloudEdit:) forControlEvents:UIControlEventTouchUpInside];
    
    if (indexPath.row>5) {
        [cell.btnCloud setSelected:YES];
    }
    return cell;
}

-(IBAction)Click_iCloud:(id)sender{
    
}
-(IBAction)Click_iCloudEdit:(id)sender{
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
//    UIViewController *viewControllerForPopover =
//    [[UIStoryboard storyboardWithName:@"Main" bundle:nil]instantiateViewControllerWithIdentifier:@"VCMap"];
//    
//    
//    UINavigationController *nav =[[UINavigationController alloc] initWithRootViewController:viewControllerForPopover];
//    UINavigationController *destViewController=nav;
//    
//    // Set the title of navigation bar by using the menu items
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    UINavigationController *destViewController = (UINavigationController*)segue.destinationViewController;
    destViewController.title = [[self.arrayFeeds objectAtIndex:indexPath.row] capitalizedString];
    
    // Set the photo if it navigates to the PhotoView
//    if ([segue.identifier isEqualToString:@"showPhoto"]) {
//        UINavigationController *navController = segue.destinationViewController;
//        PhotoViewController *photoController = [navController childViewControllers].firstObject;
//        NSString *photoFilename = [NSString stringWithFormat:@"%@_photo", [self.arrayFeeds objectAtIndex:indexPath.row]];
//        photoController.photoFilename = photoFilename;
//    }
}


-(IBAction)Click_Settings:(id)sender{
    
    UIButton *btnSender=sender;
    
    if ([btnSender isSelected]) {
        
        
        CGRect imageFrame = self.viewSettings.frame;
        imageFrame.origin.y=self.view.frame.size.height-self.viewSettings.frame.size.height;
        self.viewSettings.frame=imageFrame;
        imageFrame = self.viewSettings.frame;
        imageFrame.origin.y = self.view.frame.size.height+self.viewSettings.frame.size.height;;
        
        [UIView animateWithDuration:1.0
                              delay:0.0
                            options: UIViewAnimationCurveEaseOut
                         animations:^{
                             self.viewSettings.frame = imageFrame;
                             
                         }
                         completion:^(BOOL finished){
                             [btnSender setSelected:NO];
                             NSLog(@"Done!");
                         }];
        
        
        
        //[self.viewSettings setHidden:YES];
        
    }else{
        //[btnSender setSelected:YES];
        [self.viewSettings setHidden:NO];
        
        CGRect imageFrame = self.viewSettings.frame;
        imageFrame.origin.y=self.view.frame.size.height+self.viewSettings.frame.size.height;
        self.viewSettings.frame=imageFrame;
        imageFrame = self.viewSettings.frame;
        imageFrame.origin.y = self.view.frame.size.height-self.viewSettings.frame.size.height;;
        
        [UIView animateWithDuration:1.0
                              delay:0.0
                            options: UIViewAnimationCurveEaseOut
                         animations:^{
                             self.viewSettings.frame = imageFrame;
                             
                         } 
                         completion:^(BOOL finished){
                             [btnSender setSelected:YES];
                             NSLog(@"Done!");
                         }];
        
        
        
    }
    
}
-(IBAction)Click_AutoSync:(id)sender{
    UIButton *btnSender=sender;
    
    if ([btnSender isSelected]) {
        [btnSender setSelected:NO];
        
        
    }else{
        [btnSender setSelected:YES];
        
    }
}
@end
