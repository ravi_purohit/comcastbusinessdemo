//
//  cellSideMenu.h
//  ComcastBusiness
//
//  Created by Admin on 5/8/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface cellSideMenu : UITableViewCell
{
    
}

@property(nonnull,retain)IBOutlet UILabel *lblTitle;
@property(nonnull,retain)IBOutlet UILabel *lblactTitle;
@property(nonnull,strong)IBOutlet UIButton *btnCloud, *btnEdit;

@end
