//
//  SidebarTableViewController.h
//  SidebarDemo
//
//  Created by Simon Ng on 10/11/14.
//  Copyright (c) 2014 AppCoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeadCustomClass.h"
#import "clsDatabase.h"
@interface SidebarTableViewController : UIViewController
{
    NSMutableArray *arrayFeeds;
    LeadCustomClass *leaddataoffline;
}

@property(nonnull,strong)NSMutableArray *arrayFeeds;
@property(nonnull,retain)IBOutlet UITableView *tableView;
@property(nonnull,strong)IBOutlet UIView *viewSettings, *viewHamburger;
@property(nonnull,strong)IBOutlet UIButton *btnSelectDiffShow, *btnHideHamburger;
@property(nonatomic,strong) LeadCustomClass *leaddataoffline;

-(IBAction)Click_Settings:(id)sender;
-(IBAction)Click_AutoSync:(id)sender;
- (void) FetchAllLeads;

@end
