//
//  AppDelegate.m
//  ComcastBusiness
//
//  Created by Admin on 5/5/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "AppDelegate.h"
#import "Step1ViewController.h"
#import "step2ViewController.h"
#import "step3ViewController.h"
#import "step4ViewController.h"
#import "Step5ViewController.h"
#import "WebserviceAPI.h"
#import "NSString+URLEncoding.h"
#import "XmlParser.h"
#import "VCPDFVideoViewer.h"
@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize revealViewController,isHamburgerVisible,objVCLeftMenu,activeView,ShowId,GlobalFilePathVal,leaddataoffline,selectassetdataarray,UpdateId,objRootView,leadarray,formvalarray,formidarray,forseldsel,emailsArray,scanfname,scanlname,scanemail,scanphone,scanbname,scanjtitle,scanaddr,scanzcode,notesval,tmptable,step1viewcont,surveyjson,firsttime,preview,GlobalFileTypeVal,curentleadid,edited,completeresult,urlstring,showchange,ShowName,tmpshwid,tmpshwnme,viewviewcont,seprator,colorforall,txtboxid,radioboxid,checkboxid,backgroundimgpath,glbsurid,urldwnld,GlobalFileNameVal,previousemail;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    ///****************************// Set here API link and values that would be used during the life cycle of app. //******************
    
    NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"assets" ofType:nil];
    
    
    myAppdelegate.colorforall = [self colorWithHexString:@"#008ccd"];
    
    
    //***********/ Setup timer here that will call in every second to check update at server side and accordingly update the app. /**********
    NSTimer *timer;
    timer = [NSTimer scheduledTimerWithTimeInterval:60.0
                                             target:self
                                           selector:@selector(performOnBack)
                                           userInfo:nil
                                            repeats:YES];
    //***************************************//
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *Update_sync_startup = [prefs valueForKey:@"startupsync"];
    if([Update_sync_startup length] <= 0)
    {
        [prefs setObject:@"1" forKey:@"startupsync"];
    }
    
    [prefs synchronize];
    
    
    //****************************************************************************//
    
    return YES;
}

#pragma mark - Below methods would be called in every second to get update

- (void)performOnBack {
    
    
    
    
    if(!myAppdelegate.showchange)
        [self performSelectorInBackground:@selector(updateLabel) withObject:nil];
}

- (void)updateLabel
{
    
    [self Exportdata];
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *Update_sync_time = [prefs valueForKey:@"Update_sync_timeLeads"];
    if([Update_sync_time length] <= 0)
    {
        [prefs setObject:@"0000-00-00 00:00:00" forKey:@"Update_sync_timeLeads"];
        Update_sync_time = @"0000-00-00 00:00:00";
    }
    NSData *LoginData = [WebserviceAPI requestFetchAllLeadDataandUpdateInBackground:Update_sync_time Version:@"1" Showid:myAppdelegate.ShowId];
    
    if([self.leadarray count] > 0)
    {
        [self.leadarray removeAllObjects];
    }
    NSString *temp2=[NSString stringWithFormat:@"SELECT * FROM leads_offline"];
    sqlite3_stmt *dataRows1=[clsDatabase getDataset:[temp2 UTF8String]];
    
    LeadCustomClass *leaddataofflineforsharing;
    
    while(sqlite3_step(dataRows1) == SQLITE_ROW)
    {
        if (leaddataofflineforsharing==nil) {
            
        }else {
            leaddataofflineforsharing=nil;
        }
        leaddataofflineforsharing =[[LeadCustomClass alloc]init];
        
        leaddataofflineforsharing.ids =  [NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows1,0)];
        
        leaddataofflineforsharing.Surid   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,1)]];
        
        leaddataofflineforsharing.shwid =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,2)]];
        
        leaddataofflineforsharing.Fname   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,3)]];
        
        leaddataofflineforsharing.Lname =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,4)]];
        
        leaddataofflineforsharing.Email   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,5)]];
        
        leaddataofflineforsharing.Phone =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,6)]];
        
        leaddataofflineforsharing.Jtitle   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,7)]];
        
        leaddataofflineforsharing.Notes =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,8)]];
        
        leaddataofflineforsharing.Rsrcid   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,9)]];
        
        leaddataofflineforsharing.emailid =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,10)]];
        
        leaddataofflineforsharing.Bname   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,11)]];
        
        leaddataofflineforsharing.Address =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,12)]];
        
        leaddataofflineforsharing.Zcode   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,13)]];
        
        leaddataofflineforsharing.surdata =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,14)]];
        leaddataofflineforsharing.leadId = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,15)]];
        
        leaddataofflineforsharing.badgeresult =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,16)]];
        leaddataofflineforsharing.leadtype  =  ((char *)sqlite3_column_text(dataRows1, 17)) ? [NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1, 17)] : @"1";
        [self.leadarray addObject:leaddataofflineforsharing];
    }
    
}

#pragma mark - Appdelegate methods

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#pragma mark - It gives color from hexvalue to change the color of app

-(UIColor*)colorWithHexString:(NSString*)hex
{
    
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    cString = [cString substringFromIndex:1];
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}


#pragma mark - Method to control viewcontroller movement

-(void)ShowViewController:(UIButton *)btn{
    
    if (btn.tag==1) {
        
        [objRootView.btn1 setSelected:YES];
        if ([myAppdelegate.selectassetdataarray count] > 0) {
            [myAppdelegate.objRootView.btnBadge setHidden:NO];
        }else{
            [myAppdelegate.objRootView.btnBadge setHidden:YES];
        }
        
        [myAppdelegate.objRootView.btnBadge setTitle:[NSString stringWithFormat:@"%d", [myAppdelegate.selectassetdataarray count]] forState:UIControlStateNormal];
        
        step1viewcont = [mainStoryBoard instantiateViewControllerWithIdentifier: @"step1ViewController"];
        [myAppdelegate.navcontroller pushViewController:step1viewcont animated:YES];
        
        
        
        [myAppdelegate.objRootView ChangeColor:myAppdelegate.objRootView.btn5];
        
    }else if (btn.tag==2){
        [objRootView.btn2 setSelected:YES];
        UIViewController* step1 = [mainStoryBoard instantiateViewControllerWithIdentifier: @"step2ViewController"];
        [myAppdelegate.navcontroller pushViewController:step1 animated:NO];
        
        
        
        
        
    }else if (btn.tag==3){
        
        [objRootView.btn3 setImage:[UIImage imageNamed:@"clipboard.png"] forState:UIControlStateNormal];
        [objRootView.btn3 setImage:[UIImage imageNamed:@"clipboardSelected.png"] forState:UIControlStateSelected];
        [objRootView.btn3 setImage:[UIImage imageNamed:@"clipboardSelected.png"] forState:UIControlStateHighlighted];
        [myAppdelegate.objRootView ChangeColor:myAppdelegate.objRootView.btn3];
        [objRootView.btn3 setSelected:YES];
        UIViewController* step1 = [mainStoryBoard instantiateViewControllerWithIdentifier: @"step3ViewController"];
        [myAppdelegate.navcontroller pushViewController:step1 animated:NO];
        
        
        
        
    }else if (btn.tag==4){
        UIViewController* step1 = [mainStoryBoard instantiateViewControllerWithIdentifier: @"step4ViewController"];
        [myAppdelegate.navcontroller pushViewController:step1 animated:NO];
        
        [objRootView.btn4 setSelected:YES];
        
    }else if (btn.tag==5){
        
        [objRootView ShowAlertView:@"Your information was successfully uploaded to the Cloud." alertType:3];
        
        
        
    }else if (btn.tag==15){
        UIViewController* step1 = [mainStoryBoard instantiateViewControllerWithIdentifier: @"step5ViewController"];
        [myAppdelegate.navcontroller pushViewController:step1 animated:NO];
        
        
        
        
    }
    
    
    
}

#pragma mark - It will check the validty of enterd email

- (BOOL)validateEmailWithString:(NSString*)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

#pragma mark - It will create local database for application

-(void) createDB:(NSString *)dbnamepath
{
    NSString *docsDir;
    NSArray *dirPaths;
    static sqlite3 *database = nil;
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    NSString *databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"ComCast.sqlite"]];
    
    
    
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
    {
        
        
        
        char *errMsg;
        const char *sql_stmt = [dbnamepath UTF8String];
        
        if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
            
        }
        sqlite3_close(database);
        
    }
}
#pragma mark - Below methods will save all data to local database
-(void) CallSaveData
{
    [self SaveAllData:leaddataoffline.Notes];
}
-(void)SaveAllData:(NSString *)notes
{
    
    showchange =true;
    
    NSString *MyQuery=[NSString stringWithFormat:@"insert into leads_offline values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"];
    
    
    NSString *emails = @"";
    for(int i = 0; i<[emailsArray count]; i++)
    {
        if([[emailsArray objectAtIndex:i] length] > 0)
        {
            
            if(emails.length <= 0)
            {
                emails = [emailsArray objectAtIndex:i];
            }
            else
            {
                emails = [NSString stringWithFormat:@"%@,%@",emails,[emailsArray objectAtIndex:i]];
                
            }
            
        }
    }
    
    
    
    NSString *assetids = @"";
    for(int i= 0; i< [myAppdelegate.selectassetdataarray count]; i++)
    {
        if(i == 0)
            assetids = [myAppdelegate.selectassetdataarray objectAtIndex:i];
        else
            assetids = [NSString stringWithFormat:@"%@,%@",assetids,[myAppdelegate.selectassetdataarray objectAtIndex:i]];
    }
    NSMutableDictionary *nameElements;
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    NSMutableArray *listOfVals;
    listOfVals = [[NSMutableArray alloc] init];
    if(![myAppdelegate.UpdateId isEqualToString:@"0"])
    {
        MyQuery=[NSString stringWithFormat:@"UPDATE leads_offline SET Survey_Id = '%@',  Show_Id = '%@', First_Name = '%@', Last_Name = '%@', Email = '%@', Phone = '%@', Job_Function = '%@', Notes = '%@', resourceid = '%@', emailid = '%@', Business_Name = '%@', Address = '%@', Zipcode = '%@', Surveydata = '%@',Lead_Id = '%@', Badge_Data_Offline = '%@', leadtype = %@ WHERE id = '%d'",myAppdelegate.leaddataoffline.Surid,myAppdelegate.leaddataoffline.shwid,myAppdelegate.leaddataoffline.Fname,myAppdelegate.leaddataoffline.Lname,myAppdelegate.leaddataoffline.Email,myAppdelegate.leaddataoffline.Phone,myAppdelegate.leaddataoffline.Jtitle,notes,assetids,emails,myAppdelegate.leaddataoffline.Bname,myAppdelegate.leaddataoffline.Address,myAppdelegate.leaddataoffline.Zcode,myAppdelegate.leaddataoffline.surdata,leaddataoffline.leadId,myAppdelegate.completeresult, myAppdelegate.leaddataoffline.leadtype, [myAppdelegate.UpdateId intValue]];
        
        
        
        [self createDB:MyQuery];
        NSString *myDelquery = [NSString stringWithFormat:@"delete  from  survey_submitted_data_offline where Lead_Id = '%@'",myAppdelegate.UpdateId];
        [self createDB:myDelquery];
        int k = 0;
        for(int i = 0; i< formvalarray.count; i++)
        {
            
            
            NSString *Field_Name = @"";
            NSString *Field_Value = @"";
            
            NSString *myString = [myAppdelegate.formvalarray objectAtIndex:i];
            int loc =  [myString rangeOfString:seprator].location;
            
            if(loc > 0)
            {
                Field_Name = [myString substringWithRange: NSMakeRange(0, [myString rangeOfString: seprator].location)];
                
                Field_Value = [myString substringFromIndex:[myString rangeOfString:seprator].location + seprator.length];
            }
            
            
            NSString *MyQuery=[NSString stringWithFormat:@"insert into survey_submitted_data_offline (Lead_Id,Field_Name,Field_Value) values('%@','%@','%@')",myAppdelegate.UpdateId,Field_Name,Field_Value];
            
            [self createDB:MyQuery];
        }
        
    }
    else
    {
        [clsDatabase AddBasketVal:[MyQuery UTF8String] Survey_Id:myAppdelegate.glbsurid Show_Id:myAppdelegate.ShowId First_Name:myAppdelegate.leaddataoffline.Fname Last_Name:myAppdelegate.leaddataoffline.Lname Email:myAppdelegate.leaddataoffline.Email Phone:myAppdelegate.leaddataoffline.Phone Job_Function:myAppdelegate.leaddataoffline.Jtitle Notes:notes Created:@"NA" resourceid:assetids emailid:emails Business_Name:myAppdelegate.leaddataoffline.Bname Address:myAppdelegate.leaddataoffline.Address Zipcode:myAppdelegate.leaddataoffline.Zcode
                       Surveydata:leaddataoffline.surdata leadId:leaddataoffline.leadId
                       Fullresult:myAppdelegate.completeresult leadtype:myAppdelegate.leaddataoffline.leadtype];
        
        NSString *temp2=[NSString stringWithFormat:@"select MAX(Id) from leads_offline"];
        sqlite3_stmt *dataRows1=[clsDatabase getDataset:[temp2 UTF8String]];
        NSString * idval = @"1";
        while(sqlite3_step(dataRows1) == SQLITE_ROW)
        {
            
            idval = [NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows1,0)];
        }
        
        
        
        
        NSString *myDelquery = [NSString stringWithFormat:@"delete  from  survey_submitted_data_offline where Lead_Id = '%@'",idval];
        [self createDB:myDelquery];
        
        
        int k = 0;
        
        for(int i = 0; i< formvalarray.count; i++)
        {
            
            
            NSString *MyQuery=[NSString stringWithFormat:@"insert into survey_submitted_data_offline values(?,?,?,?)"];
            
            NSString *Field_Name = @"";
            NSString *Field_Value = @"";
            
            
            NSString *myString = [myAppdelegate.formvalarray objectAtIndex:i];
            int loc =  [myString rangeOfString:seprator].location;
            
            if(loc > 0)
            {
                Field_Name = [myString substringWithRange: NSMakeRange(0, [myString rangeOfString: seprator].location)];
                
                Field_Value = [myString substringFromIndex:[myString rangeOfString:seprator].location + seprator.length];
            }
            
            [clsDatabase AddSurveyVal:[MyQuery UTF8String] Lead_Id:idval Field_Name:Field_Name Field_Value:Field_Value];
            
        }
    }
    
    
}
#pragma mark - Below method will export all data from local database to display
-(void) Exportdata
{
    
    NSError *error;
    NSString *stringToWrite = @"";
    
    if([self.leadarray count] > 0)
    {
        [self.leadarray removeAllObjects];
    }
    NSString *temp2=[NSString stringWithFormat:@"SELECT * FROM leads_offline"];
    sqlite3_stmt *dataRows1=[clsDatabase getDataset:[temp2 UTF8String]];
    
    LeadCustomClass *leaddataofflineforsaving;
    
    while(sqlite3_step(dataRows1) == SQLITE_ROW)
    {
        if (leaddataofflineforsaving==nil) {
            
        }else {
            leaddataofflineforsaving=nil;
        }
        leaddataofflineforsaving =[[LeadCustomClass alloc]init];
        
        leaddataofflineforsaving.ids =  [NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows1,0)];
        
        leaddataofflineforsaving.Surid   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,1)]];
        
        leaddataofflineforsaving.shwid =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,2)]];
        
        leaddataofflineforsaving.Fname   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,3)]];
        
        leaddataofflineforsaving.Lname =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,4)]];
        
        leaddataofflineforsaving.Email   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,5)]];
        
        leaddataofflineforsaving.Phone =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,6)]];
        
        leaddataofflineforsaving.Jtitle   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,7)]];
        
        leaddataofflineforsaving.Notes =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,8)]];
        
        leaddataofflineforsaving.Rsrcid   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,9)]];
        
        leaddataofflineforsaving.emailid =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,10)]];
        
        leaddataofflineforsaving.Bname   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,11)]];
        
        leaddataofflineforsaving.Address =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,12)]];
        
        leaddataofflineforsaving.Zcode   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,13)]];
        
        leaddataofflineforsaving.surdata =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,14)]];
        leaddataofflineforsaving.leadId = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,15)]];
        
        leaddataofflineforsaving.badgeresult = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,16)]];
        leaddataofflineforsaving.leadtype  =  ((char *)sqlite3_column_text(dataRows1, 17)) ? [NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1, 17)] : @"1";
        
        
        [self.leadarray addObject:leaddataofflineforsaving];
    }
    int sucs = 0;
    stringToWrite = @"\n\n\n";
    if(leadarray.count > 0)
    {
        for(int i = 0; i< leadarray.count; i++)
        {
            
            if (leaddataofflineforsaving==nil) {
                
            }else {
                leaddataofflineforsaving=nil;
            }
            leaddataofflineforsaving =[[LeadCustomClass alloc]init];
            
            leaddataofflineforsaving = [leadarray objectAtIndex:i];
            
            [self FetchJson:leaddataofflineforsaving.ids];
            
            NSString *detailstr = @"";
            detailstr = [NSString stringWithFormat:@"FIRST NAME:  %@\n LAST NAME: %@\n EMAIL: %@\n PHONE: %@\n BUSINESS NAME: %@\n JOB TITLE: %@\n ADDRESS: %@\n ZIP CODE: %@\n SURVEY FIELDS: %@\n NOTES: %@\n EMAIL_ID: %@\n BADGE RESULT : %@\n ASSETS_ID: %@\n SHOW_ID: %@\n",leaddataofflineforsaving.Fname,leaddataofflineforsaving.Lname,leaddataofflineforsaving.Email,leaddataofflineforsaving.Phone,leaddataofflineforsaving.Bname,leaddataofflineforsaving.Jtitle,leaddataofflineforsaving.Address,leaddataofflineforsaving.Zcode,surveyjson,leaddataofflineforsaving.Notes,leaddataofflineforsaving.emailid,leaddataofflineforsaving.badgeresult,leaddataofflineforsaving.Rsrcid,leaddataofflineforsaving.shwid];
            stringToWrite = [NSString stringWithFormat:@"%@%@\n\n\n",stringToWrite,detailstr];
            
        }
    }
    
    
    
}

-(void) callHide
{
    curentleadid = @"0";
    [myAppdelegate.objVCLeftMenu FetchAllLeads];
    UIButton *butval= [UIButton buttonWithType:UIButtonTypeRoundedRect];
    butval.tag = 1;
    if ([myAppdelegate.selectassetdataarray count] > 0) {
        [myAppdelegate.objRootView.btnBadge setHidden:NO];
    }else{
        [myAppdelegate.objRootView.btnBadge setHidden:YES];
    }
    [myAppdelegate.objRootView.btnBadge setTitle:[NSString stringWithFormat:@"%d", [myAppdelegate.selectassetdataarray count]] forState:UIControlStateNormal];
    
    
    myAppdelegate.activeView=-1;
    
    
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *Badge_Scan = [prefs valueForKey:@"Badge_Scan"];
    
    if([Badge_Scan isEqualToString:@"0"])
        [myAppdelegate ShowViewController:butval];
    else
        [myAppdelegate.navcontroller popToRootViewControllerAnimated:YES];
    
    
    myAppdelegate.objRootView.progView.hidden = true;
    [objRootView HideAlertView];
    
}

#pragma mark - Below method will fetch or create the json string to send the srver

-(void) FetchJson:(NSString *)idval
{
    
    NSMutableDictionary *nameElements;
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    NSMutableArray *listOfVals;
    NSMutableArray *listOfValsother;
    NSMutableArray *listofother;
    listOfVals = [[NSMutableArray alloc] init];
    listOfValsother = [[NSMutableArray alloc] init];
    listofother = [[NSMutableArray alloc] init];
    surveyjson = @"";
    int k = 0;
    int kl = 0;
    
    NSString *temp22=[NSString stringWithFormat:@"select Label from field_options where Other_Flag = 1"];
    sqlite3_stmt *dataRows11=[clsDatabase getDataset:[temp22 UTF8String]];
    
    while(sqlite3_step(dataRows11) == SQLITE_ROW)
    {
        NSString *value = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows11,0)]];
        
        [listofother addObject:value];
    }
    
    NSString *temp2=[NSString stringWithFormat:@"select Field_Name, Field_Value from survey_submitted_data_offline where Lead_Id='%@' ",idval];
    sqlite3_stmt *dataRows1=[clsDatabase getDataset:[temp2 UTF8String]];
    
    while(sqlite3_step(dataRows1) == SQLITE_ROW)
    {
        
        NSString *Field_Name = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,0)]];
        
        NSString *Field_Value = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,1)]];
        
        if(!([listofother containsObject:Field_Value]))
        {
            if(Field_Value.length < 2)
            {
                nameElements = [[NSMutableDictionary alloc] init ];
                [nameElements setObject:[Field_Value URLEncodedString] forKey:Field_Name];
                [listOfVals insertObject:nameElements atIndex:k];
                k++;
            }
            else
            {
                if(([[Field_Value substringToIndex:2] isEqualToString:@"ot"]))
                {
                    nameElements = [[NSMutableDictionary alloc] init ];
                    [nameElements setObject:[[Field_Value substringFromIndex:2]URLEncodedString] forKey:Field_Name];
                    [listOfValsother insertObject:nameElements atIndex:kl];
                    kl++;
                }
                else
                {
                    nameElements = [[NSMutableDictionary alloc] init ];
                    [nameElements setObject:[Field_Value URLEncodedString] forKey:Field_Name];
                    [listOfVals insertObject:nameElements atIndex:k];
                    k++;
                }
            }
        }
        
    }
    
    [dict setObject:listOfVals forKey:@"Survey_Detail"];
    [dict setObject:listOfValsother forKey:@"Other_Detail"];
    
    NSError *error;
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:nil error:&error];
    
    if (! jsonData) {
        
    } else {
        surveyjson = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    
    
    
    
    
    if(surveyjson == NULL)
        surveyjson = @"";
    
}
#pragma mark - Below method will hide alert info from the screen

-(void)HideAlert
{
    curentleadid = @"0";
    [myAppdelegate.objVCLeftMenu FetchAllLeads];
    UIButton *butval= [UIButton buttonWithType:UIButtonTypeRoundedRect];
    butval.tag = 1;
    [objRootView ShowAlertView:@"Your information was successfully uploaded to the Cloud." alertType:3];
    if ([myAppdelegate.selectassetdataarray count] > 0) {
        [myAppdelegate.objRootView.btnBadge setHidden:NO];
    }else{
        [myAppdelegate.objRootView.btnBadge setHidden:YES];
    }
    [myAppdelegate.objRootView.btnBadge setTitle:[NSString stringWithFormat:@"%d", [myAppdelegate.selectassetdataarray count]] forState:UIControlStateNormal];
    
    
    myAppdelegate.activeView=-1;
    
    
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *Badge_Scan = [prefs valueForKey:@"Badge_Scan"];
    
    if([Badge_Scan isEqualToString:@"0"])
        [myAppdelegate ShowViewController:butval];
    else
        [myAppdelegate.navcontroller popToRootViewControllerAnimated:YES];
    
    
    
}

#pragma mark - Below method will show left or hamburger menu as user tap on button or gesture

-(void)ShowLeftMenu{
    
    if(!showchange)
    {
        [self.window endEditing:YES];
        [myAppdelegate.objRootView ChangeColor:myAppdelegate.objVCLeftMenu.btnSettings];
        [myAppdelegate.objVCLeftMenu.view setHidden:NO];
        
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *Update_sync_startup = [prefs valueForKey:@"startupsync"];
        if([Update_sync_startup isEqualToString:@"1"])
        {
            [myAppdelegate.objVCLeftMenu.autosyncbut setSelected:YES];
            [myAppdelegate.objVCLeftMenu.btnsel setHidden:NO];
        }
        else
        {
            [myAppdelegate.objVCLeftMenu.autosyncbut setSelected:NO];
            [myAppdelegate.objVCLeftMenu.btnsel setHidden:YES];
        }
        
        [myAppdelegate.objVCLeftMenu.btnSelectDiffShow setBackgroundColor:myAppdelegate.colorforall];
        [myAppdelegate.objRootView ChangeColor:myAppdelegate.objVCLeftMenu.btnsel];
        
        [UIView animateWithDuration:0.5f
                         animations:^{
                             myAppdelegate.objVCLeftMenu.view.frame = CGRectMake(0, 0, 768, self.window.frame.size.height);
                         }
                         completion:^(BOOL finished){
                             
                             
                             
                         }];
    }
}
#pragma mark - Below method will hide left or hamburger menu as user tap on button or gesture
-(void)HideLeftMenu{
    
    
    [UIView animateWithDuration:0.5f
                     animations:^{
                         myAppdelegate.objVCLeftMenu.view.frame = CGRectMake(-768, 0, 768, self.window.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         
                         if ([[myAppdelegate.objVCLeftMenu.btnSettings titleForState:UIControlStateNormal] isEqualToString:@"Close Settings"]) {
                             CGRect imageFrame = myAppdelegate.objVCLeftMenu.viewSettings.frame;
                             imageFrame.origin.y=myAppdelegate.objVCLeftMenu.view.frame.size.height-myAppdelegate.objVCLeftMenu.viewSettings.frame.size.height;
                             myAppdelegate.objVCLeftMenu.viewSettings.frame=imageFrame;
                             imageFrame = myAppdelegate.objVCLeftMenu.viewSettings.frame;
                             imageFrame.origin.y = myAppdelegate.objVCLeftMenu.view.frame.size.height+myAppdelegate.objVCLeftMenu.viewSettings.frame.size.height;;
                             myAppdelegate.objVCLeftMenu.viewSettings.frame = imageFrame;
                             [myAppdelegate.objVCLeftMenu.btnSettings setTitle:@"Settings" forState:UIControlStateNormal];
                             
                             [myAppdelegate.objVCLeftMenu.btnSettings setSelected:NO];
                             
                             
                             [myAppdelegate.objVCLeftMenu.viewSettings setHidden:YES];
                         }
                         [myAppdelegate.objVCLeftMenu.view setHidden:YES];
                         
                     }];
}
#pragma mark - Below method will control the oriention in app
- (UIInterfaceOrientationMask)application:(UIApplication* )application supportedInterfaceOrientationsForWindow:(UIWindow* )window
{
    
    
    if ([self.navcontroller.presentedViewController isKindOfClass: [VCPDFVideoViewer class]])
    {
        if (self.navcontroller.presentedViewController)
            return UIInterfaceOrientationMaskAll;
        else return UIInterfaceOrientationMaskPortrait;
    }
    else return UIInterfaceOrientationMaskPortrait;
}


@end
