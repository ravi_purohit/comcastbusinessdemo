//
//  AppDelegate.h
//  ComcastBusiness
//
//  Created by Admin on 5/5/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VCLeftMenu.h"
#import "LeadCustomClass.h"
#import "VCRootView.h"
#import "AssetsCustomClass.h"
#import "Step1ViewController.h"
#import "ViewController.h"
#import <QuickLook/QuickLook.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    NSString *GlobalFilePathVal;
    NSString *GlobalFileTypeVal;
    NSString *GlobalFileNameVal;
    LeadCustomClass *leaddataoffline;
    NSMutableArray *selectassetdataarray;
    NSMutableArray *addedemailarray;
    NSMutableArray *formvalarray;
    NSMutableArray *formidarray;
    
    NSMutableArray *leadarray;
    NSMutableArray *emailsArray;
    NSString *UpdateId;
    NSString *ShowId;
    NSString *ShowName;
    AssetsCustomClass *forseldsel;
    Step1ViewController* step1viewcont;
    NSString *scanfname;
    NSString *scanlname;
    NSString *scanemail;
    NSString *scanphone;
    NSString *scanbname;
    NSString *scanjtitle;
    NSString *scanaddr;
    NSString *scanzcode;
    NSString *notesval;
    NSString *surveyjson;
    BOOL *firsttime;
    BOOL *edited;
    BOOL *showchange;
    NSString *editledid;
    QLPreviewController* preview;
    NSString *curentleadid;
    NSString *completeresult;
    NSString *urlstring;
    NSString *tmpshwid;
    NSString *tmpshwnme;
    NSString *seprator;
    
    NSString *txtboxid;
    NSString *radioboxid;
    NSString *checkboxid;
    NSString *backgroundimgpath;
    NSString *glbsurid;
    NSString *urldwnld;
    NSString *previousemail;
}
@property(strong,nonatomic)UINavigationController *navcontroller;

@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic) IBOutlet UITableView *tmptable;
@property(nonnull,strong) SWRevealViewController *revealViewController;
@property (nonatomic, retain) Step1ViewController* step1viewcont;
@property (nonatomic, retain) ViewController* viewviewcont;

@property (nonatomic, retain) NSMutableArray *selectassetdataarray;
@property (nonatomic, retain) NSString *glbsurid;
@property (nonatomic, retain) NSString *editledid;
@property (nonatomic, retain) NSString *urlstring;
@property (nonatomic, retain) NSString *curentleadid;
@property (nonatomic, retain) NSString *completeresult;
@property (nonatomic, retain) NSString *ShowName;
@property (nonatomic, retain) NSString *previousemail;
@property (nonatomic, retain) QLPreviewController* preview;
@property (nonatomic, assign) BOOL *firsttime;
@property (nonatomic, assign) BOOL *edited;
@property (nonatomic, assign) BOOL *showchange;
@property (nonatomic, retain) NSMutableArray *addedemailarray;
@property (nonatomic, retain) NSMutableArray *leadarray;
@property (nonatomic, retain) NSMutableArray *formvalarray;
@property (nonatomic, retain) NSMutableArray *formidarray;
@property (nonatomic, retain) NSMutableArray *emailsArray;
@property (nonatomic, retain) NSString *urldwnld;
@property (nonatomic, retain)  NSString *surveyjson;
@property (nonatomic, retain) NSString *scanfname;
@property (nonatomic, retain) NSString *scanlname;
@property (nonatomic, retain) NSString *scanemail;
@property (nonatomic, retain) NSString *scanphone;
@property (nonatomic, retain) NSString *scanbname;
@property (nonatomic, retain) NSString *scanjtitle;
@property (nonatomic, retain) NSString *scanaddr;
@property (nonatomic, retain) NSString *scanzcode;
@property (nonatomic, retain) NSString *UpdateId;
@property (nonatomic, retain) NSString *notesval;
@property (nonatomic, retain) NSString *tmpshwid;
@property (nonatomic, retain) NSString *tmpshwnme;
@property (nonatomic, retain) NSString *seprator;

@property (nonatomic, retain) NSString *txtboxid;
@property (nonatomic, retain) NSString *radioboxid;
@property (nonatomic, retain) NSString *checkboxid;

@property (nonatomic, retain)NSString *ShowId;
@property (strong,nonatomic) UIColor *titleColor;
@property (strong,nonatomic) UIColor *textfieldColor;
@property (strong,nonatomic) UIColor *footerColor;
@property (strong,nonatomic) UIColor *categorySelectedColor;
@property (strong,nonatomic) UIColor *categoryDeSelectedColor;
@property (strong,nonatomic) UIColor *categorySelectedBGColor;
@property (strong,nonatomic) UIColor *categoryDeSelectedBGColor;
@property (strong,nonatomic) UIColor *btnSelectedBGColor;
@property (strong,nonatomic) UIColor *btnSelectedTextColor;
@property (strong,nonatomic) UIColor *btnDeSelectedBGColor;
@property (strong,nonatomic) UIColor *btnDeSelectedTextColor;
@property (strong,nonatomic) UIColor *btnScanBadgeColor;
@property (strong,nonatomic) UIColor *hamburgerBGColor;
@property (strong,nonatomic) UIColor *colorforall;
@property (strong,nonatomic) NSString *backgroundimgpath;

@property (strong,nonatomic) UIColor *screen2HeaderBG; //UPDATED ON 02-JUN-2017
@property (strong,nonatomic) UIColor *screen2TableBG;//UPDATED ON 02-JUN-2017



@property(nonatomic,assign)BOOL isHamburgerVisible;
@property(nonatomic,strong) VCLeftMenu *objVCLeftMenu;
@property(nonatomic,strong) NSString *GlobalFilePathVal;
@property(nonatomic,strong) NSString *GlobalFileTypeVal;
@property(nonatomic,strong) NSString *GlobalFileNameVal;
@property(nonatomic,strong) LeadCustomClass *leaddataoffline;
@property(nonatomic,strong) AssetsCustomClass *forseldsel;
@property(nonatomic,assign)int activeView;

@property(nonatomic,strong)VCRootView *objRootView;
-(void)ShowViewController:(UIButton *)btn;
-(void) FetchJson:(NSString *)idval;


-(void)ShowLeftMenu;
-(void)HideLeftMenu;
-(void)SaveAllData:(NSString *)notes;
-(UIColor*)colorWithHexString:(NSString*)hex;
@end

