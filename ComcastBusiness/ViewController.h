//
//  ViewController.h
//  ComcastBusiness
//
//  Created by Admin on 5/5/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZBarSDK.h"

@interface ViewController : UIViewController<ZBarReaderDelegate, CAAnimationDelegate>
{
    NSMutableArray *PictureArray;
    NSString *globalfilepath;
    float totalwidth;
    float totalfile;
    float onewidth;
    float cureentfileno;
    float width;
    BOOL *donescan;
    long long dataSize;
}
- (IBAction)scanBadgeClick:(id)sender;
- (IBAction)manualScanClick:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *splashproglabel;
@property (strong, nonatomic) IBOutlet UILabel *splashtxtlabel;
@property (strong, nonatomic) IBOutlet UILabel *splashprogbcklabel;
@property (strong, nonatomic) IBOutlet UIImageView *footer, *bckimgview,*opaqimgview, *imgfooter;
@property (strong, nonatomic) IBOutlet UIView *SplashView;
@property (nonatomic, assign) BOOL *donescan;
@property (nonatomic, assign) long long dataSize;
@property(nonnull,strong)IBOutlet UIButton *btnHamburger;
@property(nonatomic,strong)IBOutlet UIButton *btnScanBadge, *btnManualEntry;
@property (strong, nonatomic) IBOutlet UIImageView   *splshimgview;

-(NSDictionary *)getDataFromScanevt_uid:(NSString *)evt_uid badge_id:(NSString *)badge_id check:(NSString *)check token:(NSString *)token ;

-(IBAction)ClickOnTopNavigationButtons:(id)sender;

@property(nonatomic,retain)	NSString *globalfilepath;
-(void) createDB:(NSString *)dbnamepath;
-(void) Changeshow: (NSString *)showid;
@property (strong, nonatomic) IBOutlet UIView *footerView;

@end

