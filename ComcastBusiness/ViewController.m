//
//  ViewController.m
//  ComcastBusiness
//
//  Created by Admin on 5/5/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "ViewController.h"
#import "Step1ViewController.h"
#import "ZBarSDK.h"
#import "ZBarReaderViewController.h"
#import "ZBarImageScanner.h"
#import "WebserviceAPI.h"
#import "XmlParser.h"
#import "clsDatabase.h"
#import "Reachability.h"
#import "NSString+URLEncoding.h"

static NSDictionary*jsonDict;

@interface ViewController ()
{
    NSString *eventId;
    NSString *tokenNumber;
    NSMutableArray *newArray;
    
}
@property(nonatomic,strong)IBOutlet UIButton *btn1, *btn2, *btn3, *btn4, *btn5;
@end

@implementation ViewController
@synthesize btnHamburger,btnScanBadge,btnManualEntry,donescan,dataSize,globalfilepath,splshimgview,splashtxtlabel,splashprogbcklabel;


@synthesize btn1, btn2, btn3, btn4, btn5;

#pragma mark - Below methods will called as view load so here we setup the intial values for app

- (void)viewDidLoad {
    
    
    
    
    
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *fetchQAuery=[NSString stringWithFormat:@"SELECT File_Path FROM file_manager WHERE  File_Id IN (Select File_Id from theme_data)"];
    myAppdelegate.backgroundimgpath = @"";
    sqlite3_stmt *dataRows1=[clsDatabase getDataset:[fetchQAuery UTF8String]];
    
    while(sqlite3_step(dataRows1) == SQLITE_ROW)
    {
        myAppdelegate.backgroundimgpath = [NSString stringWithFormat:@"%@/%@",docDir,[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,0)]];
    }
    
    NSFileManager *FileManager1 = [NSFileManager defaultManager];
    BOOL success = [FileManager1 fileExistsAtPath:myAppdelegate.backgroundimgpath];
    if(!success)
    {
        myAppdelegate.backgroundimgpath = @"";
    }
    
    
    
    
    
    
    
    self.footerView.backgroundColor=myAppdelegate.footerColor; //SET FOOTER COLOR ON DATED 02-JUN-2017

    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    
    
    
    [clsDatabase checkDataBase];
    CGRect frm_but = _splashproglabel.frame;
    frm_but.size.width = 0;
    _splashproglabel.frame = frm_but;
    
    NSUserDefaults *prefsp = [NSUserDefaults standardUserDefaults];
    NSString *Update_sync_startup = [prefsp valueForKey:@"startupsync"];
    
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *Update_sync_time = [prefs valueForKey:@"Update_sync_time"];
    if([Update_sync_time length] <= 0)
    {
        [prefs setObject:@"0000-00-00 00:00:00" forKey:@"Update_sync_time"];
        
        [prefs setObject:@"61" forKey:@"Show_select"];
        Update_sync_time = @"0000-00-00 00:00:00";
    }
    else
    {
        myAppdelegate.ShowId = [prefs valueForKey:@"Show_select"];
        myAppdelegate.ShowName = [prefs valueForKey:@"Show_name"];
        
    }
    fetchQAuery=[NSString stringWithFormat:@"SELECT Color_code from theme_data"];
    dataRows1=[clsDatabase getDataset:[fetchQAuery UTF8String]];
    NSString *colorcode = @"";
    while(sqlite3_step(dataRows1) == SQLITE_ROW)
    {
        colorcode = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,0)]];
    }
    
    if(colorcode.length > 0)
    {
        myAppdelegate.colorforall = [myAppdelegate colorWithHexString:colorcode];
    }
    
    self.imgfooter.backgroundColor=myAppdelegate.colorforall;
    
    
    myAppdelegate.showchange = true;
    if([Update_sync_startup isEqualToString:@"1"])
    {
        
        if([[Reachability sharedReachability] internetConnectionStatus] != NotReachable)
        {
            if(myAppdelegate.backgroundimgpath.length > 0)
            {
                [self.splshimgview setImage:[UIImage imageWithContentsOfFile:myAppdelegate.backgroundimgpath]];
            }
            _SplashView.hidden = false;
            [self performSelector:@selector(CallWebService) withObject:nil afterDelay:0.5];
        }
        
    }
    else
    {
        if([[Reachability sharedReachability] internetConnectionStatus] != NotReachable)
        {
            if(myAppdelegate.backgroundimgpath.length > 0)
            {
                [self.splshimgview setImage:[UIImage imageWithContentsOfFile:myAppdelegate.backgroundimgpath]];
            }
            _SplashView.hidden = false;
            [self performSelector:@selector(CallWebServicecheckupdate) withObject:nil afterDelay:0.5];
        }
        
    }
    
    splashprogbcklabel.textColor     = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.5];
    splashprogbcklabel.shadowColor   = [UIColor redColor];
    splashprogbcklabel.shadowOffset  = CGSizeMake(-5.0,25.0);
    splashprogbcklabel.layer.shadowColor = (__bridge CGColorRef _Nullable)([UIColor redColor]);
    splashprogbcklabel.layer.shadowRadius = 3.0;
    splashprogbcklabel.layer.shadowOpacity = 1.0;
    splashprogbcklabel.layer.shadowOffset = CGSizeMake(14, 14);
    splashprogbcklabel.layer.masksToBounds = false;
    
    splashprogbcklabel.layer.cornerRadius = 5.0;
    splashprogbcklabel.layer.masksToBounds = YES;
    _splashproglabel.layer.cornerRadius = 5.0;
    _splashproglabel.layer.masksToBounds = YES;
    
    
    
    
    // Do any additional setup after loading the view.
}



- (BOOL)shouldAutorotate {
    return NO;
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    
}

#pragma mark - Below methods will called as user try to change the selected show

-(void) ChangeshowProcess: (NSString *)showid
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"assets"];
    NSError*    error = nil; //error setting
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    
    NSString *dataPath1 = [dataPath stringByAppendingPathComponent:@"uploads"];
    NSError*    error1 = nil; //error setting
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath1])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath1 withIntermediateDirectories:NO attributes:nil error:&error1]; //Create folder
    
    
    NSUserDefaults *prefspre = [NSUserDefaults standardUserDefaults];
    NSString *Update_sync_timepre = [prefspre valueForKey:@"Update_sync_time"];
    NSString *Show_selectpre = [prefspre valueForKey:@"Show_select"];
    
    
    NSData *LoginData = [WebserviceAPI requestFetchAllDataandUpdate:@"0000-00-00 00:00:00" Version:@"1" Showid:showid];
    
    XmlParser *parser=[[XmlParser alloc] init];
    [parser parseData:LoginData];
    
    if([parser.ResponseCode isEqualToString:@"200"])
    {
        NSString  *parsersynctime = parser.Update_sync_time;
        NSString *parserqueries  = parser.Queries;
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *Update_sync_time = [prefs valueForKey:@"Update_sync_time"];
        [prefs setObject:parsersynctime forKey:@"Update_sync_time"];
        [prefs synchronize];
        
        
       
        
        NSString *temp2=[NSString stringWithFormat:@"SELECT Show_id,Name FROM tradeshow where Show_id = %@",showid];
        
        sqlite3_stmt *dataRows1=[clsDatabase getDataset:[temp2 UTF8String]];
        int k = 0;
        while(sqlite3_step(dataRows1) == SQLITE_ROW)
        {
            myAppdelegate.ShowId = [NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows1,0)];
            
            
            myAppdelegate.ShowName =[self convertEntities:[NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,1)]]];
            k++;
        }
        
        if(k != 0)
        {
            [prefs setObject:myAppdelegate.ShowId forKey:@"Show_select"];
            [prefs setObject:myAppdelegate.ShowName forKey:@"Show_name"];
            
            
            
            [prefs setObject:@"" forKey:@"Update_sync_timeLeads"];
            
        }
        else
        {
            if(k == 0)
            {
                NSString *temp2=[NSString stringWithFormat:@"SELECT Show_id,Name FROM tradeshow where Show_Type = 1"];
                
                sqlite3_stmt *dataRows1=[clsDatabase getDataset:[temp2 UTF8String]];
                
                while(sqlite3_step(dataRows1) == SQLITE_ROW)
                {
                    myAppdelegate.ShowId = [NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows1,0)];
                    
                    
                    myAppdelegate.ShowName =[self convertEntities:[NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,1)]]];
                    
                }
                
                
                [prefs setObject:myAppdelegate.ShowId forKey:@"Show_select"];
                [prefs setObject:myAppdelegate.ShowName forKey:@"Show_name"];
                [prefs setObject:@"0000-00-00 00:00:00" forKey:@"Update_sync_timeLeads"];
                
            }
            
        }
        [self CallLeadSyncWebService];
        
        [myAppdelegate.objVCLeftMenu FetchAllLeads];
        
        
        
        
    }
    else
    {
        if(parser.ResponseCode.length <= 0)
            [myAppdelegate.objRootView ShowAlertView:@"Network is not available \n Please try again later" alertType:14];
        else
            [myAppdelegate.objRootView ShowAlertView:parser.errorMessage alertType:14];
        
        
       
    }
    
    
}
-(void) Changeshow: (NSString *)showid
{
    
    
    [myAppdelegate.objRootView.btn1 setSelected:NO];
    [myAppdelegate.objRootView.btn2 setSelected:NO];
    [myAppdelegate.objRootView.btn3 setSelected:NO];
    [myAppdelegate.objRootView.btn4 setSelected:NO];
    [myAppdelegate.objRootView.btn5 setSelected:NO];
    
    [myAppdelegate.objRootView.btnBadge setHidden:YES];
    
    [myAppdelegate.objRootView.btn1 setEnabled:NO];
    [myAppdelegate.objRootView.btn2 setEnabled:NO];
    [myAppdelegate.objRootView.btn3 setEnabled:NO];
    [myAppdelegate.objRootView.btn4 setEnabled:NO];
    [myAppdelegate.objRootView.btn5 setEnabled:NO];
    
    [self.btn1 setEnabled:NO];
    [self.btn2 setEnabled:NO];
    [self.btn3 setEnabled:NO];
    [self.btn4 setEnabled:NO];
    [self.btn5 setEnabled:NO];
    
    myAppdelegate.editledid = @"-1";
    
    
    [myAppdelegate.objRootView.btn5 setImage:[UIImage imageNamed:@"upload1.png"] forState:UIControlStateNormal];
    [myAppdelegate.objRootView.btn5 setImage:[UIImage imageNamed:@"uploadSelected1.png"] forState:UIControlStateSelected];
    [myAppdelegate.objRootView.btn5 setImage:[UIImage imageNamed:@"uploadSelected1.png"] forState:UIControlStateHighlighted];
    myAppdelegate.activeView=0;
    [myAppdelegate.objRootView ChangeColor:myAppdelegate.objRootView.btn5];
    CGRect frm_but = _splashproglabel.frame;
    frm_but.size.width = 0;
    _splashproglabel.frame = frm_but;
    if(myAppdelegate.backgroundimgpath.length > 0)
    {
        [self.splshimgview setImage:[UIImage imageWithContentsOfFile:myAppdelegate.backgroundimgpath]];
    }
    _SplashView.hidden = false;
    splashtxtlabel.text = @"";
    [self performSelector:@selector(ChangeshowProcess:) withObject:showid afterDelay:0.5];
}

#pragma mark - Below methods will called to sync the data from server to app

-(void)CallWebServicecheckupdate
{
    NSUserDefaults *prefspre = [NSUserDefaults standardUserDefaults];
    NSString *Show_selectpre = [prefspre valueForKey:@"Show_select"];
    
    NSData *LoginData = [WebserviceAPI requestForUpdate:@"1" Showid:Show_selectpre];
    XmlParser *parser=[[XmlParser alloc] init];
    [parser parseData:LoginData];
    
    
    if([parser.ResponseCode isEqualToString:@"200"])
    {
        
        
        NSString *parserqueries  = parser.Queries;
        
        
        NSString *fetchQAuery=[NSString stringWithFormat:@"select App_Update from tradeshow where Show_Id = %@",myAppdelegate.ShowId];
        sqlite3_stmt *dataRows1=[clsDatabase getDataset:[fetchQAuery UTF8String]];
        
        NSString *startupsync = @"0";
        
        while(sqlite3_step(dataRows1) == SQLITE_ROW)
        {
            startupsync = [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,0)]];
            
        }
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
        [prefs setObject:startupsync forKey:@"startupsync"];
        
        [prefs synchronize];
        
        if([startupsync isEqualToString:@"1"])
        {
            if([[Reachability sharedReachability] internetConnectionStatus] != NotReachable)
            {
                if(myAppdelegate.backgroundimgpath.length > 0)
                {
                    [self.splshimgview setImage:[UIImage imageWithContentsOfFile:myAppdelegate.backgroundimgpath]];
                }
                
                _SplashView.hidden = false;
                [self performSelector:@selector(CallWebService) withObject:nil afterDelay:0.5];
            }
            
            
        }
        
        
    }
    
}
-(void)CallWebService
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"assets"];
    NSError*    error = nil; //error setting
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    
    NSString *dataPath1 = [dataPath stringByAppendingPathComponent:@"uploads"];
    NSError*    error1 = nil; //error setting
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath1])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath1 withIntermediateDirectories:NO attributes:nil error:&error1]; //Create folder
    
    
    NSUserDefaults *prefspre = [NSUserDefaults standardUserDefaults];
    NSString *Update_sync_timepre = [prefspre valueForKey:@"Update_sync_time"];
    NSString *Show_selectpre = [prefspre valueForKey:@"Show_select"];
    
    
    NSData *LoginData = [WebserviceAPI requestFetchAllDataandUpdate:Update_sync_timepre Version:@"1" Showid:Show_selectpre];
    
    XmlParser *parser=[[XmlParser alloc] init];
    [parser parseData:LoginData];
    NSString  *parsersynctime = parser.Update_sync_time;
    NSString *parserqueries  = parser.Queries;
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *Update_sync_time = [prefs valueForKey:@"Update_sync_time"];
    [prefs setObject:parsersynctime forKey:@"Update_sync_time"];
    [prefs synchronize];
    
    
    
    
    NSString *Show_select = [prefspre valueForKey:@"Show_select"];
    
    if([Show_select isEqualToString:@"0"])
    {
        NSString *temp2=[NSString stringWithFormat:@"SELECT Show_id,Name FROM tradeshow where Show_Type = 1"];
        
        sqlite3_stmt *dataRows1=[clsDatabase getDataset:[temp2 UTF8String]];
        
        while(sqlite3_step(dataRows1) == SQLITE_ROW)
        {
            myAppdelegate.ShowId = [NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows1,0)];
            
            
            myAppdelegate.ShowName =[self convertEntities:[NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,1)]]];
            
        }
        
        
        [prefs setObject:myAppdelegate.ShowId forKey:@"Show_select"];
        [prefs setObject:myAppdelegate.ShowName forKey:@"Show_name"];
        
        
    }
    else
    {
        NSString *temp2=[NSString stringWithFormat:@"SELECT Show_id,Name FROM tradeshow where Show_id = %@",Show_select];
        
        sqlite3_stmt *dataRows1=[clsDatabase getDataset:[temp2 UTF8String]];
        int k =0;
        while(sqlite3_step(dataRows1) == SQLITE_ROW)
        {
            k++;
        }
        if(k == 0)
        {
            NSString *temp2=[NSString stringWithFormat:@"SELECT Show_id,Name FROM tradeshow where Show_Type = 1"];
            
            sqlite3_stmt *dataRows1=[clsDatabase getDataset:[temp2 UTF8String]];
            
            while(sqlite3_step(dataRows1) == SQLITE_ROW)
            {
                myAppdelegate.ShowId = [NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows1,0)];
                
                
                myAppdelegate.ShowName =[self convertEntities:[NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,1)]]];
                
            }
            
            
            [prefs setObject:myAppdelegate.ShowId forKey:@"Show_select"];
            [prefs setObject:myAppdelegate.ShowName forKey:@"Show_name"];
            [prefs setObject:@"0000-00-00 00:00:00" forKey:@"Update_sync_timeLeads"];
            
        }
        else
        {
            NSString *temp2=[NSString stringWithFormat:@"SELECT Show_id,Name FROM tradeshow where Show_id = %@",Show_select];
            
            sqlite3_stmt *dataRows1=[clsDatabase getDataset:[temp2 UTF8String]];
            
            while(sqlite3_step(dataRows1) == SQLITE_ROW)
            {
                myAppdelegate.ShowId = [NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows1,0)];
                
                
                myAppdelegate.ShowName =[self convertEntities:[NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,1)]]];
                
            }
            
            
            [prefs setObject:myAppdelegate.ShowId forKey:@"Show_select"];
            [prefs setObject:myAppdelegate.ShowName forKey:@"Show_name"];
            
        }
    }
    [self CallLeadSyncWebService];
    
    [myAppdelegate.objVCLeftMenu FetchAllLeads];
    
    
    
}

-(void)CallLeadSyncWebService
{
    
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *Update_sync_time = [prefs valueForKey:@"Update_sync_timeLeads"];
    if([Update_sync_time length] <= 0)
    {
        [prefs setObject:@"0000-00-00 00:00:00" forKey:@"Update_sync_timeLeads"];
        Update_sync_time = @"0000-00-00 00:00:00";
    }
    NSData *LoginData = [WebserviceAPI requestFetchAllLeadDataandUpdate:Update_sync_time Version:@"1" Showid:myAppdelegate.ShowId];
    XmlParser *parser=[[XmlParser alloc] init];
    [parser parseData:LoginData];
    
    if([parser.ResponseCode isEqualToString:@"200"])
    {
        NSString  *parsersynctime = parser.Update_sync_time;
        NSString *parserqueries  = parser.Queries;
        
        
        [prefs setObject:parsersynctime forKey:@"Update_sync_timeLeads"];
        [prefs synchronize];
       
    }
    [self StartDownloading];
    
    [myAppdelegate.objVCLeftMenu FetchAllLeads];
    myAppdelegate.objRootView.progView.hidden = true;
    [myAppdelegate.objRootView HideMyAlertView];
    
    
    //
}

#pragma mark - Below method will handle the special character at hte time syncing

- (NSString*)convertEntities:(NSString*)string
{
    
    NSString    *returnStr = nil;
    
    if( string )
    {
        returnStr = [ string stringByReplacingOccurrencesOfString:@"&amp;" withString: @"&"  ];
        
        returnStr = [ returnStr stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""  ];
        
        returnStr = [ returnStr stringByReplacingOccurrencesOfString:@"&#x27;" withString:@"'"  ];
        
        returnStr = [ returnStr stringByReplacingOccurrencesOfString:@"&#x39;" withString:@"'"  ];
        
        returnStr = [ returnStr stringByReplacingOccurrencesOfString:@"&#x92;" withString:@"'"  ];
        
        returnStr = [ returnStr stringByReplacingOccurrencesOfString:@"&#x96;" withString:@"'"  ];
        
        returnStr = [ returnStr stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"  ];
        
        returnStr = [ returnStr stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"  ];
        
        returnStr = [ [ NSString alloc ] initWithString:returnStr ];
    }
    
    return returnStr;
}


#pragma mark - Below method will start the downloading of required resources for the app

- (void)StartDownloading
{
    PictureArray   = [[NSMutableArray alloc] init];
    
    
    NSString *fetchQAuery=[NSString stringWithFormat:@"SELECT File_Path,Icon,Type_Id FROM file_manager WHERE File_Id in (Select File_Id from all_assets ) OR  File_Id IN (Select File_Id from theme_data)"];
    
    
    
    sqlite3_stmt *dataRows1=[clsDatabase getDataset:[fetchQAuery UTF8String]];
    while(sqlite3_step(dataRows1) == SQLITE_ROW)
    {
        
        [PictureArray addObject: [NSString stringWithFormat:@"%@%@",myAppdelegate.urldwnld,[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,1)]]];
        
        [PictureArray addObject: [NSString stringWithFormat:@"%@%@",myAppdelegate.urldwnld,[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,0)]]];
        
        
    }
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSMutableArray *tmpdataarray;
    tmpdataarray = [[NSMutableArray alloc]init];
    
    NSMutableArray *tmpdataarray2;
    tmpdataarray2 = [[NSMutableArray alloc]init];
    
    NSMutableArray *listItemArray;
    listItemArray=[[NSMutableArray alloc] init];
    
    int cnt = [PictureArray count];
    if([PictureArray count] > 0)
    {
        
        
        for(int i = 0; i < cnt; i++)
        {
            NSString *list = [PictureArray objectAtIndex:i];
            NSArray *listItems = [list componentsSeparatedByString:@"/"];
            NSString *subString = [[list componentsSeparatedByString:@"/"] lastObject];
            [listItemArray addObject:subString];
            NSString *f_name = nil;
            if([listItems containsObject:@"uploads"])
            {
                int inx= [listItems indexOfObject:@"uploads"] + 1;
                if(inx < [listItems count])
                {
                    f_name = [listItems objectAtIndex:inx];
                }
                NSFileManager *FileManager1 = [NSFileManager defaultManager];
                NSString *dataPath = [docDir stringByAppendingPathComponent:@"assets"];
                dataPath = [dataPath stringByAppendingPathComponent:@"uploads"];
                NSString *videopath =  [dataPath stringByAppendingPathComponent:f_name];
                NSString *vdoFilePath = [NSString stringWithFormat:@"%@/%@",videopath,subString];
                BOOL success = [FileManager1 fileExistsAtPath:vdoFilePath];
                if(!success)
                {
                    [tmpdataarray addObject:[PictureArray objectAtIndex:i]];
                    
                }
                
                
            }
        }
        
        
        
        NSString *dataPath = [docDir stringByAppendingPathComponent:@"assets"];
        dataPath = [dataPath stringByAppendingPathComponent:@"uploads"];
        
        NSString *dataPath_All_Assets = [dataPath stringByAppendingPathComponent:@"media"];
        NSArray *filePathsArray_All_Assets = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:dataPath_All_Assets  error:nil];
        
        
        NSString *dataPath_Assets_Icon = [dataPath stringByAppendingPathComponent:@"Thumbnails"];
        NSArray *filePathsArray_Assets_Icon = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:dataPath_Assets_Icon  error:nil];
        
        
        NSString *dataPath_Cat_Icon = [dataPath stringByAppendingPathComponent:@"Cat_Icon"];
        NSArray *filePathsArray_Cat_Icon = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:dataPath_Cat_Icon  error:nil];
        
        
        NSMutableArray *listItemArray2=[[NSMutableArray alloc] init];
        
        [listItemArray2 addObjectsFromArray:filePathsArray_All_Assets];
        [listItemArray2 addObjectsFromArray:filePathsArray_Assets_Icon];
        [listItemArray2 addObjectsFromArray:filePathsArray_Cat_Icon];
        
        
        
        
        
        
        
        NSMutableArray *deleteItemArray=[[NSMutableArray alloc] init];
        
        for (int i=0; i<[listItemArray2 count]; i++)
        {
            id value =listItemArray2[i];
            NSUInteger objIdx = [listItemArray indexOfObject:value];
            if(objIdx != NSNotFound)
            {
            }
            else
            {
                [deleteItemArray addObject:value];
            }
        }
        
        
        for (int i=0; i<[deleteItemArray count]; i++)
        {
            NSString *value =deleteItemArray[i];
            
            if ([filePathsArray_All_Assets containsObject:value])
            {
                NSString *path=[dataPath_All_Assets stringByAppendingPathComponent:value];
                NSError*    error = nil;
                NSFileManager *fileManager = [NSFileManager defaultManager];
                
                [fileManager removeItemAtPath:path error:&error];
                
            }
            else if([filePathsArray_Assets_Icon containsObject:value])
            {
                NSString *path=[dataPath_Assets_Icon stringByAppendingPathComponent:value];
                NSError*    error = nil;
                NSFileManager *fileManager = [NSFileManager defaultManager];
                
                
                [fileManager removeItemAtPath:path error:&error];
                
                
            }
            else if ([filePathsArray_Cat_Icon containsObject:value])
            {
                NSString *path=[dataPath_Cat_Icon stringByAppendingPathComponent:value];
                
                NSError*    error = nil;
                NSFileManager *fileManager = [NSFileManager defaultManager];
                
                
                [fileManager removeItemAtPath:path error:&error];
                
                
            }
        }
        
        
        
        
        
        [PictureArray removeAllObjects];
        PictureArray = [tmpdataarray mutableCopy];
        
        
        
        if([PictureArray count] > 0)
        {
            _splashproglabel.frame = CGRectMake(_splashproglabel.frame.origin.x, _splashproglabel.frame.origin.y, 0, _splashproglabel.frame.size.height);
            totalwidth = 700;
            totalfile = [PictureArray count];
            onewidth = (totalwidth/totalfile);
            cureentfileno = 0;
            splashtxtlabel.text = [NSString stringWithFormat:@"Downloading %.0f of %.0f",(cureentfileno +1),totalfile];
            
            [self performSelectorOnMainThread:@selector(downloadImageFileFromURL) withObject:nil waitUntilDone:NO];
        }
        else
        {
            CGRect frm_but = _splashproglabel.frame;
            if(frm_but.size.width < 875)
            {
                frm_but.size.width = 875;
                _splashproglabel.frame = frm_but;
                [UIView animateWithDuration:1.5
                                      delay:0.0
                                    options:UIViewAnimationOptionBeginFromCurrentState
                                 animations:^{
                                     _splashproglabel.frame = frm_but;
                                 }
                                 completion:NULL
                 ];
            }
            
            
            
            
        }
        
    }
    
}
-(void)downloadImageFileFromURL
{
    if([[Reachability sharedReachability] internetConnectionStatus] == NotReachable)
    {
        
        
        [myAppdelegate.objRootView ShowAlertView:@"Network is not available \n Please try again later" alertType:14];
        [PictureArray removeAllObjects];
        
        
    }
    else {
        
        
        
        
        NSString *POSTURL;
        POSTURL = [PictureArray objectAtIndex:0];
        POSTURL = [POSTURL stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        POSTURL = [POSTURL stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSString *mydata123 = POSTURL;
        dataSize = 0;
        POSTURL = [POSTURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        if([POSTURL length] > 0)
        {
            NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:
                                              [NSURLRequest requestWithURL:
                                               [NSURL URLWithString:POSTURL]] delegate:self];
            
            
            
            
            mydata123 = [mydata123 stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            mydata123 = [[NSString stringWithFormat:@"%@",mydata123] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            NSString *subString = [[mydata123 componentsSeparatedByString:@"/"] lastObject];
            NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *pngFilePath1 = [NSString stringWithFormat:@"%@/%@",docDir,subString];
            NSString *ext = [pngFilePath1 pathExtension];
            if([ext isEqualToString:@"zip"])
            {
            }
            else {
                NSString *list = mydata123;
                NSArray *listItems = [list componentsSeparatedByString:@"/"];
                NSString *f_name = nil;
                if([listItems containsObject:@"uploads"])
                {
                    NSError*    error = nil; //error setting
                    
                    int inx= [listItems indexOfObject:@"uploads"] + 1;
                    if(inx < [listItems count])
                    {
                        f_name = [listItems objectAtIndex:inx];
                    }
                    if(f_name != nil)
                    {
                        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                        NSString *documentsDirectory = [paths objectAtIndex:0];
                        NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"assets"];
                        dataPath = [dataPath stringByAppendingPathComponent:@"uploads"];
                        
                        NSString *videopath =  [dataPath stringByAppendingPathComponent:f_name];
                        
                        if (![[NSFileManager defaultManager] fileExistsAtPath:videopath])
                            [[NSFileManager defaultManager] createDirectoryAtPath:videopath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
                        
                    }
                }
                
                
                NSFileManager *FileManager1 = [NSFileManager defaultManager];
                NSString *dataPath = [docDir stringByAppendingPathComponent:@"assets"];
                dataPath = [dataPath stringByAppendingPathComponent:@"uploads"];
                NSString *videopath =  [dataPath stringByAppendingPathComponent:f_name];
                NSString *vdoFilePath = [NSString stringWithFormat:@"%@/%@",videopath,subString];
                pngFilePath1 = vdoFilePath;
                BOOL success = [FileManager1 fileExistsAtPath:vdoFilePath];
                if(success && [ext length] > 0 && [subString length] > 0)
                {
                    [FileManager1 removeItemAtPath:vdoFilePath error:nil];
                }
                
            }
            if (theConnection) {
                
                globalfilepath  = pngFilePath1;
                if (![[NSFileManager defaultManager] fileExistsAtPath:pngFilePath1]) {
                    [[NSFileManager defaultManager] createFileAtPath:pngFilePath1 contents:nil attributes:nil];
                }
            }
        }
        
    }
}
#pragma mark -
#pragma mark Download support (NSURLConnectionDelegate)

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    NSFileHandle *file1 = [NSFileHandle fileHandleForUpdatingAtPath:globalfilepath];
    
    
    [file1 seekToEndOfFile];
    [file1 writeData: data];
    [file1 closeFile];
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    
    
    NSFileManager *FileManager1 = [NSFileManager defaultManager];
    
    NSString *ext = [globalfilepath pathExtension];
    
    if([ext length] > 0)
    {
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:globalfilepath])
        {
            [FileManager1 removeItemAtPath:globalfilepath error:nil];
            
            
        }
        
    }
    
    
    [self LoadingComplete];
    
}
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    
    dataSize = [response expectedContentLength];
    
    
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    [self LoadingComplete];
    
}
-(void)LoadingComplete
{
    
    cureentfileno = cureentfileno + 1;
    if(cureentfileno < totalfile)
    {
        splashtxtlabel.text = [NSString stringWithFormat:@"Downloading %.0f of %.0f",(cureentfileno +1),totalfile];
        
        
    }
    
    CGRect frm_but = _splashproglabel.frame;
    frm_but.size.width = ( cureentfileno * onewidth);
    
    _splashproglabel.frame = frm_but;
    
    
    [UIView animateWithDuration:1.5
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         _splashproglabel.frame = frm_but;
                     }
                     completion:NULL
     ];
    
    
    
    
    
    if([PictureArray count] > 0)
    {
        [PictureArray removeObjectAtIndex:0];
        
    }
    if([PictureArray count] > 0)
    {
        [self downloadImageFileFromURL];
    }
    
}

#pragma mark Here we check the touch event for show and hide the left or hamburger menu

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    UITouch *touch= [touches anyObject];
    CGPoint touchPoint = [touch locationInView:self.view];
    
    
    if (touchPoint.x>386) {
        [myAppdelegate HideLeftMenu];
    }
    
    
    
    if ([touch view] == self.view)
    {
        [myAppdelegate HideLeftMenu];
    }
    
    if (touchPoint.y<self.view.frame.size.height-384 && touchPoint.x<386) {
        if ([myAppdelegate.objVCLeftMenu.btnSettings isSelected]) {
            [myAppdelegate.objVCLeftMenu Click_Settings:myAppdelegate.objVCLeftMenu.btnSettings];
        }
    }
    
    
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Action for manualscanclick

- (IBAction)manualScanClick:(id)sender
{
    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    Step1ViewController* step1 = (Step1ViewController*)[mainStoryBoard instantiateViewControllerWithIdentifier: @"step1ViewController"];
    [myAppdelegate.navcontroller pushViewController:step1 animated:YES];
}

#pragma mark Action for badgescanclick show the camera

- (IBAction)scanBadgeClick:(id)sender {
    
    if([myAppdelegate.ShowId isEqualToString:@"49"])
    {
        UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        UIViewController* step1 = (UIViewController*)[mainStoryBoard instantiateViewControllerWithIdentifier: @"VCScanQRCode"];
        [myAppdelegate.navcontroller pushViewController:step1 animated:YES];
    }
    else
    {
        donescan = false;
        ZBarReaderViewController *reader = [ZBarReaderViewController new];
        reader.readerDelegate = self;
        reader.showsZBarControls = NO;
        reader.supportedOrientationsMask = ZBarOrientationMaskAll;
        
        ZBarImageScanner *scanner = reader.scanner;
        
        [scanner setSymbology: ZBAR_I25
                       config: ZBAR_CFG_ENABLE
                           to: 0];
        
        UIImageView *imgView =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"scanFrame.png"]];
        
        imgView.frame=CGRectMake((self.view.frame.size.width - 457)/2, (self.view.frame.size.height - 447)/2, 457, 447);
        
        
        
        
        [self.btnManualEntry setFrame:CGRectMake(self.view.frame.size.width-250.0,self.view.frame.size.height-120,182.0,47.0)];
        [self.btnManualEntry addTarget:self action:@selector(CancelCamera:) forControlEvents:UIControlEventTouchUpInside];
        
        [myAppdelegate.objRootView ChangeColorImgView:imgView];
        
        [myAppdelegate.objRootView ChangeColor:self.btnManualEntry];
        
        [reader.view addSubview:imgView];
        [reader.view addSubview:self.btnManualEntry];
        
        CATransition *animation;
        animation = [CATransition animation];
        [animation setDelegate:self];
        [animation setType:kCATransitionPush];
        [animation setSubtype:kCATransitionFromLeft];
        [animation setDuration:0.3f];
        [[myAppdelegate.navcontroller.view layer] addAnimation:animation forKey:@"pushIn"];
        [myAppdelegate.navcontroller pushViewController:reader animated:NO];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        
    }
    
    
}

-(IBAction)CancelCamera:(id)sender
{
    CATransition *animation;
    animation = [CATransition animation];
    [animation setDelegate:self];
    [animation setType:kCATransitionPush];
    [animation setSubtype:kCATransitionFromRight];
    [animation setDuration:0.3f];
    [[myAppdelegate.navcontroller.view layer] addAnimation:animation forKey:@"pushIn"];
    
    //[myAppdelegate.navcontroller popViewControllerAnimated:NO];
    myAppdelegate.activeView=0;
    UIButton *btn =[[UIButton alloc] init];
    btn.tag=1;
    [myAppdelegate ShowViewController:btn];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    
    
}

#pragma mark Action for top navigation buttons

-(IBAction)ClickOnTopNavigationButtons:(id)sender{
    UIButton *btn=(UIButton *)sender;
    
    
    
    [myAppdelegate ShowViewController:btn];
}

#pragma mark method for scan result 

- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    
    id<NSFastEnumeration> results =
    [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        
        
        // EXAMPLE: just grab the first barcode
        break;
    
    if(symbol.data.length > 0)
    {
        NSArray *dataArray=[symbol.data componentsSeparatedByString:@","];
        if([dataArray count] > 0)
        {
            
            
            NSArray* data = [[dataArray objectAtIndex:0] componentsSeparatedByString: @"|"];
            
            for(int i=0; i<data.count; i++)
            {
                NSString *desc=[data objectAtIndex:i];
                
                
                desc = [[desc description] stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                [newArray addObject:desc];
                
                
                
            }
            
            
            [newArray removeObject:@""];
            
            
            
            
            if(newArray.count>3)
            {
                NSString* csiMarker = [data objectAtIndex: 0];
                NSString* firstName = [data objectAtIndex: 2];
                NSString* LastName = [data objectAtIndex: 3];  //last name
                
                
                NSString* badgeNumber = [data objectAtIndex: 1];   //badge id
                badgeNumber= [badgeNumber stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                
                
                NSString *firstCharacterOfLastName1= [firstCharacterOfLastName1 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                
                firstCharacterOfLastName1=[LastName substringToIndex:1];
                
                if([firstCharacterOfLastName1 isEqualToString:@" "])
                {
                    firstCharacterOfLastName1=[LastName substringToIndex:2];
                    firstCharacterOfLastName1= [firstCharacterOfLastName1 stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    
                }
                
                
                
                [self getDataFromScanevt_uid:eventId badge_id:badgeNumber check:firstCharacterOfLastName1 token:tokenNumber];
                newArray=[[NSMutableArray alloc]init];
                [reader dismissModalViewControllerAnimated: YES];
            }
            
            else
            {
                newArray=[[NSMutableArray alloc]init];
                [myAppdelegate.objRootView ShowAlertView:@"Badge is not compatible, please check and try again." alertType:1];
            }
            
        }
    }
    
}




-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}

@end
