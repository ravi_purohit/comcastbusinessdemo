//
//  XmlParser.h
//  NewOne
//
//  Created by BLuePony  on 04/04/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XmlParser : NSObject<NSXMLParserDelegate> {

	NSMutableString *currentElement;
	NSString *errorMessage;
	NSString *Update_sync_time;
	NSString *Queries;
    NSString *Show_Id;
    NSString *count;
    NSString *text;
    NSString *Show_change_message;
    NSString *ResponseCode;
   
}
@property(nonatomic,retain)NSMutableString *currentElement;
@property(nonatomic,retain)NSString *errorMessage;
@property(nonatomic,retain)NSString *Update_sync_time;
@property(nonatomic,retain)NSString *Queries;
@property(nonatomic,retain)NSString *Show_Id;
@property(nonatomic,retain)NSString *count;
@property(nonatomic,retain)NSString *text;
@property(nonatomic,retain)NSString *Show_change_message;
@property(nonatomic,retain)NSString *ResponseCode;
-(void)parseData:(NSData *)rawData;


@end
