//
//  NSObject+Showlist.h
//  ComcastBusiness
//
//  Created by User14 on 10/07/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Showlist : NSObject
{
    
    NSString *ShwId;
    NSString *ShwName;
    NSString *ShwBscan;
    NSString *ShwAtrct;
}
@property(nonatomic,retain)NSString *ShwId;
@property(nonatomic,retain)NSString *ShwName;
@property(nonatomic,retain)NSString *ShwBscan;
@property(nonatomic,retain)NSString *ShwAtrct;
@end
