
#import <UIKit/UIKit.h>
#import "CatCusomCell.h"
#import "AssetsCustomClass.h"
#import <QuickLook/QuickLook.h>
@interface step2ViewController : UIViewController<QLPreviewControllerDataSource>
{
    CatCusomCell *mydata;
    NSMutableArray *assestdataarray;
    AssetsCustomClass *myassestsdata;
    NSString *GlobalFilePath;
    int typeval;
    
    
}
@property(nonatomic,strong)IBOutlet UILabel *noassets;

@property (strong, nonatomic) IBOutlet UITableView * _Nullable categoryTable;
@property (strong, nonatomic) IBOutlet UIButton * _Nullable btnAllfiles;
@property (strong, nonatomic) IBOutlet UIButton * _Nullable btnPDF;
@property (strong, nonatomic) IBOutlet UIButton * _Nullable btnVideos;
@property(nonatomic,retain)CatCusomCell * _Nullable mydata;
@property(nonatomic,retain)NSString * _Nullable GlobalFilePath;
@property(nonatomic,assign) int typeval;
@property(nonatomic,retain) AssetsCustomClass * _Nullable myassestsdata;
@property (nonatomic, retain) NSMutableArray * _Nullable assestdataarray;

- (IBAction)allFilesClick:(id _Nullable )sender;
- (IBAction)pdfClick:(id _Nullable )sender;
- (IBAction)videoClick:(id _Nullable )sender;

- (IBAction)nextClick:(id _Nullable )sender;
@property (strong, nonatomic) IBOutlet UIImageView * _Nullable footer;

@property (strong, nonatomic) IBOutlet UIImageView   *bckimgview,*opaqimgview;

@property (strong, nonatomic) IBOutlet UICollectionView * _Nullable collectionView;

@property(strong,nonatomic)IBOutlet UILabel * _Nullable lblSelectedFileCount;
@property(nonatomic,assign)int selectedFileCount;

@property(nonnull,strong)IBOutlet UIButton *btnHamburger;
@property(nonatomic,retain)IBOutlet UIButton *btnNext;

-(IBAction)ClickOnTopNavigationButtons:(id _Nullable )sender;

-(IBAction)Click_viewSend:(id _Nullable )sender;
- (void) FetchAllCategory;
- (NSString*_Nullable)convertEntities:(NSString*_Nullable)string;
-(void) getassetsData: (NSString *_Nullable)catid;


@property(nonatomic,strong)IBOutlet UIButton * _Nullable btn1, *btn2, *btn3, *btn4, *btn5;
@property (strong, nonatomic) IBOutlet UIView * _Nullable footerView, *viewButtonsBG, *viewTableBG;
@property (strong, nonatomic) IBOutlet UIImageView * _Nullable imgViewViewBtn;



@end
