
#import "step2ViewController.h"
#import "step2CategoryTableViewCell.h"
#import "step3ViewController.h"
#import "step2CollectionViewCell.h"
#import "AppDelegate.h"

#import "clsDatabase.h"

#define myAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])


@interface step2ViewController ()
{
    NSMutableArray *categoryList;
    NSInteger selIndex,btnSelected,selIndexForall;
    
    
    
    
}
@end

@implementation step2ViewController
@synthesize btnHamburger,lblSelectedFileCount,selectedFileCount,typeval,GlobalFilePath;
@synthesize btn1, btn2, btn3, btn4, btn5,mydata,assestdataarray,myassestsdata,footerView,imgViewViewBtn, viewButtonsBG, viewTableBG,noassets;

- (void)viewDidLoad {
    
    [self.btn2 setSelected:YES];
    [myAppdelegate.objRootView.btn1 setEnabled:YES];
    [myAppdelegate.objRootView.btn2 setEnabled:YES];
    [myAppdelegate.objRootView.btn3 setEnabled:YES];
    [myAppdelegate.objRootView.btn4 setEnabled:YES];
    [myAppdelegate.objRootView.btn5 setEnabled:YES];
    
    [clsGlobal GetHamburgerButton:self.btnHamburger];
    
    [super viewDidLoad];
    
    if(!myAppdelegate.firsttime)
    {
        myAppdelegate.firsttime = true;
        [myAppdelegate.emailsArray replaceObjectAtIndex:0 withObject:myAppdelegate.leaddataoffline.Email];
    }
    selIndex=0;
    btnSelected=0;
    selectedFileCount=0;
    self.collectionView.delegate=self;
    self.collectionView.dataSource=self;
    
    //********************************************************************************************************************
    //self.footer.backgroundColor=myAppDelegate.footerColor;
    self.footerView.backgroundColor=myAppdelegate.footerColor; //SET FOOTER COLOR ON DATED 02-JUN-2017
    
    self.imgViewViewBtn.backgroundColor=myAppdelegate.footerColor; //SET FOOTER COLOR ON DATED 02-JUN-2017
    self.viewButtonsBG.backgroundColor=myAppdelegate.screen2HeaderBG;//SET FOOTER COLOR ON DATED 02-JUN-2017
    self.viewTableBG.backgroundColor=myAppdelegate.screen2TableBG;//SET FOOTER COLOR ON DATED 02-JUN-2017
    
    //*********************************************************************************************************************
    
    
    
    
    categoryList =[[NSMutableArray alloc] init];
    assestdataarray = [[NSMutableArray alloc] init];
    
    
    typeval = 0;
    
    
    
    self.btnAllfiles.layer.cornerRadius = 2; // this value vary as per your desire
    self.btnAllfiles.clipsToBounds = YES;
    [self.btnAllfiles setTitle:NSLocalizedString(@"ALL FILES", nil) forState:UIControlStateNormal];
    
    
    self.btnPDF.layer.cornerRadius = 2; // this value vary as per your desire
    self.btnPDF.clipsToBounds = YES;
    [self.btnPDF setTitle:NSLocalizedString(@"PDFs", nil) forState:UIControlStateNormal];
    
    
    self.btnVideos.layer.cornerRadius = 2; // this value vary as per your desire
    self.btnVideos.clipsToBounds = YES;
    [self.btnVideos setTitle:NSLocalizedString(@"VIDEOS", nil) forState:UIControlStateNormal];
    
    
    
    self.btnNext.layer.cornerRadius = 2; // this value vary as per your desire
    self.btnNext.clipsToBounds = YES;
    // Do any additional setup after loading the view.
    
    
    
    
    myAppdelegate.btnDeSelectedTextColor = myAppdelegate.colorforall;
    
    [self.footerView setBackgroundColor:myAppdelegate.colorforall];
    [self.imgViewViewBtn setBackgroundColor:myAppdelegate.colorforall];
    [self FetchAllCategory];
    self.lblSelectedFileCount.text=[NSString stringWithFormat:@"%d", [myAppdelegate.selectassetdataarray count]];
    
    
    
    [self.btnAllfiles setBackgroundColor:myAppDelegate.btnSelectedBGColor];
    [self.btnAllfiles setTitleColor:myAppDelegate.btnSelectedTextColor forState:UIControlStateNormal];
    
    
    [self.btnPDF setBackgroundColor:[UIColor whiteColor]];
    [self.btnPDF setTitleColor:myAppDelegate.colorforall forState:UIControlStateNormal];
    
    
    
    [self.btnVideos setBackgroundColor:[UIColor whiteColor]];
    [self.btnVideos setTitleColor:myAppDelegate.colorforall forState:UIControlStateNormal];
    
    if(myAppdelegate.backgroundimgpath.length > 0)
    {
        [self.bckimgview setImage:[UIImage imageWithContentsOfFile:myAppdelegate.backgroundimgpath]];
        self.opaqimgview.hidden = true;
    }
    
    
    [myAppdelegate.objRootView ChangeColor:self.btnNext];
}

#pragma mark Here we fetch all category from database to display in the app

- (void) FetchAllCategory
{
    
    NSString *temp2=[NSString stringWithFormat:@"SELECT Cat_Id,Name FROM category order by Sequence"];
    sqlite3_stmt *dataRows1=[clsDatabase getDataset:[temp2 UTF8String]];
    
    if (mydata==nil) {
        
    }else {
        mydata=nil;
    }
    mydata =[[CatCusomCell alloc]init];
    
    mydata.Cat_Id =  @"0";
    mydata.Name   =  @"ALL CATEGORY";
    [categoryList addObject:mydata];
    
    while(sqlite3_step(dataRows1) == SQLITE_ROW)
    {
        if (mydata==nil) {
            
        }else {
            mydata=nil;
        }
        mydata =[[CatCusomCell alloc]init];
        
        mydata.Cat_Id =  [NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows1,0)];
        mydata.Name   =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows1,1)]];
        
        
        [categoryList addObject:mydata];
    }
    
    [self.categoryTable reloadData];
    if([categoryList count]>0)
    {
        mydata =  [categoryList objectAtIndex:0];
        [self getassetsData:mydata.Cat_Id];
    }
    
    
}

#pragma mark Here we get the assets data for all category

-(void) getassetsData: (NSString *)catid
{
    
    NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    
    if([assestdataarray count] > 0)
    {
        [assestdataarray removeAllObjects];
    }
    
    
    NSString *temp2;
    
    
    
    if(![catid isEqualToString:@"0"])
    {
        if(typeval == 0)
        {
            
            temp2 =[NSString stringWithFormat:@"SELECT tbasset. * ,tbfile. File_Path,tbfile.Icon,tbfile.Type_Id,tbfile.Size , ft.Name as Type_name , group_concat( tbca.Cat_Id ) as Cat_Id FROM all_assets tbasset                   LEFT JOIN asset_sequence tbss ON tbasset.Asset_Id = tbss.Asset_Id LEFT JOIN file_manager tbfile ON tbasset.File_Id = tbfile.File_Id LEFT JOIN filetype ft ON ft.Type_Id = tbfile.Type_Id LEFT JOIN category tbca ON tbss.Cat_Id = tbca.Cat_Id WHERE tbasset.Visible = '0' and tbca.Cat_Id = %@ and (tbasset.Name like '%%' or tbasset.Tag like '%%') GROUP BY tbasset.Asset_Id ORDER BY tbca.Sequence, tbss.Sequence",catid];
        }
        else if(typeval == 1)
        {
            temp2 =[NSString stringWithFormat:@"SELECT tbasset. * ,tbfile. File_Path,tbfile.Icon,tbfile.Type_Id,tbfile.Size , ft.Name as Type_name , group_concat( tbca.Cat_Id ) as Cat_Id FROM all_assets tbasset                   LEFT JOIN asset_sequence tbss ON tbasset.Asset_Id = tbss.Asset_Id LEFT JOIN file_manager tbfile ON tbasset.File_Id = tbfile.File_Id LEFT JOIN filetype ft ON ft.Type_Id = tbfile.Type_Id LEFT JOIN category tbca ON tbss.Cat_Id = tbca.Cat_Id WHERE tbasset.Visible = '0' and  tbfile.Type_Id = %d and tbca.Cat_Id = %@ and (tbasset.Name like '%%%@%%' or tbasset.Tag like '%%%@%%') GROUP BY tbasset.Asset_Id ORDER BY tbca.Sequence, tbss.Sequence",typeval,catid,@"",@""];
        }
        else if(typeval == 2)
        {
            temp2 =[NSString stringWithFormat:@"SELECT tbasset. * ,tbfile. File_Path,tbfile.Icon,tbfile.Type_Id,tbfile.Size , ft.Name as Type_name , group_concat( tbca.Cat_Id ) as Cat_Id FROM all_assets tbasset                   LEFT JOIN asset_sequence tbss ON tbasset.Asset_Id = tbss.Asset_Id LEFT JOIN file_manager tbfile ON tbasset.File_Id = tbfile.File_Id LEFT JOIN filetype ft ON ft.Type_Id = tbfile.Type_Id LEFT JOIN category tbca ON tbss.Cat_Id = tbca.Cat_Id WHERE tbasset.Visible = '0' and  tbfile.Type_Id = %d and tbca.Cat_Id = %@ and (tbasset.Name like '%%%@%%' or tbasset.Tag like '%%%@%%') GROUP BY tbasset.Asset_Id ORDER BY tbca.Sequence, tbss.Sequence",typeval,catid,@"",@""];
        }
        
    }
    else
    {
        if(typeval == 0)
        {
            temp2 =[NSString stringWithFormat:@"SELECT tbasset. * ,tbfile. File_Path,tbfile.Icon,tbfile.Type_Id,tbfile.Size , ft.Name as Type_name, group_concat( tbca.Cat_Id ) as Cat_Id FROM all_assets tbasset                   LEFT JOIN asset_sequence tbss ON tbasset.Asset_Id = tbss.Asset_Id LEFT JOIN file_manager tbfile ON tbasset.File_Id = tbfile.File_Id LEFT JOIN filetype ft ON ft.Type_Id = tbfile.Type_Id LEFT JOIN category tbca ON tbss.Cat_Id = tbca.Cat_Id WHERE tbasset.Visible = '0' and (tbasset.Name like '%%' or tbasset.Tag like '%%') GROUP BY tbasset.Asset_Id ORDER BY tbca.Sequence, tbss.Sequence"];
        }
        else if(typeval == 1)
        {
            temp2 =[NSString stringWithFormat:@"SELECT tbasset. * ,tbfile. File_Path,tbfile.Icon,tbfile.Type_Id,tbfile.Size , ft.Name as Type_name, group_concat( tbca.Cat_Id ) as Cat_Id FROM all_assets tbasset                   LEFT JOIN asset_sequence tbss ON tbasset.Asset_Id = tbss.Asset_Id LEFT JOIN file_manager tbfile ON tbasset.File_Id = tbfile.File_Id LEFT JOIN filetype ft ON ft.Type_Id = tbfile.Type_Id LEFT JOIN category tbca ON tbss.Cat_Id = tbca.Cat_Id WHERE tbasset.Visible = '0' and  tbfile.Type_Id = %d and (tbasset.Name like '%%' or tbasset.Tag like '%%') GROUP BY tbasset.Asset_Id ORDER BY tbca.Sequence, tbss.Sequence",typeval];
        }
        else if(typeval == 2)
        {
            temp2 =[NSString stringWithFormat:@"SELECT tbasset. * ,tbfile. File_Path,tbfile.Icon,tbfile.Type_Id,tbfile.Size , ft.Name as Type_name, group_concat( tbca.Cat_Id ) as Cat_Id FROM all_assets tbasset                   LEFT JOIN asset_sequence tbss ON tbasset.Asset_Id = tbss.Asset_Id LEFT JOIN file_manager tbfile ON tbasset.File_Id = tbfile.File_Id LEFT JOIN filetype ft ON ft.Type_Id = tbfile.Type_Id LEFT JOIN category tbca ON tbss.Cat_Id = tbca.Cat_Id WHERE tbasset.Visible = '0' and  tbfile.Type_Id = %d and (tbasset.Name like '%%' or tbasset.Tag like '%%') GROUP BY tbasset.Asset_Id ORDER BY tbca.Sequence, tbss.Sequence",typeval];
        }
        
        
    }
    
    
    
    sqlite3_stmt *dataRows1=[clsDatabase getDataset:[temp2 UTF8String]];
    
    while(sqlite3_step(dataRows1) == SQLITE_ROW)
    {
        if (myassestsdata==nil) {
            
        }else {
            myassestsdata=nil;
        }
        myassestsdata =[[AssetsCustomClass alloc]init];
        
        //Here Now File_Id become Asset_Id
        myassestsdata.File_Id =  [NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows1,0)];
        
        
        
        
        
        
        
        
        
        
        myassestsdata.Tag =  [NSString stringWithFormat:@"%s",(const char*)sqlite3_column_text(dataRows1, 5)];
        
        
        myassestsdata.Visible =  [NSString stringWithFormat:@"%s",(const char*)sqlite3_column_text(dataRows1, 6)];
        myassestsdata.Count =  0;//[NSString
        
        myassestsdata.Part   =  [NSString stringWithFormat:@"%s",(const char*)sqlite3_column_text(dataRows1, 7)];
        
        myassestsdata.lastUpdateTime =  [NSString stringWithFormat:@"%s",(const char*)sqlite3_column_text(dataRows1, 8)];
        
        NSString *dataPath1 = [docDir stringByAppendingPathComponent: [NSString stringWithFormat:@"%s",(const char*)sqlite3_column_text(dataRows1, 10)]];
        
        myassestsdata.File_Path   =  dataPath1;
        
        
        
        NSString *dataPath = [docDir stringByAppendingPathComponent: [NSString stringWithFormat:@"%s",(const char*)sqlite3_column_text(dataRows1, 11)]];
        
        myassestsdata.Icon =  dataPath;
        
        myassestsdata.TypeId =  [NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows1,12)];
        
        myassestsdata.Size   =  [NSString stringWithFormat:@"%s",(const char*)sqlite3_column_text(dataRows1, 13)];
        
        myassestsdata.File_Type =  [NSString stringWithFormat:@"%s",(const char*)sqlite3_column_text(dataRows1, 14)];
        
        myassestsdata.Cat_Id =  [NSString stringWithFormat:@"%s",(const char*)sqlite3_column_text(dataRows1, 15)];
        if([myassestsdata.Cat_Id length] > 0)
        {
            
            NSRange range = [myassestsdata.Cat_Id rangeOfString:@","];
            
            if (range.length > 0){
                
                myassestsdata.Cat_Id = [myassestsdata.Cat_Id substringToIndex:(range.location)];
            }
            
        }
        
        
        
        [assestdataarray addObject:myassestsdata];
    }
    
    temp2 =[NSString stringWithFormat:@"SELECT File_Sequence FROM tbl_Category where Cat_Id = %@",catid];
    sqlite3_stmt *dataRows11=[clsDatabase getDataset:[temp2 UTF8String]];
    NSArray *myArray;
    while(sqlite3_step(dataRows11) == SQLITE_ROW)
    {
        NSString *fileseq =  [NSString stringWithFormat:@"%@",[NSString stringWithUTF8String:(char *)sqlite3_column_text(dataRows11,0)]];
        myArray = [fileseq componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]];
    }
    NSMutableArray *tmpassestdataarray = [[NSMutableArray alloc] init];
    if([myArray count] > 0)
    {
        for(int i = 0; i< [myArray count]; i++)
        {
            for(int j = 0; j < [assestdataarray count]; j++)
            {
                myassestsdata = [assestdataarray objectAtIndex:j];
                if([myassestsdata.File_Id isEqualToString:[myArray objectAtIndex:i]])
                {
                    [tmpassestdataarray addObject:myassestsdata];
                }
            }
        }
        [assestdataarray removeAllObjects];
        assestdataarray = [tmpassestdataarray mutableCopy];
    }
    
    
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





#pragma mark - tableview data source and delegates


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 47.0;
    
}




-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return categoryList.count;
    
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
    
    
    selIndex=indexPath.row;
    
    selIndexForall=indexPath.row;
    mydata =  [categoryList objectAtIndex:indexPath.row];
    [self getassetsData:mydata.Cat_Id];
    [self.categoryTable reloadData];
    [self.collectionView reloadData];
    
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    
    step2CategoryTableViewCell *cell = (step2CategoryTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"step2CategoryTableViewCell" forIndexPath:indexPath];
    
    
    mydata =  [categoryList objectAtIndex:indexPath.row];
    NSString *title= mydata.Name;
    cell.lblTitle.text=title;
    
    
    cell.selectedView.backgroundColor=myAppdelegate.colorforall;//ON DATED 02-JUNE-2017
    
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    if (selIndex==indexPath.row) {
        cell.lblTitle.textColor=myAppDelegate.categorySelectedColor;
        cell.selectedView.hidden=false;
        cell.contentView.backgroundColor=myAppDelegate.categorySelectedBGColor;
    }
    else
    {
        cell.lblTitle.textColor=myAppDelegate.colorforall;
        cell.selectedView.hidden=true;
        cell.contentView.backgroundColor=myAppDelegate.categoryDeSelectedBGColor;
        
        
    }
    
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}

#pragma mark - Here we handle the pdf file to show it in pdf viewer

- (IBAction)pdfClick:(id)sender {
    
    typeval = 1;
    mydata =  [categoryList objectAtIndex:selIndexForall];
    [self getassetsData:mydata.Cat_Id];
    [self.categoryTable reloadData];
    [self.collectionView reloadData];
    
    
    //ON DATED 02-JUNE-2017
    [self.btnPDF setBackgroundColor:[UIColor blackColor]];
    [self.btnPDF setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    
    
    [self.btnAllfiles setBackgroundColor:[UIColor whiteColor]];
    [self.btnAllfiles setTitleColor:myAppdelegate.colorforall forState:UIControlStateNormal];
    
    
    
    [self.btnVideos setBackgroundColor:[UIColor whiteColor]];
    [self.btnVideos setTitleColor:myAppdelegate.colorforall forState:UIControlStateNormal];
    
    
    
    
    
    
    
}

#pragma mark - Here we handle the video file to show it in video viewer

- (IBAction)videoClick:(id)sender {
    
    typeval = 2;
    mydata =  [categoryList objectAtIndex:selIndexForall];
    [self getassetsData:mydata.Cat_Id];
    [self.categoryTable reloadData];
    [self.collectionView reloadData];
    
    
    
    //ON DATED 02-JUNE-2017
    [self.btnVideos setBackgroundColor:[UIColor blackColor]];
    [self.btnVideos setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.btnPDF setBackgroundColor:[UIColor whiteColor]];
    [self.btnPDF setTitleColor:myAppdelegate.colorforall forState:UIControlStateNormal];
    
    
    
    [self.btnAllfiles setBackgroundColor:[UIColor whiteColor]];
    [self.btnAllfiles setTitleColor:myAppdelegate.colorforall forState:UIControlStateNormal];
    
    
    
    
    
}


#pragma mark-
#pragma mark - collectionview delegate and data source




- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake(209.0, 257.0);
    
}


#pragma mark collection view cell paddings


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    
    if([assestdataarray count] <= 0)
    {
        noassets.hidden = false;
    }
    else
    {
        noassets.hidden = true;
    }
    return assestdataarray.count;
    
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    step2CollectionViewCell *customCell = (step2CollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"customcell" forIndexPath:indexPath];
    
    myassestsdata = [assestdataarray objectAtIndex:indexPath.row];
    
    customCell.lblFileType.text = [myassestsdata.File_Type uppercaseString];
    customCell.lblTitle.text = [myassestsdata.Name uppercaseString];
    [customCell.ImgThumbnail setImage:[UIImage imageWithContentsOfFile:myassestsdata.Icon]];
    [customCell.btnSelect addTarget:self action:@selector(SelectItem:) forControlEvents:UIControlEventTouchUpInside];
    
    [customCell.btnsel addTarget:self action:@selector(SelectItem:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(DoublePress:)];
    singleTap.numberOfTapsRequired = 1;
    customCell.ImgThumbnail.tag = indexPath.row;
    customCell.btnSelect.tag = indexPath.row;
    customCell.btnsel.tag = indexPath.row;
    [customCell.ImgThumbnail addGestureRecognizer:singleTap];
    [customCell.ImgThumbnail setUserInteractionEnabled:YES];
    [customCell.backgroundView setBackgroundColor:[UIColor clearColor]];
    
    customCell.btnFileIcon.tag = indexPath.row;
    
    if([customCell.lblFileType.text isEqualToString:@"PDF"])
        [customCell.btnFileIcon setBackgroundImage:[UIImage imageNamed:@"pdfIcon.png"] forState:UIControlStateNormal];
    else
        [customCell.btnFileIcon setBackgroundImage:[UIImage imageNamed:@"videoIcon.png"] forState:UIControlStateNormal];
    
    [customCell.btnFileIcon addTarget:self action:@selector(ClickOnPDFVideoButton:) forControlEvents:UIControlEventTouchUpInside];
    
    
    customCell.btnsel.hidden = YES;
    if([myAppdelegate.selectassetdataarray containsObject:[NSString stringWithFormat:@"%@",myassestsdata.File_Id]])
    {
        [customCell.btnSelect setSelected:YES];
        customCell.backgroundView.layer.borderWidth = 2;
        customCell.btnsel.hidden = NO;
        [myAppdelegate.objRootView ChangeColor:customCell.btnsel];
        
        
    }
    else
    {
        [customCell.btnSelect setSelected:NO];
        customCell.backgroundView.layer.borderWidth = 1;
        customCell.btnsel.hidden = YES;
        
    }
    
    
    
    
    customCell.backgroundView.layer.borderColor = myAppdelegate.colorforall.CGColor;
    return customCell;
}

-(void)ClickOnPDFVideoButton:(UIButton *)sender{
    
    UIButton *btn=(UIButton *)sender;
    
    myassestsdata = [assestdataarray objectAtIndex:btn.tag];
    myAppdelegate.forseldsel = [assestdataarray objectAtIndex:btn.tag];
    myAppDelegate.GlobalFilePathVal = myassestsdata.File_Path;
    myAppDelegate.GlobalFileNameVal = myassestsdata.Name;
    myAppDelegate.GlobalFileTypeVal = myassestsdata.TypeId;
    
    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UIViewController* step1 = [mainStoryBoard instantiateViewControllerWithIdentifier: @"VCPDFVideoViewer"];
    
    [myAppdelegate.navcontroller presentViewController:step1 animated:YES completion:nil];
}

#pragma mark Here we handle the double tap gesture in app

- (void)DoublePress:(UILongPressGestureRecognizer*)gesture {
    
    
    UIImageView *but = [[UIImageView alloc] init];
    but.tag = gesture.view.tag;
    myassestsdata = [assestdataarray objectAtIndex:but.tag];
    myAppdelegate.forseldsel = [assestdataarray objectAtIndex:but.tag];
    myAppDelegate.GlobalFilePathVal = myassestsdata.File_Path;
    myAppDelegate.GlobalFileNameVal = myassestsdata.Name;
    myAppDelegate.GlobalFileTypeVal = myassestsdata.TypeId;
    
    UIStoryboard *mainStoryBoard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UIViewController* step1 = [mainStoryBoard instantiateViewControllerWithIdentifier: @"VCPDFVideoViewer"];
    
    [myAppdelegate.navcontroller presentViewController:step1 animated:YES completion:nil];
    
    
    
}
#pragma mark -
#pragma mark Preview Controller

/*---------------------------------------------------------------------------
 *
 *--------------------------------------------------------------------------*/
- (NSInteger) numberOfPreviewItemsInPreviewController: (QLPreviewController *) controller
{
    return 1;
}

/*---------------------------------------------------------------------------
 *
 *--------------------------------------------------------------------------*/
- (id <QLPreviewItem>)previewController: (QLPreviewController *)controller previewItemAtIndex:(NSInteger)index
{
    
    return [NSURL fileURLWithPath:GlobalFilePath];
}

#pragma mark Here we handle singletap gesture as user tap on asset select the item

- (void)SinglePress:(UILongPressGestureRecognizer*)gesture {
    UIImageView *but = [[UIImageView alloc] init];
    but.tag = gesture.view.tag;
    
    
    step2CollectionViewCell *customCell = [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:but.tag inSection:0 ]];
    
    [self SelectItem:customCell.btnSelect];
    
    
    
}

#pragma mark Here we select the item for email

-(void)SelectItem:(UIButton *)btn{
    
    step2CollectionViewCell *customCell = [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:btn.tag inSection:0 ]];
    myassestsdata = [assestdataarray objectAtIndex:btn.tag];
    
    myAppdelegate.edited = true;
    
    if([customCell.btnSelect isSelected])
    {
        [btn setSelected:NO];
        selectedFileCount--;
        [customCell.btnSelect setSelected:NO];
        
        
        
        
        if([myAppdelegate.selectassetdataarray containsObject:[NSString stringWithFormat:@"%@",myassestsdata.File_Id]])
        {
            int pos = [myAppdelegate.selectassetdataarray indexOfObject:[NSString stringWithFormat:@"%@",myassestsdata.File_Id]];
            [myAppdelegate.selectassetdataarray removeObjectAtIndex:pos];
        }
        
        customCell.backgroundView.layer.borderWidth = 1;
        
        customCell.backgroundView.layer.borderColor = myAppdelegate.colorforall.CGColor;
    }
    else
    {
        selectedFileCount++;
        [customCell.btnSelect setSelected:YES];
        
        
        customCell.backgroundView.layer.borderWidth = 2;
        
        customCell.backgroundView.layer.borderColor = myAppdelegate.colorforall.CGColor;
        [btn setSelected:YES];
        
        [myAppdelegate.selectassetdataarray addObject:[NSString stringWithFormat:@"%@",myassestsdata.File_Id]];
    }
    
    
    self.lblSelectedFileCount.text=[NSString stringWithFormat:@"%d", [myAppdelegate.selectassetdataarray count]];
    
    if ([myAppdelegate.selectassetdataarray count] > 0) {
        [myAppdelegate.objRootView.btnBadge setHidden:NO];
        
        int cnt = 0;
        
        for(int i = 0; i <[myAppdelegate.emailsArray count]; i++)
        {
            if([[myAppdelegate.emailsArray objectAtIndex:i] length] > 0)
            {
                cnt++;
            }
        }
        
        if(cnt == 0 )
        {
            [myAppdelegate.emailsArray replaceObjectAtIndex:0 withObject:myAppdelegate.leaddataoffline.Email];
        }
        
        
    }else{
        [myAppdelegate.objRootView.btnBadge setHidden:YES];
    }
    [myAppdelegate.objRootView.btnBadge setTitle:[NSString stringWithFormat:@"%d", [myAppdelegate.selectassetdataarray count]] forState:UIControlStateNormal];
    
    
    
    [self.collectionView reloadData];
    
}


#pragma mark Here we handle top navigation button evant

-(IBAction)ClickOnTopNavigationButtons:(id)sender{
    UIButton *btn=(UIButton *)sender;
    
    [myAppdelegate ShowViewController:btn];
}




-(IBAction)Click_viewSend:(id)sender{
    
    if([myAppdelegate.selectassetdataarray count] > 0)
    {
        UIButton *btn=(UIButton *)sender;
        [myAppdelegate ShowViewController:btn];
    }
}

#pragma mark Here we check the touch event for hide the left or hamburger menu

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    UITouch *touch= [touches anyObject];
    
    CGPoint touchPoint = [touch locationInView:self.view];
    
    
    if (touchPoint.x>386) {
        [myAppdelegate HideLeftMenu];
    }
}

#pragma mark viewdidappear

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [myAppdelegate.objRootView.btnBadge setEnabled:YES];
    
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    
}

#pragma mark viewDidDisappear

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}


@end
