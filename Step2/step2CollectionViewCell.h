
#import <UIKit/UIKit.h>

@interface step2CollectionViewCell : UICollectionViewCell
{
    
}

@property(nonatomic,strong)IBOutlet UILabel *lblFileType, *lblTitle;
@property(nonatomic,strong)IBOutlet UIButton *btnSelect, *btnFileIcon, *btnsel;
@property(nonatomic,strong)IBOutlet UIImageView *ImgThumbnail;

@end
