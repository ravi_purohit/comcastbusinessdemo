//
//  XmlParser.m
//  NewOne
//
//  Created by BLuePony  on 04/04/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "XmlParser.h"

@implementation XmlParser
@synthesize currentElement,errorMessage,Update_sync_time,Queries,text,Show_Id,count,Show_change_message,ResponseCode;

-(void)parseData:(NSData *)rawData
{
	
	NSXMLParser *parser = [[NSXMLParser alloc] initWithData:rawData];
	[parser setDelegate:self];
	[parser setShouldProcessNamespaces:NO];
	[parser setShouldReportNamespacePrefixes:NO];
	[parser setShouldResolveExternalEntities:NO];
	[parser parse]; 
	
	
	
}



- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{			
	if (qName)
	{
        elementName = qName;
    }
		
	self.currentElement = [NSMutableString string];
	
	}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{    
	if (qName)
	{
        elementName = qName;
    }

	if ([elementName isEqualToString:@"Update_sync_time"])
	{
		self.Update_sync_time  = self.currentElement;
	}
	else if ([elementName isEqualToString:@"Queries"])
	{
		self.Queries  = self.currentElement;
	}
    else if ([elementName isEqualToString:@"Message"])
	{
		self.errorMessage  = self.currentElement;
	}
    else if ([elementName isEqualToString:@"ResponseMessage"])
    {
        self.errorMessage  = self.currentElement;
    }
    else if ([elementName isEqualToString:@"text"])
    {
        self.text  = self.currentElement;
    }
    else if ([elementName isEqualToString:@"ResponseCode"])
    {
        self.ResponseCode  = self.currentElement;
    }
    else if ([elementName isEqualToString:@"count"])
    {
        self.count  = self.currentElement;
    }
    else if ([elementName isEqualToString:@"Show_Id"])
    {
        self.Show_Id  = self.currentElement;
    }
    else if ([elementName isEqualToString:@"Show_change_message"])
    {
        self.Show_change_message  = self.currentElement;
    }
    
    
    //NSLog(@"didEndElement %@", elementName);
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
	
//	NSLog(@"foundCharacters %@", string);
	
	if (self.currentElement)
	{
	    [self.currentElement appendString:string];
	}
	//NSLog(@"foundCharacters %@", self.currentElement);
}

@end
