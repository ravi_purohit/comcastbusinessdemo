
#import "step3ViewController.h"
#import "step3TableViewCell.h"
#import "step4ViewController.h"
#import "AppDelegate.h"
#import "clsQuestions.h"
#import "cellOptions.h"
#import "clsquestionoption.h"
#import "step3tableviewother.h"
#define myAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])


@interface step3ViewController ()




@end

@implementation step3ViewController
@synthesize btnHamburger,webView;
@synthesize btn1, btn2, btn3, btn4, btn5,arrayQuestions, tblViewQuestions, tableBottomConstraint,footerView;



- (void)viewDidLoad {
    
    [self.btn3 setSelected:YES];
    [clsGlobal GetHamburgerButton:self.btnHamburger];
    
    self.btnNext.layer.cornerRadius = 2; // this value vary as per your desire
    self.btnNext.clipsToBounds = YES;
    
    [super viewDidLoad];
    
    
    self.footerView.backgroundColor=myAppdelegate.footerColor; //SET FOOTER COLOR ON DATED 02-JUN-2017
    
    NSString *fetchQAuery=[NSString stringWithFormat:@"select Field_Id, Label, Type, Field_Name_Id from survey_linking  SL left join survey_fields  SF on SL.Survey_Id=SF.Survey_Id where Show_Id = %@ order by Field_Id", myAppdelegate.ShowId];
    
    sqlite3_stmt *dataRows1=[clsDatabase getDataset:[fetchQAuery UTF8String]];
    NSString *htmlview;
    NSMutableArray *Qarray = [[NSMutableArray alloc] init];
    while(sqlite3_step(dataRows1) == SQLITE_ROW)
    {
        
        clsquestionoption *quest =[[clsquestionoption alloc]init];
        quest.Fieldid = [NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows1,0)];
        
        if(sqlite3_column_text(dataRows1,1) != nil)
            
        
        quest.Type = [NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows1,2)];
        
        
        
        if(sqlite3_column_text(dataRows1,1) != nil)
            [Qarray addObject:quest];
        
        
    }
    
    self.arrayQuestions =[[NSMutableArray alloc] init];
    
    for(int i = 0; i<[Qarray count]; i++ )
    {
        
        clsquestionoption *quest =[[clsquestionoption alloc]init];
        quest = [Qarray objectAtIndex:i];
        NSString *fetchQAuery=[NSString stringWithFormat:@"select Label, Other_Flag from field_options where Field_Id = %@ order by Option_Id", quest.Fieldid];
        
        sqlite3_stmt *dataRows1=[clsDatabase getDataset:[fetchQAuery UTF8String]];
        NSString *htmlview;
        NSMutableArray *Qarray = [[NSMutableArray alloc] init];
        clsQuestions *objQuestion=[[clsQuestions alloc] init];
        objQuestion.question = quest.label;
        objQuestion.questionid = quest.Fieldnameid;
        objQuestion.questiontype = quest.Type;
        objQuestion.arrayOptions=[[NSMutableArray alloc] init];
        objQuestion.arrayOptionsType=[[NSMutableArray alloc] init];
        while(sqlite3_step(dataRows1) == SQLITE_ROW)
        {
            
            
            
            [objQuestion.arrayOptionsType addObject:[NSString stringWithFormat:@"%d",sqlite3_column_int(dataRows1,1)]];
            
        }
        
        [self.arrayQuestions addObject:objQuestion];
    }
    
    
    [self.tblViewQuestions setBackgroundColor:[UIColor clearColor]];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [myAppdelegate.objRootView ChangeColor:_btnNext];
    [self.footerView setBackgroundColor:myAppdelegate.colorforall];
    
    if(myAppdelegate.backgroundimgpath.length > 0)
    {
        [self.bckimgview setImage:[UIImage imageWithContentsOfFile:myAppdelegate.backgroundimgpath]];
        self.opaqimgview.hidden = true;
    }
    
    //    self.opaqimgview.hidden = true;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [self.btn3 setSelected:YES];
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}







#pragma mark - tableview data source and delegates

-(CGFloat)getLabelHeight:(CGSize)labelSize string: (NSString *)string font: (UIFont *)font{
    
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [string boundingRectWithSize:labelSize
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{NSFontAttributeName:font}
                                              context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView.tag == 1000)
    {
        clsQuestions *objQuestion =[self.arrayQuestions objectAtIndex:indexPath.row];
        
        if([objQuestion.questiontype isEqualToString:myAppdelegate.txtboxid])
        {
            CGFloat valo =  [self getLabelHeight:CGSizeMake(100, 100) string:objQuestion.question font:[UIFont fontWithName:@"Montserrat-Black" size:19]];
            
            valo = valo + 90;
            if(valo < 160)
                valo = 160;
            
            return valo;
        }
        float height=(([objQuestion.arrayOptions count]) * 70.0)+120;
        
        if (height<0) {
            height=120.0;
        }
        if([objQuestion.arrayOptionsType containsObject:@"1"]){
            return height + 20;
        }else{
            return height-35.0;
        }
        
        return height;
        
    }
    else
    {
        
        
        clsQuestions *objQuestion = [self.arrayQuestions objectAtIndex:tableView.tag];
        
        if (![[objQuestion.arrayOptionsType objectAtIndex:indexPath.row] isEqualToString:@"1"])
        {
            return 65;
        }
        else
        {
            
            NSString *val = [NSString stringWithFormat:@"%@%@%@",objQuestion.questionid,myAppdelegate.seprator,[objQuestion.arrayOptions objectAtIndex:indexPath.row]];
            
            if([self isContainedIn:myAppdelegate.formvalarray stringToCheck:val])
                return 120;
            else
                return 65;
        }
    }
    
}




-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(tableView.tag == 1000)
    {
        return [self.arrayQuestions count];
    }
    else
    {
        clsQuestions *objQuestion =[self.arrayQuestions objectAtIndex:tableView.tag];
        
        return [objQuestion.arrayOptions count];
        
    }
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if(tableView.tag == 1000)
    {
        
        clsQuestions *objQuestion = [self.arrayQuestions objectAtIndex:indexPath.row];
        step3TableViewCell *cell;
        if([objQuestion.questiontype isEqualToString:myAppdelegate.txtboxid])
        {
            
            cell = (step3TableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"text" forIndexPath:indexPath];
            
            [cell.lblTitleText setFont:[UIFont fontWithName:@"Montserrat-Black" size:19]];
            [cell setBackgroundColor:[UIColor clearColor]];
            cell.lblTitleText.text=[NSString stringWithFormat:@"%@", objQuestion.question];
            
            cell.backgroundColor = [UIColor clearColor];
            cell.contentView.backgroundColor = [UIColor clearColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
            long tag =(indexPath.row * 100) + indexPath.row + 1;
            
            cell.txtTextFld.leftView = paddingView;
            cell.txtTextFld.leftViewMode = UITextFieldViewModeAlways;
            cell.txtTextFld.backgroundColor=[UIColor clearColor];
            cell.txtTextFld.tag=tag;
            cell.txtOtherOption.autocorrectionType = UITextAutocorrectionTypeNo;
            cell.txtOtherOption.autocapitalizationType = UITextAutocapitalizationTypeNone;
            cell.txtTextFld.delegate=self;
            
            
            NSString *val = [NSString stringWithFormat:@"%@%@",objQuestion.questionid,myAppdelegate.seprator];
            
            cell.txtTextFld.text = @"";
            for(int  i = 0; i< myAppdelegate.formvalarray.count; i++)
            {
                if([[myAppdelegate.formvalarray objectAtIndex:i] length] > val.length)
                {
                    NSString *substring = [[myAppdelegate.formvalarray objectAtIndex:i] substringToIndex:val.length];
                    
                    if([val isEqualToString:substring])
                    {
                        NSString *fromindex = [[myAppdelegate.formvalarray objectAtIndex:i] substringFromIndex:val.length];
                        cell.txtTextFld.text = fromindex;
                    }
                }
            }
            
        }
        else
        {
            cell = (step3TableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"check" forIndexPath:indexPath];
            
            
            
            [cell.lbltitleCheck setFont:[UIFont fontWithName:@"Montserrat-Black" size:19]];
            [cell setBackgroundColor:[UIColor clearColor]];
            cell.lbltitleCheck.text=[NSString stringWithFormat:@"%@", objQuestion.question];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            [cell.btnOption1 setHidden:YES];
            [cell.btnOption2 setHidden:YES];
            [cell.btnOption3 setHidden:YES];
            [cell.btnOption4 setHidden:YES];
            [cell.btnOption5 setHidden:YES];
            [cell.btnOption6 setHidden:YES];
            [cell.btnOption7 setHidden:YES];
            [cell.txtOtherOption setHidden:YES];
            
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
            cell.txtOtherOption.leftView = paddingView;
            cell.txtOtherOption.leftViewMode = UITextFieldViewModeAlways;
            cell.txtOtherOption.backgroundColor=[UIColor clearColor];
            cell.txtOtherOption.tag=indexPath.row;
            cell.txtOtherOption.autocorrectionType = UITextAutocorrectionTypeNo;
            cell.txtOtherOption.autocapitalizationType = UITextAutocapitalizationTypeNone;
            cell.txtOtherOption.delegate=self;
            
            
            
            
            [cell.tblViewOther setDataSource:self];
            [cell.tblViewOther setDelegate:self];
            
            cell.tblViewOther.tag = indexPath.row;
            
            [cell.tblViewOther reloadData];
        }
        
        return cell;
    }
    else
    {
        step3tableviewother *cell = (step3tableviewother*)[tableView dequeueReusableCellWithIdentifier:@"stepother" forIndexPath:indexPath];
        
        [cell.btnOption1 setHidden:YES];
        [cell.btnSeltn setHidden:YES];
        [cell.txtOtherOption setHidden:YES];
        
        long tag =(tableView.tag * 100) + indexPath.row + 1;
        
        cell.backgroundColor = [UIColor clearColor];
        cell.contentView.backgroundColor = [UIColor clearColor];
        clsQuestions *objQuestion = [self.arrayQuestions objectAtIndex:tableView.tag];
        
        [self DrawForCell:cell.btnOption1 objQuestion:objQuestion tag:tag i:indexPath.row txtOtherOption:cell.txtOtherOption :cell.btnSeltn];
        
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
        cell.txtOtherOption.leftView = paddingView;
        cell.txtOtherOption.leftViewMode = UITextFieldViewModeAlways;
        cell.txtOtherOption.backgroundColor=[UIColor clearColor];
        cell.txtOtherOption.tag=tag;//indexPath.row;
        cell.txtOtherOption.autocorrectionType = UITextAutocorrectionTypeNo;
        cell.txtOtherOption.autocapitalizationType = UITextAutocapitalizationTypeNone;
        cell.txtOtherOption.delegate=self;
        
        
        return cell;
    }
}

#pragma mark - This method check form field has edittext or not

-(BOOL)isContainedIn:(NSMutableArray *) bunchOfStrings stringToCheck :(NSString *) stringToCheck
{
    for (NSString* string in bunchOfStrings) {
        if ([string caseInsensitiveCompare:stringToCheck] == NSOrderedSame)
            return YES;
    }
    return NO;
}

#pragma mark - Here we draw the cell as per our need

-(void)DrawForCell:(UIButton *) option objQuestion :(clsQuestions *) objQuestion tag :(long) tag i :(int) i txtOtherOption :(UITextField *) txtOtherOption:(UIButton *) selbut {
    
    [option setHidden:NO];
    [option setTitle:[objQuestion.arrayOptions objectAtIndex:i] forState:UIControlStateNormal];
    option.tag=tag;
    selbut.tag=tag;
    NSString *val = [NSString stringWithFormat:@"%@%@%@",objQuestion.questionid,myAppdelegate.seprator,[objQuestion.arrayOptions objectAtIndex:i]];
    
    
    
    
    if([self isContainedIn:myAppdelegate.formvalarray stringToCheck:val])
    {
        [option setSelected:YES];
        [selbut setHidden:NO];
        [selbut setSelected:YES];
        
        
        if([objQuestion.questiontype isEqualToString:myAppdelegate.txtboxid])
        {
            
        }
        else if([objQuestion.questiontype isEqualToString:myAppdelegate.checkboxid])
        {
            [option setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
            
            [selbut setImage:[UIImage imageNamed:@"smallcheck.png"] forState:UIControlStateNormal];
            [myAppdelegate.objRootView ChangeColor:selbut];
        }
        else if([objQuestion.questiontype isEqualToString:myAppdelegate.radioboxid])
        {
            [option setImage:[UIImage imageNamed:@"radio.png"] forState:UIControlStateNormal];
            
            [selbut setImage:[UIImage imageNamed:@"smallradio.png"] forState:UIControlStateNormal];
            [myAppdelegate.objRootView ChangeColor:selbut];
        }
        
        
        if (![[objQuestion.arrayOptionsType objectAtIndex:(tag%100)-1] isEqualToString:@"1"])
        {
            
        }else{
            
            [txtOtherOption setHidden:NO];
            txtOtherOption.text = @"";
            txtOtherOption.autocorrectionType = UITextAutocorrectionTypeNo;
            
            NSString *val = [NSString stringWithFormat:@"%@%@ot",objQuestion.questionid,myAppdelegate.seprator];
            
            
            for(int  i = 0; i< myAppdelegate.formvalarray.count; i++)
            {
                if([[myAppdelegate.formvalarray objectAtIndex:i] length] > val.length)
                {
                    NSString *substring = [[myAppdelegate.formvalarray objectAtIndex:i] substringToIndex:val.length];
                    
                    if([val isEqualToString:substring])
                    {
                        NSString *fromindex = [[myAppdelegate.formvalarray objectAtIndex:i] substringFromIndex:val.length];
                        txtOtherOption.text = fromindex;
                    }
                }
            }
        }
        
    }
    else
    {
        [option setSelected:NO];
        [selbut setHidden:YES];
        [selbut setSelected:NO];
        
        if([objQuestion.questiontype isEqualToString:myAppdelegate.txtboxid])
        {
            
        }
        else if([objQuestion.questiontype isEqualToString:myAppdelegate.checkboxid])
        {
            [option setImage:[UIImage imageNamed:@"uncheck.png"] forState:UIControlStateNormal];
        }
        else if([objQuestion.questiontype isEqualToString:myAppdelegate.radioboxid])
        {
            [option setImage:[UIImage imageNamed:@"radio.png"] forState:UIControlStateNormal];
        }
        
        [txtOtherOption setHidden:YES];
    }
    
    
}



#pragma mark - Action for next button click

- (IBAction)nextClick:(id)sender {
    
    
    UIButton *btn =sender;
    btn.tag=4;
    
    [myAppdelegate ShowViewController:btn];
    myAppdelegate.activeView=4;
    
}

-(IBAction)ClickOnTopNavigationButtons:(id)sender{
    UIButton *btn=(UIButton *)sender;
    
    [myAppdelegate ShowViewController:btn];
}

#pragma mark Here we check the touch event for hide the left or hamburger menu

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    UITouch *touch= [touches anyObject];
    CGPoint touchPoint = [touch locationInView:self.view];
    
    
    if (touchPoint.x>386) {
        [myAppdelegate HideLeftMenu];
    }
}

- (void)keyboardWillHide:(NSNotification *)notification{
    self.tableBottomConstraint.constant=12;
    
}
#pragma mark - Textfield delegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    
    self.tableBottomConstraint.constant=250;
    
    
    
    
    return YES;
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    self.tableBottomConstraint.constant=12;
    
    
    
    
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
}
-(void)textFieldDidChange :(UITextField *)theTextField{
    
}
- (BOOL)textField:(UITextField *)textField didChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([textField isFirstResponder])
    {
        if ([[[textField textInputMode] primaryLanguage] isEqualToString:@"emoji"] || ![[textField textInputMode] primaryLanguage])
        {
            return NO;
        }
    }
    if ([textField.text length] > 300) {
        textField.text = [textField.text substringToIndex:300-1];
        return NO;
    }
    
    return YES;
}


@end
