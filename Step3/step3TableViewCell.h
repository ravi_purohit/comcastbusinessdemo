
#import <UIKit/UIKit.h>

@interface step3TableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblTitleText;
@property (strong, nonatomic) IBOutlet UILabel *lblTitleRadio;
@property (strong, nonatomic) IBOutlet UILabel *lbltitleCheck;
@property (strong, nonatomic) IBOutlet UILabel *lblTitleDropdown;

@property(nonatomic,strong)IBOutlet UITextField *txtTextFld;

@property(nonatomic,strong)IBOutlet UIView *viewOptions;
@property(nonatomic,strong)IBOutlet UITableView *tblViewOptions;

@property(nonatomic,strong)IBOutlet UIButton *btnOption1,*btnOption2, *btnOption3, *btnOption4, *btnOption5, *btnOption6, *btnOption7;
@property(nonatomic,strong)IBOutlet UITextField *txtOtherOption;
@property(nonatomic,strong)IBOutlet UITableView *tblViewOther;

@end
