
#import <UIKit/UIKit.h>

@interface step3ViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *footer, *bckimgview,*opaqimgview;
- (IBAction)nextClick:(id)sender ;

@property(nonnull,strong)IBOutlet UIButton *btnHamburger;

-(IBAction)ClickOnTopNavigationButtons:(id)sender;
@property(nonatomic,retain)IBOutlet UIButton *btnNext;
@property(nonatomic,retain)IBOutlet UIWebView *webView;
@property(nonatomic,strong)IBOutlet UIButton *btn1, *btn2, *btn3, *btn4, *btn5;


@property(nonatomic,strong)NSMutableArray *arrayQuestions;
@property(nonatomic,strong)IBOutlet UITableView *tblViewQuestions;



@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tableBottomConstraint;

@property (strong, nonatomic) IBOutlet UIView *footerView;

@end
