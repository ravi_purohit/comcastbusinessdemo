//
//  clsQuestions.h
//  ComcastBusiness
//
//  Created by Admin on 5/29/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface clsQuestions : NSObject
{
    
}

@property(nonatomic,strong)NSString *question;
@property(nonatomic,strong)NSString *questionid;
@property(nonatomic,strong)NSString *questiontype;
@property(nonatomic,strong)NSMutableArray *arrayOptions;
@property(nonatomic,strong)NSMutableArray *arrayOptionsType;

@end
