//
//  AssetsCustomClass.h
//  ExxonMobil
//
//  Created by BLuePony on 15/02/14.
//  Copyright (c) 2014 BLuePony. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LeadCustomClass : NSObject
{
     NSString *ids;
     NSString *shwid;
     NSString *Fname;
     NSString *Lname;
     NSString *Email;
     NSString *Phone;
     NSString *Bname;
     NSString *Jtitle;
     NSString *Address;
     NSString *Zcode;
     NSString *Rsrcid;
     NSString *Surid;
     NSString *Notes;
     NSString *emailid;
     NSString *surdata;
     NSString *leadId;
     NSString *fromOffline;
     NSString *badgeresult;
     NSString *leadtype;
    
    
    
}

@property(nonatomic,retain) NSString *ids;
@property(nonatomic,retain) NSString *shwid;
@property(nonatomic,retain) NSString *Fname;
@property(nonatomic,retain) NSString *Lname;
@property(nonatomic,retain) NSString *Email;
@property(nonatomic,retain) NSString *Phone;
@property(nonatomic,retain) NSString *Bname;
@property(nonatomic,retain) NSString *Jtitle;
@property(nonatomic,retain) NSString *Address;
@property(nonatomic,retain) NSString *Zcode;
@property(nonatomic,retain) NSString *Rsrcid;
@property(nonatomic,retain) NSString *Surid;
@property(nonatomic,retain) NSString *Notes;
@property(nonatomic,retain) NSString *emailid;
@property(nonatomic,retain) NSString *surdata;
@property(nonatomic,retain) NSString *leadId;
@property(nonatomic,retain) NSString *fromOffline;
@property(nonatomic,retain) NSString *badgeresult;
@property(nonatomic,retain) NSString *leadtype;


@end
