//
//  VCScanQRCode.h
//  ComcastBusiness
//
//  Created by Admin on 6/16/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZXingObjC.h"

@interface VCScanQRCode : UIViewController<ZXCaptureDelegate, ZBarReaderDelegate>
{
    BOOL *donescan;
}

@property (nonatomic, strong) ZXCapture *capture;
@property (nonatomic, strong) IBOutlet UIView *scanRectView;
@property (nonatomic, strong) IBOutlet UILabel *decodedLabel;
@property (nonatomic, assign) BOOL *donescan;
@property(nonatomic,strong)IBOutlet UIButton *btnManualEntry;
@property(nonatomic,strong)IBOutlet UIImageView *imgarea;
- (IBAction)manualScanClick:(id)sender;
@end
