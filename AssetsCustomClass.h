//
//  AssetsCustomClass.h
//  ExxonMobil
//
//  Created by BLuePony on 15/02/14.
//  Copyright (c) 2014 BLuePony. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AssetsCustomClass : NSObject
{
     NSString *Assets_Id;
     NSString *Name;
     NSString *Summary;
     NSString *Tag;
     NSString *Icon;
     NSString *File_Id;
     NSString *Cat_Id;
     NSString *Visible;
     NSString *Sequence;
     NSString *Count;
     NSString *lastUpdateTime;
     NSString *Title;
     NSString *Size;
     NSString *TypeId;
    
    NSString *File_Path;
     NSString *Part;
    NSString *File_Type;
   
    
    
    
}
@property(nonatomic,retain)NSString *Assets_Id;
@property(nonatomic,retain)NSString *Name;
@property(nonatomic,retain)NSString *Summary;
@property(nonatomic,retain)NSString *Tag;
@property(nonatomic,retain)NSString *Icon;
@property(nonatomic,retain)NSString *File_Id;
@property(nonatomic,retain)NSString *Cat_Id;
@property(nonatomic,retain)NSString *Visible;
@property(nonatomic,retain)NSString *Sequence;
@property(nonatomic,retain)NSString *Count;
@property(nonatomic,retain)NSString *lastUpdateTime;
@property(nonatomic,retain)NSString *Title;
@property(nonatomic,retain)NSString *Size;
@property(nonatomic,retain)NSString *TypeId;

@property(nonatomic,retain)NSString *File_Path;
@property(nonatomic,retain)NSString *File_Type;

@property(nonatomic,retain)NSString *Part;

@end
