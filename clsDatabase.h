//
//  clsDatabase.h
//  Westech Demo
//
//  Created by BLuePony on 15/02/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface clsDatabase : NSObject {

}
+ (void) finalizeStatements;
+(NSString *)getDBPath;
//+(NSString *) getZipFilePath;
+(void) checkDataBase;
+(void)deleteDatabase;
+(void)OpenDatabase;
+(void)CloseDatabase;
+ (sqlite3_stmt *) getDataset:(const char *)query;
+(void)AddFormValue:(const char *)query name:(NSString *)name link:(NSString *)link
               data:(NSString *)data designerval:(NSString *)designerval;

+(void)deleteRecord:(const char *)query;
+(void)UpdateDatabase:(const char *)query;
+(void) alterDB:(NSString *)dbName fldname:(NSString *)fldname;
+(void) createDB:(NSString *)dbnamepath;
+(void)AddBasketVal:(const char *)query Survey_Id:(NSString * )Survey_Id Show_Id:(NSString *)Show_Id First_Name:(NSString *)First_Name Last_Name:(NSString * )Last_Name Email:(NSString *)Email Phone:(NSString *)Phone Job_Function:(NSString * )Job_Function Notes:(NSString *)Notes Created:(NSString *)Created resourceid:(NSString * )resourceid emailid:(NSString *)emailid Business_Name:(NSString *)Business_Name Address:(NSString * )Address Zipcode:(NSString *)Zipcode Surveydata:(NSString *)Surveydata leadId:(NSString *)LeadId Fullresult:(NSString *)Fullresult
           leadtype:(NSString *)leadtype;

+(void)AddSurveyVal:(const char *)query Lead_Id:(NSString * )Lead_Id Field_Name:(NSString *)Field_Name Field_Value:(NSString *)Field_Value;


@end
