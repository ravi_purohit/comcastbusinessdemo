//
//  clsDatabase.m
//  Westech Demo
//
//  Created by BLuePony on 15/02/13.
//  Copyright 2013 __MyCompanyName__. All rights reserved.
//

#import "clsDatabase.h"
static sqlite3 *PatientDatabase = nil;
static NSString *databaseName=@"ComCast.sqlite";

@implementation clsDatabase
-(void)dealloc
{
	[super dealloc];
}


+ (void) finalizeStatements {
	
	if(PatientDatabase) sqlite3_close(PatientDatabase);
}

+(NSString *) getDBPath
{
	NSArray *paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)	;
	NSString *documentDir = [paths objectAtIndex:0];
	return [documentDir stringByAppendingPathComponent:databaseName];
}

+(NSString *) getFolderPath
{
    NSArray *paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)	;
    NSString *documentDir = [paths objectAtIndex:0];
    return [documentDir stringByAppendingPathComponent:@"assets"];
}

+(void) checkDataBase
{
	NSFileManager *fileManager=[NSFileManager defaultManager];
	NSError *error;
	NSString *dbPath =[self getDBPath];
	BOOL success=[fileManager fileExistsAtPath:dbPath];
	if(!success)
	{
		NSString *defaultDBPath=[[[NSBundle mainBundle]resourcePath]stringByAppendingPathComponent:databaseName];
		success=[fileManager copyItemAtPath:defaultDBPath  toPath:dbPath error:&error];
		if(!success)
		{
			NSAssert1(0,@"failed to create database with message '%@'.",[error localizedDescription]);	
		}
	}
	else
	{
		
	}
    
    
    fileManager = [NSFileManager defaultManager];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    documentsDirectory = [NSString stringWithFormat:@"%@/assets",documentsDirectory];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:documentsDirectory])
    {
        NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"assets" ofType:nil];
        if(resourcePath != nil)
            [fileManager copyItemAtPath:resourcePath toPath:documentsDirectory error:&error];
    }
}
+(void) alterDB:(NSString *)dbName fldname:(NSString *)fldname
{
    sqlite3_stmt *statement;
	static sqlite3 *database = nil;
    
    NSString *databasePath = [self getDBPath];
	
    if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
    {
		NSString* firstName = [NSString stringWithFormat: @"select %@ from %@",fldname,dbName];
		const char *sqlStatement = [firstName UTF8String];
		if(sqlite3_prepare_v2(database, sqlStatement, -1, &statement, NULL) != SQLITE_OK)
		{
			NSString *updateSQL = [NSString stringWithFormat: @"ALTER TABLE %@ ADD COLUMN %@ VARCHAR",dbName,fldname];
			const char *update_stmt = [updateSQL UTF8String];
			sqlite3_prepare_v2(database, update_stmt, -1, &statement, NULL);
			
			if(sqlite3_step(statement)==SQLITE_DONE)
			{
				//NSLog(@"OKkk we have done DB altered...");
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                [prefs setObject:nil forKey:@"FirstRun"];
                [prefs synchronize];
                
				
			}
			else
			{
				//NSLog(@"No i think we have some problem with DB altered...");
			}
			// Release the compiled statement from memory
		}
        sqlite3_finalize(statement);
        sqlite3_close(database);
    }
}

+(void)deleteDatabase
{
	NSError **error = NULL;
	NSFileManager *FileManager = [NSFileManager defaultManager];
	NSString *databasepath = [self getDBPath];
	BOOL success = [FileManager fileExistsAtPath:databasepath];
	if(success) {
		[FileManager removeItemAtPath:databasepath error:error];}
}

+ (sqlite3_stmt *) getDataset:(const char *)query
{
	
	sqlite3_stmt *dataRows=nil;
	if(sqlite3_open([[self getDBPath] UTF8String],&PatientDatabase) == SQLITE_OK)
	{
		if (sqlite3_prepare_v2(PatientDatabase, query, -1, &dataRows, NULL)!=SQLITE_OK) 
		{
			//char *err;
//			err=(char *) sqlite3_errmsg(PatientDatabase);
//			if (err)
//				sqlite3_free(err);
		}
		
		sqlite3_close(PatientDatabase);
		PatientDatabase=nil;
	}
	return dataRows;
}

+(sqlite3_stmt *) getDatasetForSearch:(const char *)sql searchValue:(NSString *)searchValue
{
	sqlite3_stmt *dataRows=nil;
	if(sqlite3_open([[self getDBPath] UTF8String],&PatientDatabase) == SQLITE_OK)
	{
		
		if (sqlite3_prepare_v2(PatientDatabase, sql, -1, &dataRows, NULL)!=SQLITE_OK) 
		{
			
			NSAssert1(0,@"error while preparing  %s",sqlite3_errmsg(PatientDatabase));
			
		}
		
		sqlite3_bind_text(dataRows, 1, [searchValue UTF8String],-1,SQLITE_TRANSIENT);
		if (SQLITE_DONE!=sqlite3_step(dataRows))
		{
			//char *err;
//			err=(char *) sqlite3_errmsg(PatientDatabase);
//			if (err)
//				sqlite3_free(err);	
			
		}
		sqlite3_finalize(dataRows);
		sqlite3_close(PatientDatabase);
		PatientDatabase=nil;
	}
	return dataRows;
}

+(void)deleteRecord:(const char *)query 
{
	sqlite3_stmt *dataRows=nil;
	if(sqlite3_open([[self getDBPath] UTF8String],&PatientDatabase) == SQLITE_OK)
	{
		
		if (sqlite3_prepare_v2(PatientDatabase, query, -1, &dataRows, NULL)!=SQLITE_OK) 
		{
			
			char *err;
			err=(char *) sqlite3_errmsg(PatientDatabase);
			if (err)
				sqlite3_free(err);
			
		}
		
		if (SQLITE_DONE!=sqlite3_step(dataRows))
		{
			char *err;
			err=(char *) sqlite3_errmsg(PatientDatabase);
			if (err)
				sqlite3_free(err);
			//NSAssert1(0,@"error while deleting. %s",sqlite3_errmsg(financeDatabase));	
			
		}
		sqlite3_reset(dataRows);
		sqlite3_close(PatientDatabase);
		PatientDatabase=nil;
	}
	
}
//
+(void)AddFormValue:(const char *)query name:(NSString *)name link:(NSString *)link
																		   data:(NSString *)data designerval:(NSString *)designerval;
{
	sqlite3_stmt *dataRows=nil;
	if (sqlite3_prepare_v2(PatientDatabase, query, -1, &dataRows, NULL)!=SQLITE_OK) 
	{
		
		NSAssert1(0,@"error while preparing  %s",sqlite3_errmsg(PatientDatabase));
		
	}
	
	sqlite3_bind_text(dataRows,    1, [name UTF8String],-1,SQLITE_TRANSIENT);
	sqlite3_bind_text(dataRows,    2, [link UTF8String],-1,SQLITE_TRANSIENT);
	sqlite3_bind_text(dataRows,    3, [data UTF8String],-1,SQLITE_TRANSIENT);
	sqlite3_bind_text(dataRows,    4, [designerval UTF8String],-1,SQLITE_TRANSIENT);
    
	if (SQLITE_DONE!=sqlite3_step(dataRows))
	{
		char *err;
		err=(char *) sqlite3_errmsg(PatientDatabase);
		if (err)
			sqlite3_free(err);	
		
	}
	sqlite3_finalize(dataRows);
	
}
																		   
+(void)OpenDatabase
{
	if(sqlite3_open([[self getDBPath] UTF8String],&PatientDatabase) == SQLITE_OK)
	{
		
	}
	
}
																		   
																		   
+(void)CloseDatabase
{
	sqlite3_close(PatientDatabase);
	PatientDatabase=nil;
}

+(void)UpdateDatabase:(const char *)query
{
	sqlite3_stmt *dataRows=nil;
	if(sqlite3_open([[self getDBPath] UTF8String],&PatientDatabase) == SQLITE_OK)
	{
		
		if (sqlite3_prepare_v2(PatientDatabase, query, -1, &dataRows, NULL)!=SQLITE_OK) 
		{
			
			char *err;
			err=(char *) sqlite3_errmsg(PatientDatabase);
			if (err)
				sqlite3_free(err);
			
            
		}
		
		if (SQLITE_DONE!=sqlite3_step(dataRows))
		{
			char *err;
			err=(char *) sqlite3_errmsg(PatientDatabase);
			if (err)
				sqlite3_free(err);
			//NSAssert1(0,@"error while deleting. %s",sqlite3_errmsg(financeDatabase));	
			
		}
		sqlite3_reset(dataRows);
		sqlite3_close(PatientDatabase);
		PatientDatabase=nil;
        NSLog(@"DATABASEEEEEEE====");
	}
	
}
+(void) createDB:(NSString *)dbnamepath
{
	NSString *docsDir;
	NSArray *dirPaths;
	static sqlite3 *database = nil;
	// Get the documents directory
	dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	docsDir = [dirPaths objectAtIndex:0];
	
	// Build the path to the database file
	NSString *databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"ComCast.sqlite"]];
	
	NSFileManager *filemgr = [NSFileManager defaultManager];
	
	if(sqlite3_open([databasePath UTF8String], &database) == SQLITE_OK)
    {
        
//        const char *dbpath = [databasePath UTF8String];
		
		
		//if (sqlite3_open(dbpath, &contactDB) == SQLITE_OK) {
        char *errMsg;
		const char *sql_stmt = [dbnamepath UTF8String];
        
        if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
            
        }
        sqlite3_close(database);
		//}
		//else {
        //
        //		}
	}
	[filemgr release];
}
+(void)AddBasketVal:(const char *)query Survey_Id:(NSString * )Survey_Id Show_Id:(NSString *)Show_Id First_Name:(NSString *)First_Name Last_Name:(NSString * )Last_Name Email:(NSString *)Email Phone:(NSString *)Phone Job_Function:(NSString * )Job_Function Notes:(NSString *)Notes Created:(NSString *)Created resourceid:(NSString * )resourceid emailid:(NSString *)emailid Business_Name:(NSString *)Business_Name Address:(NSString * )Address Zipcode:(NSString *)Zipcode Surveydata:(NSString *)Surveydata leadId:(NSString *)LeadId Fullresult:(NSString *)Fullresult
    leadtype:(NSString *)leadtype
{
    if(LeadId == NULL)
        LeadId = @"0";
    else if(LeadId.length <= 0)
        LeadId = @"0";
    
    if(Notes == NULL)
        Notes = @"";
    
    if(Surveydata == NULL)
        Surveydata = @"";
    
    if(Phone == NULL)
        Phone =@"";
    
    if(Job_Function == NULL)
        Job_Function =@"";
    
    if(Business_Name == NULL)
        Business_Name =@"";
    
    if(Address == NULL)
        Address =@"";
    
    if(Zipcode == NULL)
        Zipcode =@"";
    
    sqlite3_stmt *dataRows=nil;
    if(sqlite3_open([[self getDBPath] UTF8String],&PatientDatabase) == SQLITE_OK)
    {
        
        if (sqlite3_prepare_v2(PatientDatabase, query, -1, &dataRows, NULL)!=SQLITE_OK)
        {
            
            NSAssert1(0,@"error while preparing  %s",sqlite3_errmsg(PatientDatabase));
            
        }
        //sqlite3_bind_text(dataRows, 1, [Survey_Id UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 2, [Survey_Id UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 3, [Show_Id UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 4, [First_Name UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 5, [Last_Name UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 6, [Email UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 7, [Phone UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 8, [Job_Function UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 9, [Notes UTF8String],-1,SQLITE_TRANSIENT);
        //sqlite3_bind_text(dataRows, 9, [Created UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 10, [resourceid UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 11, [emailid UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 12, [Business_Name UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 13, [Address UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 14, [Zipcode UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 15, [Surveydata UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 16, [LeadId UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 17, [Fullresult UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 18, [leadtype UTF8String],-1,SQLITE_TRANSIENT);
        
        if (SQLITE_DONE!=sqlite3_step(dataRows))
        {
            char *err;
            err=(char *) sqlite3_errmsg(PatientDatabase);
            if (err)
                sqlite3_free(err);
            
        }
        sqlite3_finalize(dataRows);
        sqlite3_close(PatientDatabase);
        PatientDatabase=nil;
    }
    
}
+(void)AddSurveyVal:(const char *)query Lead_Id:(NSString * )Lead_Id Field_Name:(NSString *)Field_Name Field_Value:(NSString *)Field_Value {
    
    
    sqlite3_stmt *dataRows=nil;
    if(sqlite3_open([[self getDBPath] UTF8String],&PatientDatabase) == SQLITE_OK)
    {
        
        if (sqlite3_prepare_v2(PatientDatabase, query, -1, &dataRows, NULL)!=SQLITE_OK)
        {
            
            NSAssert1(0,@"error while preparing  %s",sqlite3_errmsg(PatientDatabase));
            
        }
        
//        sqlite3_bind_int(dataRows, 0, 1);
        sqlite3_bind_text(dataRows, 2, [Lead_Id UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 3, [Field_Name UTF8String],-1,SQLITE_TRANSIENT);
        sqlite3_bind_text(dataRows, 4, [Field_Value UTF8String],-1,SQLITE_TRANSIENT);
        
        
        if (SQLITE_DONE!=sqlite3_step(dataRows))
        {
            char *err;
            err=(char *) sqlite3_errmsg(PatientDatabase);
            if (err)
                sqlite3_free(err);
            
        }
        sqlite3_finalize(dataRows);
        sqlite3_close(PatientDatabase);
        PatientDatabase=nil;
    }
    
}


@end
