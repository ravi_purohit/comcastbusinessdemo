
#import "Step1ViewController.h"
#import "STEP1TableViewCell.h"
#import "step2ViewController.h"
#import "AppDelegate.h"


#define myAppDelegate ((AppDelegate *)[[UIApplication sharedApplication] delegate])

@interface Step1ViewController ()



@end

@implementation Step1ViewController
{
    NSMutableArray *listArray;
    NSMutableDictionary *myTextFieldDictionary;
    int txtfieldtag;
}

@synthesize btnHamburger;
@synthesize btn1, btn2, btn3, btn4, btn5,imgAlert,tempCondition,btnTrash,btnBack,lblAlertTitle, lblAlertMessage,arrayData,btnManualEntry,footerView;

- (void)viewDidLoad {
    
    txtfieldtag = -1;
    tempCondition=0;
    [self.btn1 setSelected:YES];
    
    [clsGlobal GetHamburgerButton:self.btnHamburger];
    
    self.btnNext.layer.cornerRadius = 2; // this value vary as per your desire
    self.btnNext.clipsToBounds = YES;
    
    self.btnBack.layer.cornerRadius = 2; // this value vary as per your desire
    self.btnBack.clipsToBounds = YES;
    
    self.btnManualEntry.layer.cornerRadius = 2; // this value vary as per your desire
    self.btnManualEntry.clipsToBounds = YES;
    
    [self.btnManualEntry showsTouchWhenHighlighted];
    
    myAppdelegate.activeView = 1;
    [self setFrame];
    myTextFieldDictionary = [[NSMutableDictionary alloc]init];
    
    if ([myAppdelegate.selectassetdataarray count] > 0) {
        
        [myAppdelegate.objRootView.btnBadge setHidden:NO];
    }else{
        [myAppdelegate.objRootView.btnBadge setHidden:YES];
    }
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *Badge_Scan = [prefs valueForKey:@"Badge_Scan"];
    
    if([Badge_Scan isEqualToString:@"0"])
        [self.btnManualEntry setHidden:YES];
    else
        [self.btnManualEntry setHidden:NO];
    
    
    for(int i = 0; i<8; i++)
    {
        if(i == 0)
            [myTextFieldDictionary setValue:myAppdelegate.scanfname
                                     forKey:[NSString stringWithFormat:@"%d", i]];
        else if(i == 1)
            [myTextFieldDictionary setValue:myAppdelegate.scanlname
                                     forKey:[NSString stringWithFormat:@"%d", i]];
        else if(i == 2)
            [myTextFieldDictionary setValue:myAppdelegate.scanemail
                                     forKey:[NSString stringWithFormat:@"%d", i]];
        else if(i == 3)
            [myTextFieldDictionary setValue:myAppdelegate.scanphone
                                     forKey:[NSString stringWithFormat:@"%d", i]];
        else if(i == 4)
            [myTextFieldDictionary setValue:myAppdelegate.scanbname
                                     forKey:[NSString stringWithFormat:@"%d", i]];
        else if(i == 5)
            [myTextFieldDictionary setValue:myAppdelegate.scanjtitle
                                     forKey:[NSString stringWithFormat:@"%d", i]];
        else if(i == 6)
            [myTextFieldDictionary setValue:myAppdelegate.scanaddr
                                     forKey:[NSString stringWithFormat:@"%d", i]];
        else if(i == 7)
            [myTextFieldDictionary setValue:myAppdelegate.scanzcode
                                     forKey:[NSString stringWithFormat:@"%d", i]];
        
    }
    if(myAppdelegate.leaddataoffline.Fname.length > 0)
    {
        [myTextFieldDictionary setValue:myAppdelegate.leaddataoffline.Fname
                                 forKey:[NSString stringWithFormat:@"%d", 0]];
        
        [myTextFieldDictionary setValue:myAppdelegate.leaddataoffline.Lname
                                 forKey:[NSString stringWithFormat:@"%d", 1]];
        
        [myTextFieldDictionary setValue:myAppdelegate.leaddataoffline.Email
                                 forKey:[NSString stringWithFormat:@"%d", 2]];
        
        [myTextFieldDictionary setValue:myAppdelegate.leaddataoffline.Phone
                                 forKey:[NSString stringWithFormat:@"%d", 3]];
        
        [myTextFieldDictionary setValue:myAppdelegate.leaddataoffline.Bname
                                 forKey:[NSString stringWithFormat:@"%d", 4]];
        
        [myTextFieldDictionary setValue:myAppdelegate.leaddataoffline.Jtitle
                                 forKey:[NSString stringWithFormat:@"%d", 5]];
        
        [myTextFieldDictionary setValue:myAppdelegate.leaddataoffline.Address
                                 forKey:[NSString stringWithFormat:@"%d", 6]];
        
        [myTextFieldDictionary setValue:myAppdelegate.leaddataoffline.Zcode
                                 forKey:[NSString stringWithFormat:@"%d", 7]];
        
        
        myAppdelegate.previousemail = myAppdelegate.leaddataoffline.Email;
    }
    [myAppdelegate.objRootView.btn1 setEnabled:YES];
    [myAppdelegate.objRootView.btn1 setSelected:YES];
    [self.btn1 setSelected:YES];
    [self.btn2 setEnabled:NO];
    [self.btn3 setEnabled:NO];
    [self.btn4 setEnabled:NO];
    [self.btn5 setEnabled:NO];
    [clsGlobal GetHamburgerButton:self.btnHamburger];
    
    if(![myAppdelegate.UpdateId isEqualToString:@"0"])
    {
        [myAppdelegate.objRootView.btn1 setEnabled:YES];
        [myAppdelegate.objRootView.btn2 setEnabled:YES];
        [myAppdelegate.objRootView.btn3 setEnabled:NO];
        [myAppdelegate.objRootView.btn4 setEnabled:NO];
        [myAppdelegate.objRootView.btn5 setEnabled:NO];
        [myAppdelegate.objRootView.btn5 setImage:[UIImage imageNamed:@"upload1.png"] forState:UIControlStateNormal];
        [myAppdelegate.objRootView ChangeColor:myAppdelegate.objRootView.btn5];
        
        [self.btn1 setEnabled:YES];
        [self.btn2 setEnabled:NO];
        [self.btn3 setEnabled:NO];
        [self.btn4 setEnabled:NO];
        [self.btn5 setEnabled:NO];
    }
    self.btnNext.layer.cornerRadius = 2; // this value vary as per your desire
    self.btnNext.clipsToBounds = YES;
    
    [super viewDidLoad];
    [self setFrame];
    
    
    self.footerView.backgroundColor=myAppdelegate.footerColor; //SET FOOTER COLOR ON DATED 02-JUN-2017
    
    self.footerView.backgroundColor=myAppdelegate.colorforall;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    
    
    
    arrayData=[[NSMutableArray alloc] init];
    
    [arrayData addObject:@""];
    [arrayData addObject:@""];
    [arrayData addObject:@""];
    [arrayData addObject:@""];
    [arrayData addObject:@""];
    [arrayData addObject:@""];
    [arrayData addObject:@""];
    [arrayData addObject:@""];
    
    listArray =[[NSMutableArray alloc] init];
    [listArray addObject:NSLocalizedString(@"FIRST NAME", nil)];
    [listArray addObject:NSLocalizedString(@"LAST NAME", nil)];
    [listArray addObject:NSLocalizedString(@"EMAIL", nil)];
    [listArray addObject:NSLocalizedString(@"PHONE", nil)];
    [listArray addObject:NSLocalizedString(@"BUSINESS NAME", nil)];
    [listArray addObject:NSLocalizedString(@"JOB TITLE", nil)];
    [listArray addObject:NSLocalizedString(@"ADDRESS", nil)];
    [listArray addObject:NSLocalizedString(@"ZIP CODE", nil)];
    
    
    [myAppdelegate.objRootView ChangeColor:_btnNext];
    [myAppdelegate.objRootView ChangeColor:btnManualEntry];
    
    if(myAppdelegate.backgroundimgpath.length > 0)
    {
        [self.bckimgview setImage:[UIImage imageWithContentsOfFile:myAppdelegate.backgroundimgpath]];
        self.opaqimgview.hidden = true;
    }
    
    
    
    
}

-(void)setFrame
{
    [self.lblFirstName setFont:[UIFont fontWithName:@"Montserrat-Black" size:20]];
    [self.lblLastName setFont:[UIFont fontWithName:@"Montserrat-Black" size:20]];
    [self.lblEmail setFont:[UIFont fontWithName:@"Montserrat-Black" size:20]];
    [self.lblPhone setFont:[UIFont fontWithName:@"Montserrat-Black" size:20]];
    [self.lblJobTitle setFont:[UIFont fontWithName:@"Montserrat-Black" size:20]];
    [self.lblZipCode setFont:[UIFont fontWithName:@"Montserrat-Black" size:20]];
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:YES];
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}






#pragma mark - tableview data source and delegates


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 77.0;//107;
    
}




-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return listArray.count;
    
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    
    STEP1TableViewCell *cell = (STEP1TableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"STEP1TableViewCell" forIndexPath:indexPath];
    
    
    
    NSString *title=[listArray objectAtIndex:indexPath.row];
    cell.lblTitle.text=NSLocalizedString(title, nil);
    
    [cell.lblTitle setFont:[UIFont fontWithName:@"Montserrat-Black" size:19]];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    cell.lblTitle.textColor=myAppDelegate.titleColor;
    
    [cell.textFld setTag:indexPath.row];
    
    cell.textFld.delegate=self;
    [cell.textFld addTarget:self
                     action:@selector(textFieldDidChange:)
           forControlEvents:UIControlEventEditingChanged];
    
    cell.textFld.text=[self.arrayData objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if ([myTextFieldDictionary objectForKey:[NSString stringWithFormat:@"%ld", (long)cell.textFld.tag]])
    {
        
        
        cell.textFld.text = [myTextFieldDictionary objectForKey:[NSString stringWithFormat:@"%ld", (long)cell.textFld.tag]];
        
    }
    else
    {
        cell.textFld.text = @"";
    }
    
    
    if(indexPath.row == 2)
    {
        [cell.textFld setKeyboardType:UIKeyboardTypeEmailAddress];
        
    }
    else if(indexPath.row == 3 )
    {
        [cell.textFld setKeyboardType:UIKeyboardTypePhonePad];
    }
    else if(indexPath.row == 7)
    {
        [cell.textFld setKeyboardType:UIKeyboardTypeNumberPad];
    }
    else
    {
        [cell.textFld setKeyboardType:UIKeyboardTypeDefault];
        cell.textFld.autocapitalizationType = UITextAutocapitalizationTypeSentences;
    }
    
    
    ///****************************// ON 02-JUN-2017******************
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 20)];
    cell.textFld.leftView = paddingView;
    cell.textFld.leftViewMode = UITextFieldViewModeAlways;
    cell.textFld.backgroundColor=[UIColor clearColor];
    
    cell.textFld.textColor=myAppdelegate.textfieldColor; // ON 02-JUN-2017
    //****************************************************************************
    
    
    cell.textFld.delegate=self;
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}

#pragma mark - textField  delegates

-(void)textFieldDidChange :(UITextField *)theTextField{
    
    if(theTextField.tag == 0)
    {
        myAppdelegate.leaddataoffline.Fname = theTextField.text;
    }
    else if(theTextField.tag == 1)
    {
        myAppdelegate.leaddataoffline.Lname = theTextField.text;
    }
    else if(theTextField.tag == 2)
    {
        myAppdelegate.leaddataoffline.Email = theTextField.text;
    }
    else if(theTextField.tag == 3)
    {
        myAppdelegate.leaddataoffline.Phone = theTextField.text;
    }
    else if(theTextField.tag == 4)
    {
        myAppdelegate.leaddataoffline.Bname = theTextField.text;
    }
    else if(theTextField.tag == 5)
    {
        myAppdelegate.leaddataoffline.Jtitle = theTextField.text;
    }
    else if(theTextField.tag == 6)
    {
        myAppdelegate.leaddataoffline.Address = theTextField.text;
    }
    else if(theTextField.tag == 7)
    {
        myAppdelegate.leaddataoffline.Zcode = theTextField.text;
    }
    
    int mycond = 0;
    if([myAppdelegate.leaddataoffline.Fname stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0 || [myAppdelegate.leaddataoffline.Lname stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0 || [myAppdelegate.leaddataoffline.Email stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0 )
    {
        mycond = 0;
    }
    else if(![self validateEmailWithString:myAppdelegate.leaddataoffline.Email])
    {
        mycond = 1;
    }
    else
    {
        mycond = 4;
    }
    
    
    if(mycond == 4)
    {
        [myAppdelegate.objRootView.btn1 setEnabled:YES];
        [myAppdelegate.objRootView.btn2 setEnabled:YES];
        [myAppdelegate.objRootView.btn3 setEnabled:YES];
        [myAppdelegate.objRootView.btn4 setEnabled:YES];
        [myAppdelegate.objRootView.btn5 setEnabled:YES];
        [myAppdelegate.objRootView.btnBadge setEnabled:YES];
        
        [myAppdelegate.objRootView.btn5 setImage:[UIImage imageNamed:@"upload.png"] forState:UIControlStateNormal];
        [myAppdelegate.objRootView ChangeColor:myAppdelegate.objRootView.btn5];
        [self.btn1 setEnabled:YES];
        [self.btn2 setEnabled:YES];
        [self.btn3 setEnabled:YES];
        [self.btn4 setEnabled:YES];
        [self.btn5 setEnabled:YES];
        
    }
    else
    {
        [myAppdelegate.objRootView.btn5 setImage:[UIImage imageNamed:@"upload1.png"] forState:UIControlStateNormal];
        [myAppdelegate.objRootView ChangeColor:myAppdelegate.objRootView.btn5];
        [myAppdelegate.objRootView.btn1 setEnabled:YES];
        [myAppdelegate.objRootView.btn2 setEnabled:NO];
        [myAppdelegate.objRootView.btn3 setEnabled:NO];
        [myAppdelegate.objRootView.btn4 setEnabled:NO];
        [myAppdelegate.objRootView.btn5 setEnabled:NO];
        [myAppdelegate.objRootView.btnBadge setEnabled:NO];
        
        [self.btn1 setEnabled:YES];
        [self.btn2 setEnabled:NO];
        [self.btn3 setEnabled:NO];
        [self.btn4 setEnabled:NO];
        [self.btn5 setEnabled:NO];
    }
    
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    [myTextFieldDictionary setValue:textField.text
                             forKey:[NSString stringWithFormat:@"%ld", (long)textField.tag]];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    
    if (textField.tag>=7) {
        [arrayData replaceObjectAtIndex:textField.tag withObject:textField.text];
        [textField resignFirstResponder];
        return TRUE;
    }
    
    NSIndexPath *indexPath =[NSIndexPath indexPathForRow:textField.tag  inSection:0];
    STEP1TableViewCell *cell = (STEP1TableViewCell*)[self.table cellForRowAtIndexPath:indexPath];
    [arrayData replaceObjectAtIndex:textField.tag withObject:cell.textFld.text];
    
    
    
    if (textField.tag>=4) {
        indexPath =[NSIndexPath indexPathForRow:7 inSection:0];
        [self.table scrollToRowAtIndexPath:indexPath
                          atScrollPosition:UITableViewScrollPositionTop
                                  animated:YES];
    }
    
    
    
    
    indexPath =[NSIndexPath indexPathForRow:textField.tag + 1 inSection:0];
    cell = (STEP1TableViewCell*)[self.table cellForRowAtIndexPath:indexPath];
    [cell.textFld becomeFirstResponder];
    
    
    return TRUE;
}

-(void)TextFieldValue:(UITextField *)textField{
    
    
    [myTextFieldDictionary setValue:textField.text
                             forKey:[NSString stringWithFormat:@"%ld", (long)textField.tag]];
    
    BOOL isValueBlank=FALSE;
    
    NSString * fName = [myTextFieldDictionary objectForKey:@"0"];
    NSString * lName = [myTextFieldDictionary objectForKey:@"1"];
    NSString * email = [myTextFieldDictionary objectForKey:@"2"];
    if (fName==nil || [fName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0) {
        isValueBlank=TRUE;
        
    }
    if (lName==nil || [lName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0) {
        isValueBlank=TRUE;
        
    }
    if (email==nil || [email stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length <= 0) {
        isValueBlank=TRUE;
        
    }
    
    
    if (isValueBlank==FALSE) {
        
        
        NSString * eMail = [myTextFieldDictionary objectForKey:@"2"];
        
        if (![eMail isEqualToString:@""])
        {
            if (![self validateEmailWithString:eMail]) {
                isValueBlank=TRUE;
            }
        }
        
        
    }
    if (isValueBlank) {
        [myAppdelegate.objRootView.btn1 setEnabled:YES];
        [myAppdelegate.objRootView.btn2 setEnabled:NO];
        [myAppdelegate.objRootView.btn3 setEnabled:NO];
        [myAppdelegate.objRootView.btn4 setEnabled:NO];
        [myAppdelegate.objRootView.btn5 setEnabled:NO];
        [myAppdelegate.objRootView.btn5 setImage:[UIImage imageNamed:@"upload1.png"] forState:UIControlStateNormal];
        [myAppdelegate.objRootView ChangeColor:myAppdelegate.objRootView.btn5];
    }else {
        
        
        
        
        [myAppdelegate.objRootView.btn1 setEnabled:YES];
        [myAppdelegate.objRootView.btn2 setEnabled:YES];
        [myAppdelegate.objRootView.btn3 setEnabled:YES];
        [myAppdelegate.objRootView.btn4 setEnabled:YES];
        [myAppdelegate.objRootView.btn5 setEnabled:YES];
        [myAppdelegate.objRootView.btn5 setImage:[UIImage imageNamed:@"upload.png"] forState:UIControlStateNormal];
        [myAppdelegate.objRootView ChangeColor:myAppdelegate.objRootView.btn5];
    }
    
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([textField isFirstResponder])
    {
        if ([[[textField textInputMode] primaryLanguage] isEqualToString:@"emoji"] || ![[textField textInputMode] primaryLanguage])
        {
            return NO;
        }
    }
    
    myAppdelegate.edited = true;
    
    if (!string.length)
    {
        return YES;
    }
    
    
    if (textField.tag == 7)
    {
        if ([string rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet].invertedSet].location != NSNotFound)
        {
            
            return NO;
        }
    }
    else if(textField.tag == 3)
    {
        char *x = (char*)[string UTF8String];
        
        if([string isEqualToString:@"-"] || [string isEqualToString:@"+"] || [string isEqualToString:@"."] || [string isEqualToString:@" "] || [string isEqualToString:@"0"] || [string isEqualToString:@"1"] ||  [string isEqualToString:@"2"] ||  [string isEqualToString:@"3"] ||  [string isEqualToString:@"4"] ||  [string isEqualToString:@"5"] ||  [string isEqualToString:@"6"] ||  [string isEqualToString:@"7"] ||  [string isEqualToString:@"8"] ||  [string isEqualToString:@"9"] || [string isEqualToString:@"("] ||  [string isEqualToString:@")"] || x[0]==0 ) {
            
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            if(newLength > 18)
            {
                return NO;
            }
            else
            {
                [self performSelector:@selector(TextFieldValue:) withObject:textField afterDelay:0.01];
                return YES;
            }
            
        } else {
            return NO;
        }
    }
    
    [self performSelector:@selector(TextFieldValue:) withObject:textField afterDelay:0.01];
    return YES;
}

#pragma mark
#pragma mark Next button handling







- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    
    
    txtfieldtag = textField.tag;
    UIToolbar * keyboardToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    
    keyboardToolBar.barStyle = UIBarStyleDefault;
    [keyboardToolBar setItems: [NSArray arrayWithObjects:
                                [[UIBarButtonItem alloc]initWithTitle:@"Previous" style:UIBarButtonItemStyleBordered target:self action:@selector(previousTextField)],
                                
                                [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStyleBordered target:self action:@selector(nextTextField)],
                                [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(resignKeyboard)],
                                nil]];
    textField.inputAccessoryView = keyboardToolBar;
    
    
    return YES;
}


-(void) nextTextField
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:txtfieldtag inSection:0];
    STEP1TableViewCell * username_cell = [self.table cellForRowAtIndexPath:indexPath];
    
    if(txtfieldtag >=3)
    {
        
        
        indexPath =[NSIndexPath indexPathForRow:7 inSection:0];
        [self.table scrollToRowAtIndexPath:indexPath
                          atScrollPosition:UITableViewScrollPositionTop
                                  animated:YES];
        
        
        
        
        
        
    }
    
    {
        
    }
    
    
    indexPath = [NSIndexPath indexPathForRow:(txtfieldtag + 1) inSection:0];
    username_cell = [self.table cellForRowAtIndexPath:indexPath];
    
    [self performSelector:@selector(FocusInTextField:) withObject:username_cell afterDelay:0.1];
    
    
}

-(void)FocusInTextField:(STEP1TableViewCell *)cell{
    [cell.textFld becomeFirstResponder];
}
-(void) previousTextField
{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:txtfieldtag inSection:0];
    STEP1TableViewCell * username_cell = [self.table cellForRowAtIndexPath:indexPath];
    
    if(txtfieldtag == 0)
    {
        
    }
    else
    {
        [username_cell.textFld resignFirstResponder];
        indexPath = [NSIndexPath indexPathForRow:(txtfieldtag - 1) inSection:0];
        username_cell = [self.table cellForRowAtIndexPath:indexPath];
        [username_cell.textFld becomeFirstResponder];
    }
    
}

-(void) resignKeyboard
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:txtfieldtag inSection:0];
    STEP1TableViewCell * username_cell = [self.table cellForRowAtIndexPath:indexPath];
    [username_cell.textFld resignFirstResponder];
}

-(void)DisplayToolBarOnKeyboard:(UITextField *)txtField
{
    UIToolbar *inputAccView;
    
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *prevBtn = [[UIBarButtonItem alloc] initWithTitle:@"Prev" style:UIBarButtonItemStyleBordered target:self action:@selector(prevField)];
    UIBarButtonItem *nextBtn = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStyleBordered target:self action:@selector(nextField)];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(resignKeyboard)];
    
    
    [inputAccView setItems:[NSArray arrayWithObjects:prevBtn, nextBtn, flexSpace, doneBtn, nil]];
    [txtField setInputAccessoryView:inputAccView];
    
}

#pragma mark - It will check the validty of enterd email

- (BOOL)validateEmailWithString:(NSString*)checkString
{
    BOOL stricterFilter = YES;
    
    
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
#pragma mark - Keyboard notification handler

- (void)keyboardDidShow: (NSNotification *) notif{
    [self.view layoutIfNeeded];
    self.tableViewBottom.constant=200+100;
    self.btnNextBottom.constant=180+100;
    
    [self.view layoutIfNeeded];
}

- (void)keyboardDidHide: (NSNotification *) notif{
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.26]; // if you want to slide up the view
    
    
    [self.view layoutIfNeeded];
    self.tableViewBottom.constant=0;
    self.btnNextBottom.constant=22;
    
    [self.view layoutIfNeeded];
    
    [UIView commitAnimations];
    
    
}

#pragma mark - Action for top navigation button

-(IBAction)ClickOnTopNavigationButtons:(id)sender{
    UIButton *btn=(UIButton *)sender;
    [myAppdelegate ShowViewController:btn];
}

#pragma mark Here we check the touch event for hide the left or hamburger menu

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    UITouch *touch= [touches anyObject];
    CGPoint touchPoint = [touch locationInView:self.view];
    
    
    if (touchPoint.x>386) {
        [myAppdelegate HideLeftMenu];
    }
}



#pragma mark Here we check the validty of entered phone number

- (BOOL)validateInputPhoneString:(NSString *)phone
{
    NSString *phoneRegEx = @"^(?:(?:\\+?1\\s*(?:[.-]\\s*)?)?(?:\\(\\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\\s*\\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\\s*(?:[.-]\\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\\s*(?:[.-]\\s*)?([0-9]{4})(?:\\s*(?:#|x\\.?|ext\\.?|extension)\\s*(\\d+))?$";
    
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegEx];
    
    return  [phoneTest evaluateWithObject:phone];
}


#pragma mark viewdidappear

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    
    
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    
}









@end
