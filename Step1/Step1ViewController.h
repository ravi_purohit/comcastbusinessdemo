
#import <UIKit/UIKit.h>

@interface Step1ViewController : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UILabel *lblFirstName;
@property (strong, nonatomic) IBOutlet UILabel *lblLastName;
@property (strong, nonatomic) IBOutlet UILabel *lblEmail;
@property (strong, nonatomic) IBOutlet UILabel *lblPhone;
@property (strong, nonatomic) IBOutlet UILabel *lblJobTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblZipCode;


@property (strong,nonatomic) IBOutlet UITableView *table;
- (IBAction)nextButtonClick:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *footer, *imgAlert, *bckimgview,*opaqimgview;
@property (strong, nonatomic) IBOutlet UIView *requiredView;


@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tableViewBottom;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *btnNextBottom;

@property(nonnull,strong)IBOutlet UIButton *btnHamburger;
@property(nonatomic,retain)IBOutlet UIButton *btnNext;

@property(nonatomic,assign)int tempCondition;

-(IBAction)ClickOnTopNavigationButtons:(id)sender;

@property(nonatomic,strong)IBOutlet UIButton *btn1, *btn2, *btn3, *btn4, *btn5;

-(IBAction)Click_BackToScan:(id _Nullable )sender;

-(IBAction)Click_AlertButton:(id)sender;
-(IBAction)Click_TrashButton:(id)sender;
-(void) onTrash;
@property(nonatomic,strong)IBOutlet UIButton *btnTrash, *btnBack, *btnManualEntry;
@property(nonatomic,strong)IBOutlet UILabel *lblAlertTitle, *lblAlertMessage;
- (BOOL)validateEmailWithString:(NSString*)checkString;
@property(nonnull,strong)NSMutableArray *arrayData;

@property (strong, nonatomic) IBOutlet UIView *footerView;


@end
